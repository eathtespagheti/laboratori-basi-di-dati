SELECT
  delivererid
FROM penalties
GROUP BY
  delivererid
HAVING
  COUNT(delivererid) = (
    SELECT
      MAX(q.amount)
    FROM (
        SELECT
          delivererid,
          COUNT(delivererid) AS amount
        FROM penalties
        GROUP BY
          delivererid
      ) AS q
  )