SELECT
  delivererid
FROM companydel
WHERE
  delivererid != 57
  AND companyid IN (
    SELECT
      companyid
    FROM companydel
    WHERE
      delivererid = 57
      AND (
        numcollections > 0
        OR numdeliveries > 0
      )
  )
  AND (
    numcollections > 0
    OR numdeliveries > 0
  )