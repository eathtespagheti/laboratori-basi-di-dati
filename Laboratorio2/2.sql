SELECT
  DISTINCT delivererid
FROM penalties
WHERE
  amount >= 30
  AND delivererid IN (
    SELECT
      delivererid
    FROM penalties
    WHERE
      amount >= 25
      AND amount < 30
  )