SELECT
  DISTINCT delivererid,
  name
FROM deliverers
WHERE
  delivererid IN (
    SELECT
      delivererid
    FROM penalties
    GROUP BY
      delivererid,
      data
    HAVING
      COUNT(delivererid) > 1
  )