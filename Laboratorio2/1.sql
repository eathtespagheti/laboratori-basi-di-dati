SELECT
  delivererid,
  name,
  initials
FROM deliverers
WHERE
  delivererid NOT IN (
    SELECT
      delivererid
    FROM penalties
  )
ORDER BY
  delivererid