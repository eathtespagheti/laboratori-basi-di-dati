SELECT
  delivererid
FROM companydel
WHERE
  companyid IN (
    SELECT
      companyid
    FROM companydel
    WHERE
      delivererid = 57
      AND (
        numcollections > 0
        OR numdeliveries > 0
      )
  )
  AND (
    numcollections > 0
    OR numdeliveries > 0
  )
  AND delivererid != 57
GROUP BY
  companyid,
  delivererid
HAVING
  COUNT(companyid) = (
    SELECT
      COUNT(companyid)
    FROM companydel
    WHERE
      delivererid = 57
      AND (
        numcollections > 0
        OR numdeliveries > 0
      )
    GROUP BY
      companyid,
      delivererid
  )