SELECT
  delivererid
FROM (
    SELECT
      *
    FROM companydel
    WHERE
      numcollections > 0
      OR numdeliveries > 0
  ) AS subquery
GROUP BY
  delivererid
HAVING
  COUNT(delivererid) = (
    SELECT
      COUNT(*)
    FROM companies
  )