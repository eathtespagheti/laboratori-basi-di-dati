SELECT
  deliverers.delivererid,
  deliverers.name
FROM deliverers,
  (
    SELECT
      deliverers.delivererid,
      p.amount
    FROM deliverers
    LEFT OUTER JOIN (
        SELECT
          delivererid,
          COUNT(delivererid) AS amount
        FROM penalties
        WHERE
          data >= CAST('1981-01-01' AS DATE)
          AND data <= CAST('1981-12-31' AS DATE)
        GROUP BY
          delivererid
      ) AS p ON deliverers.delivererid = p.delivererid
  ) AS d1981,
  (
    SELECT
      deliverers.delivererid,
      p.amount
    FROM deliverers
    LEFT OUTER JOIN (
        SELECT
          delivererid,
          COUNT(delivererid) AS amount
        FROM penalties
        WHERE
          data >= CAST('1980-01-01' AS DATE)
          AND data <= CAST('1980-12-31' AS DATE)
        GROUP BY
          delivererid
      ) AS p ON deliverers.delivererid = p.delivererid
  ) AS d1980
WHERE
  deliverers.delivererid = d1980.delivererid
  AND d1980.delivererid = d1981.delivererid
  AND (
    d1980.amount > d1981.amount
    OR (
      d1980.amount IS NOT NULL
      AND d1981.amount IS NULL
    )
  )