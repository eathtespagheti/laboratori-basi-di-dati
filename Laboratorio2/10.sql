(
  SELECT
    DISTINCT delivererid
  FROM companydel
  WHERE
    delivererid != 57
    AND delivererid NOT IN (
      SELECT
        delivererid
      FROM companydel
      WHERE
        companyid NOT IN (
          SELECT
            companyid
          FROM companydel
          WHERE
            delivererid = 57
            AND (
              numcollections > 0
              OR numdeliveries > 0
            )
        )
    )
    AND (
      numcollections > 0
      OR numdeliveries > 0
    )
)