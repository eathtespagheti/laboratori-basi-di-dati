# Quaderni Basi di Dati

Le mie personali soluzioni agli esercizi dei Laboratori del corso di basi di dati al PoliTO, periodo 2019/2020

## Datasets

Ho inserito vari dataset che utilizzo per gli esercizi, quelli nella directory principale sono stati scritti e testati con PostgreSQL, ho inserito anche delle conversioni con MariaDB, MySQL e Oracle nelle rispettive cartelle, ma non le ho mai testate e non ne garantisco il funzionamento. Pull Request che correggano questi ultimi sono ben accette non siate timidi.
