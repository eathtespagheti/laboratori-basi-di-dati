<?php
$PAGE_TITLE = "Ricerca Corso " . $_POST['Codice_corso'];
require "template/begin.php";

$db = connectToDB();
$result = pg_query_params($db, "select
                                    corsi.nome,
                                    corsi.tipo,
                                    orainizio as inizio,
                                    durata,
                                    programma.giorno,
                                    corsi.livello
                                from programma join corsi on programma.codc = corsi.codc
                                where
                                    programma.codfisc in (select distinct codfisc
                                                          from istruttore
                                                          where cognome = $1)
                                and
                                    programma.giorno = $2
                                ", array($_POST['Cognome'], $_POST['Giorno']));
if ($result) {
    echo "<table>";
    echo "<thead>";
    echo "<tr>";
    printTag("th", "Nome");
    printTag("th", "Tipo");
    printTag("th", "Ora Inizio");
    printTag("th", "Ora Fine");
    printTag("th", "Giorno");
    printTag("th", "Livello");
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    while ($row = pg_fetch_array($result)) {
        $secs = $row[3] * 60 - strtotime("00:00:00");
        $row[3] = date("H:i:s",strtotime($row[2])+$secs);
        echo "<tr>";
        // For some reason the array contains the same data two times
        // so I use this variable to print only every two items
        $print_data = true;
        foreach ($row as $data) {
            if ($print_data) {
                printTag("td", $data);
            }
            $print_data = !$print_data;
        }
        echo "</tr>";
    }
    echo "</tbody>";
    echo "</table>";
} else {
    echo "0 results";
}
include "template/end.php";
