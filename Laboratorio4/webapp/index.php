<?php
$PAGE_TITLE = "Laboratorio 4";
require "template/begin.php"; ?>
<h1>Ricerca istruttore</h1>

<form action="ricerca.php" method="post">
    <div>
        <label for="nome">Nome: </label>
        <input type="text" name="Nome" id="nome">
    </div>
    <br>
    <div>
        <label for="cognome">Cognome: </label>
        <input type="text" name="Cognome" id="cognome">
    </div>
    <br>
    <input type="submit" value="Cerca">
</form>

<?php
include "template/end.php"; ?>