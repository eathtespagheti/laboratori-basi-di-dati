<?php
$PAGE_TITLE = "Ricerca per cognome e giorno";
require "template/begin.php"; ?>

<h1>Ricerca corso</h1>
<form action="ricerca-corsi-2.php" method="post">
    <div>
        <label for="cognome">Cognome istruttore: </label>
        <select id="cognome" name="Cognome">
            <?php
            $res = pg_query($db, "select distinct cognome from istruttore");

            if ($res) {
                while ($row = pg_fetch_array($res)) {
                    echo "<option value=\"" . $row[0] . "\">" . $row[0] . "</option>";
                }
            }
            ?>
        </select>
        <br>
        <label for="giorno">Giorno: </label>
        <select id="giorno" name="Giorno">
            <?php
            $res = pg_query($db, "select distinct giorno from programma");

            if ($res) {
                while ($row = pg_fetch_array($res)) {
                    echo "<option value=\"" . $row[0] . "\">" . $row[0] . "</option>";
                }
            }
            ?>
        </select>
    </div>
    <br>
    <input type="submit" value="Cerca">
</form>

<?php
include "template/end.php"; ?>