<?php
$PAGE_TITLE = "Ricerca Corso " . $_POST['Codice_corso'];
require "template/begin.php";

$db = connectToDB();
$result = pg_query_params($db, "select * from corsi where codc = $1", array($_POST['Codice_corso']));
if ($result) {
    echo "<table>";
    echo "<thead>";
    echo "<tr>";
    printTag("th", "CodC");
    printTag("th", "Nome");
    printTag("th", "Tipo");
    printTag("th", "Livello");
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    while ($row = pg_fetch_array($result)) {
        echo "<tr>";
        // For some reason the array contains the same data two times
        // so I use this variable to print only every two items
        $print_data = true;
        foreach ($row as $data) {
            if ($print_data) {
                printTag("td", $data);
            }
            $print_data = !$print_data;
        }
        echo "</tr>";
    }
    echo "</tbody>";
    echo "</table>";
} else {
    echo "0 results";
}
include "template/end.php";
