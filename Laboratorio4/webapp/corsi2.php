<?php
$PAGE_TITLE = "Corsi";
require "template/begin.php"; ?>

<h1>Ricerca corso</h1>
<form action="ricerca-corsi.php" method="post">
    <div>
        <label for="codice_corso">Codice corso: </label>
        <select id="codice_corso" name="Codice_corso">
            <?php
            $db = connectToDB();
            $res = pg_query($db, "select codc from corsi");

            if ($res) {
                while ($row = pg_fetch_array($res)) {
                    echo "<option value=\"" . $row[0] . "\">" . $row[0] . "</option>";
                }
            }
            
            ?>
        </select>
    </div>
    <br>
    <input type="submit" value="Cerca">
</form>

<?php
include "template/end.php"; ?>