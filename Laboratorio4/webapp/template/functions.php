<?php

function connectToDB() {
    return pg_pconnect("host=db dbname=" . $_ENV["POSTGRES_DB"] . " user=" . $_ENV["POSTGRES_USER"] . " password=" . $_ENV["POSTGRES_PASSWORD"] . "");
}

function printTag($tag, $content) {
    echo "<" . $tag . ">" . $content . "</" . $tag . ">";
}

?>
