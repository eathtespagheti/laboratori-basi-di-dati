<div>
    <ul>
        <?php

        $links = array(
            "Homepage" => "index.php",
            "Ricerca Corsi" => "corsi.php",
            "Ricerca Corsi 2" => "corsi2.php",
            "Ricerca per cognome e giorno" => "per_cognome.php"
        );

        foreach ($links as $title => $link) {
            echo "<li>";
            echo "<a href=" . $link . ">" . $title . "</a>";
            echo "</li>";
        }

        ?>
    </ul>
</div>