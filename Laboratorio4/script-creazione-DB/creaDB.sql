DROP TABLE IF EXISTS programma;
DROP TABLE IF EXISTS corsi;
DROP TABLE IF EXISTS istruttore;

-- CREATE tables

CREATE TABLE istruttore (
	codfisc CHAR(20) ,
	nome CHAR(50) NOT NULL ,
	cognome CHAR(50) NOT NULL ,
	datanascita TIMESTAMP(0) NOT NULL ,
	email CHAR(50) NOT NULL ,
	telefono CHAR(20) NULL ,
	PRIMARY KEY (codfisc)
);

CREATE TABLE corsi (
	codc CHAR(10) ,
	nome CHAR(50) NOT NULL ,
	tipo CHAR(50) NOT NULL ,
	livello decimal(38) NOT NULL,
	PRIMARY KEY (codc),
	constraint chk_livello CHECK (livello>=1 AND livello<=4)
);

CREATE TABLE programma (
	codfisc CHAR(20) NOT NULL ,
	giorno CHAR(15) NOT NULL ,
	orainizio TIME NOT NULL ,
	durata decimal(38) NOT NULL ,
	sala CHAR(5) NOT NULL,
	codc CHAR(10) NOT NULL,
	PRIMARY KEY (codfisc,giorno,orainizio),
	FOREIGN KEY (codfisc)
		REFERENCES istruttore(codfisc) 
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	FOREIGN KEY (codc)
		REFERENCES corsi(codc) 
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

