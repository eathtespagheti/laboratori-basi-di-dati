-- A
SELECT NomeD,
    "Settore-Scientifico",
    Conteggio
FROM cd
    JOIN spcd ON cd.CodC = spcd.CodC
    AND cd.CodD = spcd.CodD
    JOIN (
        SELECT CodD,
            COUNT(*) AS "Conteggio"
        FROM cd
        WHERE DataPubblicazione >= TO_DATE("YYYY/MM/DD", 2014 / 04 / 01)
        GROUP BY CodD
    ) AS conti ON cd.CodD = conti.CodD
WHERE NumPosti > 7;
-- B
SELECT Studente.NomeS,
    partMassima."SettoreMassimo"
FROM Studente
    LEFT OUTER JOIN (
        -- Matricole che hanno partecipato a tutti i CD di un determinato settore
        SELECT DISTINCT spcd.MatrS,
            cd."Settore-Scientifico" AS "SettoreMassimo"
        FROM cd
            JOIN spcd ON cd.CodD = spcd.CodD,
            (
                -- Numero di partecipazioni per settore scientifico
                SELECT cd."Settore-Scientifico",
                    COUNT(*) AS "partecipazioni"
                FROM cd
                    JOIN spcd ON cd.CodD = spcd.CodD
                GROUP BY cd."Settore-Scientifico"
            ) AS tmp2
        WHERE cd."Settore-Scientifico" = tmp2."Settore-Scientifico"
        GROUP BY spcd.MatrS,
            cd."Settore-Scientifico"
        HAVING COUNT(*) = tmp2."partecipazioni"
    ) AS partMassima ON Studente.MatrS = partMassima.MatrS
WHERE Studente.MatrS IN (
        -- Lista degli studenti che hanno partecipato ad almeno 3 cd con settore differente
        SELECT MatrS
        FROM spcd
        GROUP BY MatrS
        HAVING COUNT(DISTINCT "Settore-Scientifico") >= 3
    );