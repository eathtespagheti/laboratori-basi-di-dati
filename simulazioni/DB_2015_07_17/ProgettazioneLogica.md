# Progettazione Logica

## Entità

I campi in grassetto e corsivo indicano la chiave primaria

* __Progetto__ (*__Cod__*, Titolo, Budget, Inizio, Fine, Responsabile)
* __Attività__ (*__CodProgetto, Cod__*, Tipo)
* __Congresso__ (*__Cod__*, Nome)
* __Componente__ (*__Cod__*, Descrizione, Nome)
* __Requisito__  (*__Cod__*, Nome)
* __Dipendente__ (*__Matricola__*, Nome, DataAssunzione)
* __Sito__  (*__Cod__*, Indirizzo)
* __PartecipazioneAttività__ (__*Matricola*, *CodProgetto*, *CodAttività*, *Data Inizio*__, DataInizio, DataFine, OreLavoro)
* __Revisore__ (*__Cod__*, Nome, Titolo)
* __Revisione__ (*__CodRevisore, CodProgetto, CodAttività, CodPeriodo__*, Cod, Giudizio)
* __PeriodoTemporale__ (*__Cod__*, DataInizio, DataFine)
* __Rendicontazione__ (*__Anno, Trimestre, CodProgetto__*, Spesa)

### Multivalore trasformati in relazioni

* __Presentato__ (*__CodCongresso__*, *__CodAttività__*)
* __Richiede__ (*__CodComponente__*, *__CodRequisito__*)
* __PossiedeSito__ (*__Matricola__*, *__CodSito__*)

### Relazioni trasformate in entità

* __ComponentiSoftware__ (*__CodProgetto, CodAttività, CodComponente__*)

## Vincoli di integrità

* Attività(CodProgetto) __REFERENCES__ Progetto(Cod)
* Rendicontazione(CodProgetto) __REFERENCES__ Progetto(Cod)
* PossiedeSito(Matricola) __REFERENCES__ Dipendente(Matricola)
