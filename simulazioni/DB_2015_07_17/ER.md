# ER

## Entità

### Progetto

Primary Key: *Cod*

Attributi

- Titolo
- Budget
- Inizio
- Fine

------

### Attività

Tipo entità: *debole*

Primary Key: *Cod*

#### Gerarchia

Tipo: *Totale Esclusiva*

#### Definizione

#### Sviluppo

#### Disseminazione

Attributi

- Congressi (0, N)

------

### Componente

Primary Key: **Cod**

Attributi

- Descrizione
- Nome
- Requisiti (1, N)

------

### Dipendente

Primary Key: **Matricola**

Attributi

- Nome
- Data Assunzione
- Pagina Web (0, 1)

------

### Partecipazione Attività

Tipo entità: *debole*

Primary Key: **Data Inizio**

Attributi

- Data Inizio
- Data Fine
- Ore di lavoro

------

### Revisore

Primary Key: **Cod**

Attributi

- Nome
- Titolo

------

### Revisione

Tipo entità: *debole*

Primary Key: **Cod**

Attributi

- Giudizio

------

### Periodo Temporale

Primary Key: **Cod**

Attributi

- Data inizio
- Data fine

------

### Rendicontazione

Tipo entità: *debole*

Primary Key: **Anno, Trimestre**

Attributi

- Spesa

------

## Relazioni

### Diviso in

- Progetto (1, N)
- Attività (1, 1)

------

### Componenti Software

- Sviluppo (1, N)
- Componente (0, N)

------

### Responsabile Progetto

- Progetto (1, 1)
- Dipendente (0, N)

------

### Impegnato in

- Dipendente (0, N)
- Partecipazione Attività (1, 1)

------

### Svolta da

- Attività (1, N)
- Partecipazione Attività (1, 1)

------

### Esegue

- Revisore (0, N)
- Revisione (1, 1)

------

### Viene revisionata

- Attività (0, N)
- Revisione (1, 1)

------

### Durante

- Revisione (1, 1)
- Periodo Temporale (1, 1)

------

### Rendicontato in

- Progetto (0, N)
- Rendicontazione (1, 1)
