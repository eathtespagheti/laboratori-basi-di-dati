-- B
SELECT Dipendente.Matr,
    Dipendente.Nome
FROM Ferie
    JOIN Dipendente ON Ferie.Matr = Dipendente.Matr
    JOIN (
        -- Media di giorni di ferie per ogni combinazione di CodD e Mansione
        SELECT DISTINCT Dipendente.CodD,
            Dipendente.Mansione,
            AVG(Ferie.DurataInGiorni) AS Media
        FROM Ferie
            JOIN Dipendente ON Ferie.Matr = Dipendente.Matr
        GROUP BY Dipendente.CodD,
            Dipendente.Mansione
    ) AS Media ON Dipendente.CodD = Media.CodD
    AND Dipendente.Mansione = Media.Mansione
WHERE Ferie.DurataInGiorni > Media.Media
GROUP BY Matr
HAVING COUNT(DataInizio) = 1;
-- C
SELECT CodD
FROM Dipartimento
WHERE CodD NOT IN (
        -- Dipartimenti che hanno dipendenti che superano i 21 giorni di ferie
        SELECT CodD
        FROM Dipendente
        WHERE Matr IN (
                -- Dipendenti che hanno effettuato più di 21 giorni di ferie
                SELECT DISTINCT Matr
                FROM Ferie
                GROUP BY Matr
                HAVING SUM(DurataInGiorni) > 21
            )
    )