# ER

## Entità

### Persona

Primary Key: *Cod*

Attributi

* Nome
* Cognome
* Nascita
* Nazionalità

#### Gerarchia

Tipo: *Parziale Esclusiva*

#### Atleta

#### Giudice

#### Organizzatore

Attributi

* Carica
* Cellulare (0, 1)

#### Volontari

------

### Badge

Primary Key: *Cod*

Attributi

* Zone (0, N)

------

### Zona

Primary Key: *NomeZona*

------

### Disciplina

Primary Key: **Cod**

Attributi

* Nome
* Record
* DataRecord

------

### Gara

Tipo Entità: *debole*

Primary Key: **CodDisciplina, Turno, NumeroBatteria**

Attributi

* Data
* Ora

------

### Partecipazione

Tipo Entità: *debole*

Primary Key: **CodPersona, CodGara**

Attributi:

* RuoloGiudice

------

### Partecipazione Attività

Tipo entità: *debole*

Primary Key: **Data Inizio**

Attributi

* Data Inizio
* Data Fine
* Ore di lavoro

------

### Turno

Primary Key: **OraInizio**

Attributi

* Durata

------

### SvolgimentoTurno

Tipo entità: *debole*

Primary Key: **CodTurno, CodPersona, Data**

------

### Periodo Temporale

Primary Key: **Cod**

Attributi

* Data inizio
* Data fine

------

### Rendicontazione

Tipo entità: *debole*

Primary Key: **Anno, Trimestre**

Attributi

* Spesa

------

## Relazioni

### Tipologia

* Gara (1, 1)
* Disciplina (0, N)

------

### Partecipa

* Partecipazione (1, 1)
* Gara (0, N)

------

### Tramite

* Atleta (0, N)
* Partecipazione (1, 1)

------

### Esegue

* SvolgimentoTurno (1, 1)
* Persona (0, N)

------

### VieneEseguito

* SvolgimentoTurno (1, 1)
* Turno (0, N)

------

### PossiedeBadge

* Persona (1, 1)
* Badge (0, N)

------

### PermetteAccesso

* Badge (1, N)
* Zona (0, N)
