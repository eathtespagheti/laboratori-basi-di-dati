# Progettazione Logica

## Entità

I campi in grassetto e corsivo indicano la chiave primaria

* **Persona** (***Cod***, Nome, Cognome, Nascita, Nazionalità, Carica, Cellulare)
* **Badge** (***Cod***)
* **Zona** (***NomeZona***, CodBadge)
* **Disciplina** (***Cod***, Nome, Record, DataRecord)
* **Gara**  (***CodDisciplina, Turno, NumeroBatteria***, Data, Ora)
* **Partecipazione** (***CodPersona, CodDisciplina, Turno, NumeroBatteria***, Nome, DataAssunzione)
* **Sito**  (***Cod***, Indirizzo)
* **PartecipazioneAttività** (***Matricola*, *CodProgetto*, *CodAttività*, *Data Inizio***, DataInizio, DataFine, OreLavoro)
* **Revisore** (***Cod***, Nome, Titolo)
* **Revisione** (***CodRevisore, CodProgetto, CodAttività, CodPeriodo***, Cod, Giudizio)
* **PeriodoTemporale** (***Cod***, DataInizio, DataFine)
* **Rendicontazione** (***Anno, Trimestre, CodProgetto***, Spesa)

### Multivalore trasformati in relazioni

* **Presentato** (***CodCongresso***, ***CodAttività***)
* **Richiede** (***CodComponente***, ***CodRequisito***)
* **PossiedeSito** (***Matricola***, ***CodSito***)

### Relazioni trasformate in entità

* **ComponentiSoftware** (***CodProgetto, CodAttività, CodComponente***)
