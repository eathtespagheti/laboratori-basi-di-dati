DROP TABLE IF EXISTS ragazzo CASCADE;
DROP TABLE IF EXISTS attività CASCADE;
DROP TABLE IF EXISTS campo_estivo CASCADE;
DROP TABLE IF EXISTS iscrizione_per_attività_in_campo_estivo CASCADE;
CREATE TABLE ragazzo (
  codfiscale CHAR(16) PRIMARY KEY,
  nome VARCHAR(200) NOT NULL,
  cognome VARCHAR(200) NOT NULL,
  datanascita DATE NOT NULL,
  cittàresidenza VARCHAR(200) NOT NULL
);
CREATE TABLE attività (
  codattività SMALLINT PRIMARY KEY,
  nome VARCHAR(200) NOT NULL,
  descrizione VARCHAR(500) NOT NULL,
  categoria VARCHAR(200) NOT NULL
);
CREATE TABLE campo_estivo (
  codcampo SMALLINT PRIMARY KEY,
  nomecampo VARCHAR(200) NOT NULL,
  città VARCHAR(200) NOT NULL
);
CREATE TABLE iscrizione_per_attività_in_campo_estivo (
  codfiscale CHAR(16) NOT NULL,
  codattività SMALLINT NOT NULL,
  codcampo SMALLINT NOT NULL,
  dataiscrizione DATE NOT NULL,
  PRIMARY KEY (
    codfiscale,
    codattività,
    codcampo,
    dataiscrizione
  ),
  FOREIGN KEY (codfiscale) REFERENCES ragazzo,
  FOREIGN KEY (codattività) REFERENCES attività,
  FOREIGN KEY (codcampo) REFERENCES campo_estivo
);
/*
Data for ragazzo
*/
INSERT INTO "ragazzo" (
    "codfiscale",
    "nome",
    "cognome",
    "datanascita",
    "cittàresidenza"
  )
VALUES
  (
    'YHKWJJ38E54X808X',
    'Kimball',
    'Leebeter',
    '2004/01/02',
    'Ujung'
  ),
  (
    'NNZNUU28X12S243O',
    'Leif',
    'Algie',
    '2005/03/18',
    'Landskrona'
  ),
  (
    'LQENJZ10X37D528H',
    'Chick',
    'Blasli',
    '2005/05/05',
    'Nysa'
  ),
  (
    'CTLBRQ36G52P402I',
    'Gwenora',
    'Griffen',
    '2000/07/12',
    'Curahpacul Satu'
  ),
  (
    'SDKGZD33T27D595D',
    'Nicol',
    'MacEvilly',
    '2002/10/12',
    'Sīlat al Ḩārithīyah'
  ),
  (
    'ZINCHQ68G16Q241P',
    'Katrina',
    'Seid',
    '1999/11/17',
    'Pytalovo'
  ),
  (
    'VLSZIW89F67F410S',
    'Janaye',
    'Kondrachenko',
    '2005/03/07',
    'Bagarmossen'
  ),
  (
    'HHARED79H66J944S',
    'Aldrich',
    'Kincade',
    '1996/07/28',
    'Kayakent'
  ),
  (
    'IPMBZS65V32M583U',
    'Pancho',
    'O''Flaherty',
    '2001/04/01',
    'Bulqizë'
  ),
  (
    'TLOWRQ58C89E833N',
    'Hayden',
    'Francecione',
    '2004/04/15',
    'Madīnat ‘Īsá'
  ),
  (
    'OVBQSO71K30C901Q',
    'Celene',
    'Mylan',
    '2004/08/25',
    'Kihancha'
  ),
  (
    'LGUESE80L19B242N',
    'Carol',
    'Daldan',
    '1997/08/29',
    'Presidente Bernardes'
  ),
  (
    'ZSMVAB91P68Y600Y',
    'Rhianon',
    'Coad',
    '2005/05/18',
    'Al Muţayrifī'
  ),
  (
    'VKBHNW23H60S951F',
    'Joellyn',
    'Gething',
    '1996/04/03',
    'Dzikowiec'
  ),
  (
    'PQEKHU75L88Z322U',
    'Lorrin',
    'Barnsdall',
    '2001/02/28',
    'Sandefjord'
  ),
  (
    'CLVIKY77Q51B860C',
    'Karalynn',
    'Waight',
    '1997/06/19',
    'Vathýlakkos'
  ),
  (
    'GFMPKS27P22B473G',
    'Chico',
    'Jaggers',
    '2002/05/11',
    'Valky'
  ),
  (
    'RDGXSQ52T80U346W',
    'Hagan',
    'Breslin',
    '1996/07/10',
    'Az Ziyārah'
  ),
  (
    'OOOTSA88X39U356X',
    'Jimmy',
    'Yokelman',
    '2002/01/08',
    'Huanggu'
  ),
  (
    'VRTAEL19L64M897F',
    'Annabella',
    'Messruther',
    '2005/06/18',
    'Póvoa'
  ),
  (
    'FEMWPY42Y80Y214S',
    'Eudora',
    'Dibbert',
    '1996/11/18',
    'Dongshandi'
  ),
  (
    'OTGHQH42T64P140C',
    'Benedick',
    'Fairhurst',
    '2002/10/18',
    'Härnösand'
  ),
  (
    'NCQIEQ15K76F302F',
    'Gary',
    'Bruton',
    '2003/10/09',
    'Hidalgo'
  ),
  (
    'OGZBHQ45I70G670Q',
    'Ramona',
    'Stenyng',
    '2002/04/29',
    'Nantian'
  ),
  (
    'ZOLJYR23U75I232W',
    'Garrard',
    'Stonnell',
    '2001/06/01',
    'Guadalupe'
  ),
  (
    'SQIAKF72Y93A420M',
    'Caty',
    'Conley',
    '1996/11/15',
    'Yakymivka'
  ),
  (
    'YCGIXY95J48I551Y',
    'Stanfield',
    'Krollman',
    '2002/07/06',
    'Sidowayah Lor'
  ),
  (
    'LRFDDW52O75X548L',
    'Jemie',
    'Matussov',
    '1995/04/14',
    'Banjar Tibubiyu Kaja'
  ),
  (
    'TSEEGK60N55E000I',
    'Chickie',
    'Fransson',
    '1999/02/11',
    'Qiaochong'
  ),
  (
    'RJUFUQ56Z30K256V',
    'Curcio',
    'Tregensoe',
    '1998/06/24',
    'Tegalgede'
  ),
  (
    'XCJWAN70I55Y672Q',
    'Nichole',
    'Cabedo',
    '1999/03/26',
    'Kudanding'
  ),
  (
    'XIABWK07C68U956W',
    'Zabrina',
    'Beechcraft',
    '2003/09/10',
    'Bobigny'
  ),
  (
    'BQQJPG32W96H483Y',
    'Ciro',
    'Layland',
    '2000/02/23',
    'Pleszew'
  ),
  (
    'KEWGZP92F56O006B',
    'Marcie',
    'Portugal',
    '2001/01/31',
    'Xuhui'
  ),
  (
    'WBUOGV71C24I353R',
    'Juliana',
    'Cardow',
    '1997/12/01',
    'Maninihon'
  ),
  (
    'ZQGVCK14S29C052K',
    'Stephie',
    'Vasse',
    '2003/06/21',
    'Turan'
  ),
  (
    'USXKWK64P05T689I',
    'Florida',
    'Liversage',
    '1998/09/07',
    'Palaiochóri'
  ),
  (
    'TOLXJJ56Y06R612Q',
    'Yoko',
    'Berkeley',
    '1999/03/13',
    'Liushan'
  ),
  (
    'EOKMPI18M70X819C',
    'Terry',
    'Bealton',
    '2000/09/09',
    'Kadaka'
  ),
  (
    'XTKWFH67K23M759B',
    'Hilarius',
    'Alleyn',
    '1999/09/30',
    'Serednye Vodyane'
  ),
  (
    'AQDBNI50W95P264G',
    'Doroteya',
    'Dalli',
    '2002/09/10',
    'Xuanbao'
  ),
  (
    'MPYRTU30W74Z913N',
    'Lark',
    'Oldcote',
    '1998/09/07',
    'Thul'
  ),
  (
    'EUCZCN20C22W554T',
    'Merissa',
    'Wigglesworth',
    '1995/11/24',
    'Dois Portos'
  ),
  (
    'SEGDFN40X35P031S',
    'Marcellina',
    'Dozdill',
    '1995/07/31',
    'Vimieiro'
  ),
  (
    'MNJNAZ51K45B109D',
    'Celka',
    'Tyrone',
    '2005/02/17',
    'Columbeira'
  ),
  (
    'QVCZNY42E34D688X',
    'Hertha',
    'Roderham',
    '2000/01/02',
    'Taizi'
  ),
  (
    'PTOQPY06Q41K952K',
    'Lyndel',
    'De Giorgi',
    '1995/05/08',
    'Yelwa'
  ),
  (
    'YQTHJT13X11Z504U',
    'Molli',
    'Cookney',
    '2000/02/12',
    'Santo Niño'
  ),
  (
    'ZYSNFF09T01V419Z',
    'Nicol',
    'Stace',
    '2005/02/05',
    'Tongquan'
  ),
  (
    'GWWVAJ19G89C894B',
    'Brigid',
    'McCart',
    '2002/04/24',
    'Chiriguaná'
  ),
  (
    'XQZNZT88G40U962D',
    'Sunny',
    'Palister',
    '2005/10/28',
    'Songjianghe'
  ),
  (
    'LBLWJN49K33U650E',
    'Ferd',
    'Durden',
    '2001/08/05',
    'Sophia Antipolis'
  ),
  (
    'HCSQMO65I24Z817A',
    'Filia',
    'Crumpe',
    '2003/06/29',
    'Schiedam postbusnummers'
  ),
  (
    'JMRRTQ47I38T582Z',
    'Orv',
    'Cuttles',
    '1996/07/14',
    'Jolo'
  ),
  (
    'EZKUWY53I63J761E',
    'Ilka',
    'Bakewell',
    '1997/12/14',
    'Dumandesa'
  ),
  (
    'HJSUMR71Q98Q339J',
    'Inglis',
    'Alsford',
    '2004/01/11',
    'Bogotá'
  ),
  (
    'ENHCER63F82Y177V',
    'Perri',
    'O''Tierney',
    '2001/05/23',
    'Nepalgunj'
  ),
  (
    'ORSTSS31P31Y104Y',
    'Berkie',
    'Lyburn',
    '1997/10/01',
    'Palilula'
  ),
  (
    'OBTTVM87L51Z692Z',
    'Wendi',
    'Okenfold',
    '2003/06/11',
    'Nantes'
  ),
  (
    'FLMPZB77R57T465B',
    'Frankie',
    'Overington',
    '1995/05/22',
    'Sabbah'
  ),
  (
    'GGMDLR52U26C879L',
    'Crosby',
    'Lowerson',
    '2003/03/20',
    'Baie-D''Urfé'
  ),
  (
    'MBKSMC17M77C623E',
    'Werner',
    'Easbie',
    '1996/10/16',
    'Mīzan Teferī'
  ),
  (
    'VPFBBL26B64D093I',
    'Nikolos',
    'Tithecote',
    '1999/12/24',
    'Walakeri'
  ),
  (
    'SZCRKZ21A66Y000I',
    'Albertina',
    'Duce',
    '2005/02/24',
    'Terangun'
  ),
  (
    'PNHRZY51K57T045M',
    'Zola',
    'Brownlie',
    '1999/11/07',
    'Primorsko'
  ),
  (
    'EMSXGX19U73Y018Q',
    'Halsey',
    'Grayham',
    '1995/11/04',
    'Mohoro'
  ),
  (
    'RKHKIZ04F23T048L',
    'Arvie',
    'Bea',
    '1998/11/10',
    'Sasar'
  ),
  (
    'IIMGLY19Z81U011J',
    'Joseph',
    'Robez',
    '1995/12/18',
    'Cruz'
  ),
  (
    'IJOPKY15Y52Z111S',
    'Ulric',
    'Loney',
    '1997/03/10',
    'Węgliniec'
  ),
  (
    'IRMTXD97T02V003V',
    'Lew',
    'Giraudot',
    '1999/06/25',
    'El Copey'
  ),
  (
    'CPZKFW33W23B864Y',
    'Ragnar',
    'Endley',
    '2003/12/25',
    'Satita'
  ),
  (
    'BPDOYJ05F83A048V',
    'Dasie',
    'Duffin',
    '1995/04/29',
    'Olival Basto'
  ),
  (
    'YKNFUW90F91N043S',
    'Lyman',
    'Tohill',
    '2005/07/24',
    'Krasnohrad'
  ),
  (
    'KGJRDH88C95S309R',
    'Joannes',
    'Crosi',
    '1998/03/23',
    'El Bálsamo'
  ),
  (
    'UXRYEF75F99X304P',
    'Leela',
    'Berling',
    '1996/08/22',
    'Angered'
  ),
  (
    'RIAUKA68G97P974D',
    'Lief',
    'Empringham',
    '2000/06/20',
    'Asopía'
  ),
  (
    'YHEZDA01G99S628D',
    'Raphael',
    'Rosewarne',
    '2001/06/29',
    'El Paso'
  ),
  (
    'VLXTYJ37E95C042S',
    'Naoma',
    'Yeldon',
    '2003/01/28',
    'Yunmen'
  ),
  (
    'NDXNEL65D00V610K',
    'Bobina',
    'McIlharga',
    '2003/06/28',
    'Salto'
  ),
  (
    'IFNUON73U42X110K',
    'Sylvan',
    'Gladyer',
    '1996/01/30',
    'Grzęska'
  ),
  (
    'OEJIKD12Z59V728U',
    'Aubrie',
    'Amori',
    '1997/01/10',
    'Dongshan'
  ),
  (
    'QEFHMI58C36L308U',
    'Liesa',
    'Haysom',
    '2004/07/18',
    'Panxi'
  ),
  (
    'JJRVFG97K19I543P',
    'Tamara',
    'Feldberg',
    '1999/09/27',
    'Binzhou'
  ),
  (
    'VWTBVB67H26J932O',
    'Charmain',
    'Greenlees',
    '2000/08/25',
    'Düsseldorf'
  ),
  (
    'LTIYZN66B36I743M',
    'Boonie',
    'Dursley',
    '2000/09/26',
    'Bakovci'
  ),
  (
    'YLJKYG88W20C316Z',
    'Paule',
    'Anderton',
    '1997/12/16',
    'Espinheira'
  ),
  (
    'TCJWPJ41Q64B896K',
    'Jeddy',
    'Haryngton',
    '2001/06/14',
    'Issoire'
  ),
  (
    'FPJYGA51I12Z671B',
    'Alden',
    'Isacke',
    '2002/12/17',
    'Barili'
  ),
  (
    'YVXTTE91H89W246D',
    'Bond',
    'Rodmell',
    '1996/02/10',
    'Betong'
  ),
  (
    'CZKEQU58R55A910I',
    'Lennard',
    'Garaghan',
    '1999/06/03',
    'Mafeteng'
  ),
  (
    'IFDNWI07R60M980D',
    'Twila',
    'Orht',
    '2002/08/24',
    'Herīs'
  ),
  (
    'DPUZSV45E98L322Z',
    'Ania',
    'Fley',
    '1999/05/02',
    'Makiwalo'
  ),
  (
    'TTEBSW38G74U733H',
    'Neile',
    'Saundercock',
    '1997/02/19',
    'Maoshan'
  ),
  (
    'QFYJGL54G37N125Y',
    'Eddie',
    'Friberg',
    '1996/08/24',
    'Chiang Yuen'
  ),
  (
    'HIHDLS06K42B080V',
    'King',
    'Timmis',
    '2002/02/13',
    'Fukuroi'
  ),
  (
    'VZZCJN28A11Q979W',
    'Moll',
    'Dallmann',
    '2001/03/05',
    'Talavera'
  ),
  (
    'VTWDOM11F00K287F',
    'Skipton',
    'Marrow',
    '2000/11/26',
    'Spirovo'
  ),
  (
    'MDEVHV23U54F992P',
    'Jud',
    'Gude',
    '1998/06/19',
    'Kremenki'
  ),
  (
    'ORIORF06F56T308P',
    'Hoyt',
    'Smeath',
    '1999/04/25',
    'Tarnogskiy Gorodok'
  ),
  (
    'PWMZOT50Z96W342N',
    'Ninette',
    'Cattach',
    '1997/01/06',
    'Corral de Bustos'
  );
  /*
    Data for attività
    */
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    1,
    'CgFX',
    'MRI of Bi Renal Art using Oth Contrast, Unenh, Enhance',
    'Mauv'
  );
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    2,
    'MDB',
    'Removal of Nonaut Sub FROM R Humeral Head, Open Approach',
    'Crimson'
  );
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    3,
    'BSI Tax Factory',
    'Inspection of Neck, Open Approach',
    'Crimson'
  );
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    4,
    'Sleep Apnea',
    'Fusion T-lum Jt w Autol Sub, Ant Appr A Col, Perc Endo',
    'Maroon'
  );
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    5,
    'WMI',
    'Revision of INT Fix IN R Femur Shaft, Open Approach',
    'Blue'
  );
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    6,
    'Behavior Management',
    'Phys Rehab & Diag Audiology, Rehab, Motor Trmt',
    'Violet'
  );
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    7,
    'Ion Chromatography',
    'Introduce Nonaut Fertilized Ovum IN Fem Reprod, Perc',
    'Mauv'
  );
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    8,
    'CVS',
    'Beam Radiation of Brain using Photons >10 MeV',
    'Blue'
  );
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    9,
    'Frame Relay',
    'Plain Radiography of Right Ankle using Low Osmolar Contrast',
    'Orange'
  );
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    10,
    'Kinesiology',
    'Repair Common Bile Duct, Open Approach',
    'Crimson'
  );
  /*
        Data fot campo_estivo
        */
INSERT INTO campo_estivo (codcampo, nomecampo, città)
VALUES
  (1, 'November', 'Kibara');
INSERT INTO campo_estivo (codcampo, nomecampo, città)
VALUES
  (2, 'Alfa', 'Kibara');
INSERT INTO campo_estivo (codcampo, nomecampo, città)
VALUES
  (3, 'Charlie', 'Kibara');
INSERT INTO campo_estivo (codcampo, nomecampo, città)
VALUES
  (4, 'India', 'Manonjaya');
INSERT INTO campo_estivo (codcampo, nomecampo, città)
VALUES
  (5, 'Echo', 'Manonjaya');
INSERT INTO campo_estivo (codcampo, nomecampo, città)
VALUES
  (6, 'Victor', 'Verkhnyaya Tura');
INSERT INTO campo_estivo (codcampo, nomecampo, città)
VALUES
  (7, 'Echo', 'Verkhnyaya Tura');
  /*
    Data for iscrizione_per_attività_in_campo_estivo
    */
INSERT INTO "iscrizione_per_attività_in_campo_estivo" (
    "codfiscale",
    "codattività",
    "codcampo",
    "dataiscrizione"
  )
VALUES
  ('TCJWPJ41Q64B896K', 8, 4, '2012/02/13'),
  ('EUCZCN20C22W554T', 8, 1, '2012/09/18'),
  ('EZKUWY53I63J761E', 5, 1, '2012/01/17'),
  ('BQQJPG32W96H483Y', 6, 4, '2012/11/26'),
  ('XIABWK07C68U956W', 7, 4, '2012/10/19'),
  ('LGUESE80L19B242N', 9, 6, '2012/10/29'),
  ('EOKMPI18M70X819C', 2, 6, '2012/07/29'),
  ('OBTTVM87L51Z692Z', 2, 5, '2012/12/07'),
  ('EZKUWY53I63J761E', 2, 5, '2012/10/01'),
  ('IPMBZS65V32M583U', 8, 7, '2012/09/13'),
  ('GWWVAJ19G89C894B', 8, 4, '2012/02/06'),
  ('SQIAKF72Y93A420M', 3, 6, '2012/05/09'),
  ('CPZKFW33W23B864Y', 4, 5, '2012/06/03'),
  ('GGMDLR52U26C879L', 9, 6, '2012/11/01'),
  ('OOOTSA88X39U356X', 9, 7, '2012/07/30'),
  ('RDGXSQ52T80U346W', 4, 1, '2012/09/11'),
  ('SZCRKZ21A66Y000I', 8, 5, '2012/03/19'),
  ('FLMPZB77R57T465B', 7, 4, '2012/01/10'),
  ('HCSQMO65I24Z817A', 3, 3, '2012/02/25'),
  ('LQENJZ10X37D528H', 4, 5, '2012/08/06'),
  ('IFNUON73U42X110K', 10, 7, '2012/07/30'),
  ('TOLXJJ56Y06R612Q', 1, 7, '2012/11/01'),
  ('YCGIXY95J48I551Y', 9, 3, '2012/07/17'),
  ('TLOWRQ58C89E833N', 6, 2, '2012/09/09'),
  ('QEFHMI58C36L308U', 2, 6, '2012/05/26'),
  ('OOOTSA88X39U356X', 9, 5, '2012/07/20'),
  ('CLVIKY77Q51B860C', 5, 4, '2012/07/11'),
  ('EMSXGX19U73Y018Q', 4, 6, '2012/09/01'),
  ('VKBHNW23H60S951F', 3, 1, '2012/11/12'),
  ('ZINCHQ68G16Q241P', 2, 1, '2012/06/24'),
  ('CLVIKY77Q51B860C', 1, 4, '2012/03/24'),
  ('LQENJZ10X37D528H', 7, 3, '2012/08/10'),
  ('LBLWJN49K33U650E', 10, 5, '2012/06/17'),
  ('RJUFUQ56Z30K256V', 3, 6, '2012/11/01'),
  ('RJUFUQ56Z30K256V', 10, 7, '2012/09/24'),
  ('ZQGVCK14S29C052K', 7, 7, '2012/04/16'),
  ('ORIORF06F56T308P', 5, 5, '2012/06/24'),
  ('LBLWJN49K33U650E', 10, 2, '2012/02/10'),
  ('YHEZDA01G99S628D', 7, 6, '2012/08/01'),
  ('HIHDLS06K42B080V', 6, 3, '2012/10/05'),
  ('OBTTVM87L51Z692Z', 5, 1, '2012/07/14'),
  ('XCJWAN70I55Y672Q', 8, 2, '2012/04/30'),
  ('USXKWK64P05T689I', 6, 6, '2012/01/19'),
  ('IJOPKY15Y52Z111S', 7, 4, '2012/06/17'),
  ('YKNFUW90F91N043S', 10, 6, '2012/03/21'),
  ('TTEBSW38G74U733H', 8, 1, '2012/09/28'),
  ('JJRVFG97K19I543P', 2, 7, '2012/12/11'),
  ('TCJWPJ41Q64B896K', 10, 7, '2012/09/28'),
  ('CPZKFW33W23B864Y', 5, 1, '2012/03/28'),
  ('LGUESE80L19B242N', 7, 2, '2012/05/01'),
  ('OOOTSA88X39U356X', 6, 2, '2012/01/06'),
  ('TCJWPJ41Q64B896K', 10, 6, '2012/06/21'),
  ('XIABWK07C68U956W', 9, 2, '2012/09/28'),
  ('IFNUON73U42X110K', 5, 4, '2012/08/25'),
  ('CLVIKY77Q51B860C', 6, 4, '2012/06/29'),
  ('VKBHNW23H60S951F', 6, 6, '2012/06/20'),
  ('LQENJZ10X37D528H', 3, 4, '2012/10/24'),
  ('PQEKHU75L88Z322U', 7, 3, '2012/07/06'),
  ('AQDBNI50W95P264G', 1, 7, '2012/12/03'),
  ('XQZNZT88G40U962D', 10, 7, '2012/07/23'),
  ('VKBHNW23H60S951F', 2, 5, '2012/08/02'),
  ('USXKWK64P05T689I', 3, 1, '2012/06/12'),
  ('GFMPKS27P22B473G', 3, 2, '2012/04/16'),
  ('OGZBHQ45I70G670Q', 8, 4, '2012/08/14'),
  ('OGZBHQ45I70G670Q', 8, 6, '2012/11/22'),
  ('HIHDLS06K42B080V', 10, 5, '2012/03/27'),
  ('EUCZCN20C22W554T', 2, 2, '2012/10/17'),
  ('JJRVFG97K19I543P', 2, 2, '2012/10/11'),
  ('ORSTSS31P31Y104Y', 1, 6, '2012/07/22'),
  ('IPMBZS65V32M583U', 8, 2, '2012/02/14'),
  ('BQQJPG32W96H483Y', 10, 6, '2012/04/29'),
  ('ZOLJYR23U75I232W', 7, 6, '2012/04/22'),
  ('BPDOYJ05F83A048V', 4, 6, '2012/02/17'),
  ('USXKWK64P05T689I', 8, 1, '2012/10/26'),
  ('ENHCER63F82Y177V', 2, 5, '2012/02/12'),
  ('AQDBNI50W95P264G', 4, 4, '2012/12/16'),
  ('IRMTXD97T02V003V', 8, 1, '2012/03/14'),
  ('EUCZCN20C22W554T', 2, 6, '2012/07/29'),
  ('IFNUON73U42X110K', 9, 4, '2012/06/23'),
  ('VTWDOM11F00K287F', 2, 2, '2012/12/02'),
  ('VKBHNW23H60S951F', 5, 4, '2012/10/31'),
  ('FEMWPY42Y80Y214S', 1, 2, '2012/06/19'),
  ('VPFBBL26B64D093I', 5, 6, '2012/04/12'),
  ('JJRVFG97K19I543P', 8, 6, '2012/09/19'),
  ('JMRRTQ47I38T582Z', 1, 6, '2012/02/08'),
  ('OBTTVM87L51Z692Z', 8, 4, '2012/11/18'),
  ('KEWGZP92F56O006B', 6, 1, '2012/01/12'),
  ('XQZNZT88G40U962D', 1, 3, '2012/05/15'),
  ('RDGXSQ52T80U346W', 1, 3, '2012/08/03'),
  ('YCGIXY95J48I551Y', 3, 4, '2012/08/29'),
  ('GWWVAJ19G89C894B', 8, 6, '2012/12/06'),
  ('EOKMPI18M70X819C', 2, 6, '2012/10/12'),
  ('IPMBZS65V32M583U', 5, 6, '2012/06/15'),
  ('NCQIEQ15K76F302F', 5, 6, '2012/11/17'),
  ('LGUESE80L19B242N', 10, 2, '2012/04/22'),
  ('XIABWK07C68U956W', 1, 4, '2012/05/21'),
  ('XQZNZT88G40U962D', 7, 2, '2012/05/19'),
  ('SZCRKZ21A66Y000I', 5, 4, '2012/08/09'),
  ('VZZCJN28A11Q979W', 1, 5, '2012/05/24'),
  ('ZINCHQ68G16Q241P', 9, 1, '2012/11/26'),
  ('LTIYZN66B36I743M', 2, 7, '2012/08/22'),
  ('PQEKHU75L88Z322U', 1, 4, '2012/11/12'),
  ('YHKWJJ38E54X808X', 3, 1, '2012/02/05'),
  ('UXRYEF75F99X304P', 1, 7, '2012/05/15'),
  ('CLVIKY77Q51B860C', 3, 2, '2012/12/17'),
  ('LBLWJN49K33U650E', 7, 1, '2012/11/20'),
  ('EZKUWY53I63J761E', 3, 1, '2012/06/24'),
  ('EOKMPI18M70X819C', 3, 6, '2012/11/06'),
  ('FEMWPY42Y80Y214S', 4, 2, '2012/10/07'),
  ('VZZCJN28A11Q979W', 1, 4, '2012/01/24'),
  ('OTGHQH42T64P140C', 7, 1, '2012/10/09'),
  ('NDXNEL65D00V610K', 2, 6, '2012/07/19'),
  ('HJSUMR71Q98Q339J', 2, 5, '2012/09/18'),
  ('SQIAKF72Y93A420M', 2, 4, '2012/02/08'),
  ('JMRRTQ47I38T582Z', 2, 7, '2012/12/24'),
  ('PNHRZY51K57T045M', 10, 5, '2012/09/07'),
  ('GGMDLR52U26C879L', 2, 6, '2012/08/07'),
  ('RJUFUQ56Z30K256V', 6, 6, '2012/11/09'),
  ('OBTTVM87L51Z692Z', 10, 1, '2012/10/19'),
  ('YHEZDA01G99S628D', 8, 6, '2012/05/06'),
  ('TLOWRQ58C89E833N', 7, 2, '2012/08/28'),
  ('FEMWPY42Y80Y214S', 3, 3, '2012/01/12'),
  ('SZCRKZ21A66Y000I', 10, 2, '2012/12/02'),
  ('IFDNWI07R60M980D', 2, 3, '2012/09/05'),
  ('ZOLJYR23U75I232W', 4, 3, '2012/09/04'),
  ('HCSQMO65I24Z817A', 5, 6, '2012/11/17'),
  ('GFMPKS27P22B473G', 7, 4, '2012/12/28'),
  ('USXKWK64P05T689I', 3, 4, '2012/12/07'),
  ('ZQGVCK14S29C052K', 3, 5, '2012/02/21'),
  ('XCJWAN70I55Y672Q', 10, 7, '2012/12/11'),
  ('CZKEQU58R55A910I', 7, 3, '2012/04/14'),
  ('USXKWK64P05T689I', 2, 7, '2012/02/19'),
  ('BPDOYJ05F83A048V', 10, 7, '2012/05/29'),
  ('RDGXSQ52T80U346W', 10, 4, '2012/12/19'),
  ('VLSZIW89F67F410S', 5, 4, '2012/08/12'),
  ('OEJIKD12Z59V728U', 6, 3, '2012/10/16'),
  ('ZYSNFF09T01V419Z', 4, 5, '2012/01/17'),
  ('VKBHNW23H60S951F', 4, 4, '2012/03/02'),
  ('YVXTTE91H89W246D', 7, 7, '2012/09/21'),
  ('CPZKFW33W23B864Y', 9, 4, '2012/07/09'),
  ('MBKSMC17M77C623E', 9, 5, '2012/01/21'),
  ('KEWGZP92F56O006B', 2, 6, '2012/12/14'),
  ('QEFHMI58C36L308U', 2, 7, '2012/05/09'),
  ('YLJKYG88W20C316Z', 2, 4, '2012/02/29'),
  ('RIAUKA68G97P974D', 10, 7, '2012/03/20'),
  ('KEWGZP92F56O006B', 5, 3, '2012/11/19'),
  ('MBKSMC17M77C623E', 7, 5, '2012/12/01'),
  ('VRTAEL19L64M897F', 4, 3, '2012/10/28'),
  ('TLOWRQ58C89E833N', 8, 4, '2012/05/30'),
  ('PWMZOT50Z96W342N', 6, 7, '2012/09/06'),
  ('HHARED79H66J944S', 3, 3, '2012/10/09'),
  ('LQENJZ10X37D528H', 5, 4, '2012/10/07'),
  ('OEJIKD12Z59V728U', 10, 2, '2012/06/07'),
  ('XCJWAN70I55Y672Q', 10, 3, '2012/12/02'),
  ('IJOPKY15Y52Z111S', 7, 6, '2012/08/08'),
  ('XCJWAN70I55Y672Q', 6, 6, '2012/04/15'),
  ('QFYJGL54G37N125Y', 4, 2, '2012/01/24'),
  ('KGJRDH88C95S309R', 5, 5, '2012/03/07'),
  ('TCJWPJ41Q64B896K', 8, 1, '2012/09/07'),
  ('OGZBHQ45I70G670Q', 10, 7, '2012/04/15'),
  ('OTGHQH42T64P140C', 5, 5, '2012/11/01'),
  ('CTLBRQ36G52P402I', 9, 7, '2012/06/15'),
  ('FEMWPY42Y80Y214S', 1, 2, '2012/07/11'),
  ('PTOQPY06Q41K952K', 1, 2, '2012/05/23'),
  ('QVCZNY42E34D688X', 9, 4, '2012/10/15'),
  ('LTIYZN66B36I743M', 4, 1, '2012/04/03'),
  ('MBKSMC17M77C623E', 9, 7, '2012/10/15'),
  ('XQZNZT88G40U962D', 4, 4, '2012/07/29'),
  ('YLJKYG88W20C316Z', 1, 6, '2012/01/29'),
  ('ZINCHQ68G16Q241P', 7, 6, '2012/12/09'),
  ('YLJKYG88W20C316Z', 10, 5, '2012/10/16'),
  ('NDXNEL65D00V610K', 1, 3, '2012/08/16'),
  ('SZCRKZ21A66Y000I', 3, 1, '2012/10/05'),
  ('FEMWPY42Y80Y214S', 7, 2, '2012/08/11'),
  ('MNJNAZ51K45B109D', 8, 4, '2012/06/29'),
  ('ORSTSS31P31Y104Y', 3, 5, '2012/06/12'),
  ('DPUZSV45E98L322Z', 8, 6, '2012/09/29'),
  ('QEFHMI58C36L308U', 1, 4, '2012/08/21'),
  ('YVXTTE91H89W246D', 6, 6, '2012/11/29'),
  ('VZZCJN28A11Q979W', 2, 7, '2012/05/24'),
  ('GFMPKS27P22B473G', 4, 3, '2012/11/20'),
  ('HJSUMR71Q98Q339J', 4, 4, '2012/05/18'),
  ('VLXTYJ37E95C042S', 4, 7, '2012/05/11'),
  ('DPUZSV45E98L322Z', 5, 6, '2012/11/21'),
  ('YKNFUW90F91N043S', 3, 3, '2012/02/28'),
  ('OBTTVM87L51Z692Z', 7, 2, '2012/06/13'),
  ('EZKUWY53I63J761E', 1, 2, '2012/02/05'),
  ('IFDNWI07R60M980D', 2, 2, '2012/03/24'),
  ('CPZKFW33W23B864Y', 4, 1, '2012/03/23'),
  ('XIABWK07C68U956W', 1, 5, '2012/11/01'),
  ('ZOLJYR23U75I232W', 1, 5, '2012/09/02'),
  ('MPYRTU30W74Z913N', 6, 5, '2012/03/26'),
  ('XTKWFH67K23M759B', 10, 4, '2012/12/18'),
  ('SQIAKF72Y93A420M', 3, 4, '2012/10/23'),
  ('OTGHQH42T64P140C', 10, 6, '2012/06/10'),
  ('IPMBZS65V32M583U', 1, 7, '2012/08/11'),
  ('VWTBVB67H26J932O', 9, 2, '2012/04/07'),
  ('YQTHJT13X11Z504U', 4, 3, '2012/07/26'),
  ('TOLXJJ56Y06R612Q', 7, 3, '2012/11/14'),
  ('NCQIEQ15K76F302F', 3, 3, '2012/01/27'),
  ('OOOTSA88X39U356X', 7, 2, '2012/02/27'),
  ('GFMPKS27P22B473G', 8, 2, '2012/12/23'),
  ('EUCZCN20C22W554T', 4, 3, '2012/08/26'),
  ('SZCRKZ21A66Y000I', 10, 1, '2012/10/10'),
  ('SQIAKF72Y93A420M', 9, 5, '2012/04/20'),
  ('YLJKYG88W20C316Z', 1, 5, '2012/02/20'),
  ('BQQJPG32W96H483Y', 3, 5, '2012/05/09'),
  ('SEGDFN40X35P031S', 2, 3, '2012/07/18'),
  ('AQDBNI50W95P264G', 1, 1, '2012/10/26'),
  ('OGZBHQ45I70G670Q', 4, 2, '2012/09/08'),
  ('CLVIKY77Q51B860C', 3, 5, '2012/03/25'),
  ('EMSXGX19U73Y018Q', 10, 2, '2012/01/06'),
  ('TTEBSW38G74U733H', 2, 4, '2012/08/06'),
  ('VLSZIW89F67F410S', 3, 3, '2012/02/09'),
  ('QFYJGL54G37N125Y', 8, 6, '2012/01/17'),
  ('YLJKYG88W20C316Z', 7, 3, '2012/09/23'),
  ('OVBQSO71K30C901Q', 1, 4, '2012/03/13'),
  ('TOLXJJ56Y06R612Q', 7, 2, '2012/05/24'),
  ('ZQGVCK14S29C052K', 1, 6, '2012/08/20'),
  ('YLJKYG88W20C316Z', 6, 5, '2012/06/09'),
  ('VWTBVB67H26J932O', 1, 7, '2012/03/16'),
  ('JMRRTQ47I38T582Z', 9, 7, '2012/06/18'),
  ('EZKUWY53I63J761E', 4, 3, '2012/11/19'),
  ('LGUESE80L19B242N', 7, 7, '2012/09/27'),
  ('SZCRKZ21A66Y000I', 2, 1, '2012/06/11'),
  ('XIABWK07C68U956W', 2, 6, '2012/01/31'),
  ('EMSXGX19U73Y018Q', 2, 1, '2012/04/29'),
  ('OBTTVM87L51Z692Z', 7, 2, '2012/05/06'),
  ('OBTTVM87L51Z692Z', 3, 2, '2012/11/21'),
  ('VKBHNW23H60S951F', 3, 6, '2012/04/19'),
  ('EOKMPI18M70X819C', 3, 4, '2012/06/14'),
  ('YHKWJJ38E54X808X', 10, 7, '2012/12/23'),
  ('PTOQPY06Q41K952K', 3, 3, '2012/08/03'),
  ('PWMZOT50Z96W342N', 7, 7, '2012/09/06'),
  ('ZINCHQ68G16Q241P', 8, 2, '2012/07/26'),
  ('ORSTSS31P31Y104Y', 4, 5, '2012/08/14'),
  ('XQZNZT88G40U962D', 5, 7, '2012/04/07'),
  ('XCJWAN70I55Y672Q', 8, 3, '2012/08/10'),
  ('MDEVHV23U54F992P', 10, 4, '2012/03/20'),
  ('XCJWAN70I55Y672Q', 6, 1, '2012/07/26'),
  ('EZKUWY53I63J761E', 10, 4, '2012/12/20'),
  ('IPMBZS65V32M583U', 5, 2, '2012/10/31'),
  ('YLJKYG88W20C316Z', 3, 6, '2012/10/14'),
  ('ZINCHQ68G16Q241P', 10, 4, '2012/04/30'),
  ('VLXTYJ37E95C042S', 1, 6, '2012/08/18'),
  ('LBLWJN49K33U650E', 5, 2, '2012/01/11'),
  ('YLJKYG88W20C316Z', 4, 7, '2012/11/02'),
  ('TCJWPJ41Q64B896K', 10, 2, '2012/08/29'),
  ('XCJWAN70I55Y672Q', 2, 5, '2012/11/06'),
  ('VRTAEL19L64M897F', 1, 3, '2012/10/25'),
  ('YKNFUW90F91N043S', 4, 3, '2012/08/23'),
  ('OTGHQH42T64P140C', 2, 6, '2012/11/27'),
  ('TTEBSW38G74U733H', 7, 6, '2012/11/14'),
  ('OVBQSO71K30C901Q', 9, 5, '2012/12/06'),
  ('RIAUKA68G97P974D', 9, 2, '2012/10/14'),
  ('HCSQMO65I24Z817A', 2, 7, '2012/04/20'),
  ('WBUOGV71C24I353R', 7, 4, '2012/05/23'),
  ('CTLBRQ36G52P402I', 10, 1, '2012/05/11'),
  ('CTLBRQ36G52P402I', 1, 6, '2012/05/31'),
  ('ORIORF06F56T308P', 6, 5, '2012/04/21'),
  ('UXRYEF75F99X304P', 6, 1, '2012/07/31'),
  ('YVXTTE91H89W246D', 7, 4, '2012/04/08'),
  ('ZQGVCK14S29C052K', 2, 5, '2012/05/16'),
  ('KGJRDH88C95S309R', 7, 4, '2012/07/13'),
  ('YHEZDA01G99S628D', 7, 7, '2012/10/06'),
  ('DPUZSV45E98L322Z', 9, 3, '2012/01/12'),
  ('VPFBBL26B64D093I', 9, 5, '2012/03/02'),
  ('OTGHQH42T64P140C', 1, 1, '2012/01/12'),
  ('XQZNZT88G40U962D', 4, 1, '2012/04/09'),
  ('IJOPKY15Y52Z111S', 6, 2, '2012/04/17'),
  ('CZKEQU58R55A910I', 4, 3, '2012/11/20'),
  ('IIMGLY19Z81U011J', 5, 5, '2012/05/04'),
  ('XCJWAN70I55Y672Q', 4, 5, '2012/07/05'),
  ('FEMWPY42Y80Y214S', 1, 7, '2012/07/06'),
  ('FPJYGA51I12Z671B', 5, 2, '2012/12/17'),
  ('USXKWK64P05T689I', 1, 6, '2012/06/22'),
  ('HCSQMO65I24Z817A', 5, 3, '2012/03/18'),
  ('HCSQMO65I24Z817A', 2, 6, '2012/10/12'),
  ('MNJNAZ51K45B109D', 3, 1, '2012/11/11'),
  ('XTKWFH67K23M759B', 4, 3, '2012/07/13'),
  ('EMSXGX19U73Y018Q', 4, 6, '2012/04/12'),
  ('MPYRTU30W74Z913N', 7, 2, '2012/10/30'),
  ('YHKWJJ38E54X808X', 5, 6, '2012/12/29'),
  ('ZOLJYR23U75I232W', 4, 6, '2012/09/24'),
  ('QEFHMI58C36L308U', 4, 2, '2012/07/07'),
  ('FLMPZB77R57T465B', 5, 7, '2012/09/01'),
  ('TOLXJJ56Y06R612Q', 3, 4, '2012/08/16'),
  ('VZZCJN28A11Q979W', 10, 6, '2012/10/02'),
  ('YVXTTE91H89W246D', 5, 6, '2012/06/18'),
  ('LQENJZ10X37D528H', 3, 6, '2012/12/16'),
  ('LRFDDW52O75X548L', 3, 1, '2012/06/20'),
  ('ENHCER63F82Y177V', 2, 7, '2012/01/07'),
  ('ZYSNFF09T01V419Z', 1, 6, '2012/05/06'),
  ('PNHRZY51K57T045M', 9, 7, '2012/04/23'),
  ('FPJYGA51I12Z671B', 6, 7, '2012/07/31'),
  ('ZSMVAB91P68Y600Y', 2, 2, '2012/11/08'),
  ('IFNUON73U42X110K', 4, 1, '2012/02/21'),
  ('NCQIEQ15K76F302F', 7, 3, '2012/12/15'),
  ('XIABWK07C68U956W', 9, 1, '2012/07/18'),
  ('GFMPKS27P22B473G', 4, 7, '2012/10/15');