DROP TABLE visita_guidata_effettuata;
DROP TABLE guida;
DROP TABLE tipo_visita;
DROP TABLE gruppo;
CREATE TABLE guida (
  codguida NUMBER(5) PRIMARY KEY,
  nome VARCHAR2(200) NOT NULL,
  cognome VARCHAR2(200) NOT NULL,
  nazionalità VARCHAR2(200)
);
CREATE TABLE tipo_visita (
  codtipovisita NUMBER(5) PRIMARY KEY,
  monumento VARCHAR2(200),
  durata TIMESTAMP,
  città VARCHAR2(200) NOT NULL
);
CREATE TABLE gruppo (
  codgr NUMBER(5) PRIMARY KEY,
  numeropartecipanti NUMBER(5) NOT NULL,
  lingua VARCHAR2(200)
);
CREATE TABLE visita_guidata_effettuata (
  codgr NUMBER(5),
  data DATE,
  ora TIMESTAMP,
  codtipovisita NUMBER(5) NOT NULL,
  codguida NUMBER(5) NOT NULL,
  PRIMARY KEY(codgr, data, ora),
  FOREIGN KEY (codgr) REFERENCES gruppo,
  FOREIGN KEY(codtipovisita) REFERENCES tipo_visita,
  FOREIGN KEY(codguida) REFERENCES guida
);
/*
Data for guida TABLE 
*/
INSERT INTO guida (codguida, nome, cognome, nazionalità)
VALUES
  (1, 'Celia', 'Hallex', 'Russia');
INSERT INTO guida (codguida, nome, cognome, nazionalità)
VALUES
  (2, 'Chantalle', 'Paddingdon', 'Czech Republic');
INSERT INTO guida (codguida, nome, cognome, nazionalità)
VALUES
  (3, 'Eldredge', 'Platts', 'China');
INSERT INTO guida (codguida, nome, cognome, nazionalità)
VALUES
  (4, 'Valma', 'MacKeeg', 'Saint Lucia');
INSERT INTO guida (codguida, nome, cognome, nazionalità)
VALUES
  (5, 'Cly', 'Noraway', 'Canada');
INSERT INTO guida (codguida, nome, cognome, nazionalità)
VALUES
  (6, 'Robb', 'Freer', 'Georgia');
INSERT INTO guida (codguida, nome, cognome, nazionalità)
VALUES
  (7, 'Merrel', 'Hans', 'Indonesia');
INSERT INTO guida (codguida, nome, cognome, nazionalità)
VALUES
  (8, 'Ruddy', 'Fraczek', 'Ecuador');
INSERT INTO guida (codguida, nome, cognome, nazionalità)
VALUES
  (9, 'Clovis', 'Spurdens', 'Nigeria');
INSERT INTO guida (codguida, nome, cognome, nazionalità)
VALUES
  (10, 'Parsifal', 'Skyram', 'Brazil');
INSERT INTO guida (codguida, nome, cognome, nazionalità)
VALUES
  (11, 'Tootsie', 'Flello', 'Palestinian Territory');
INSERT INTO guida (codguida, nome, cognome, nazionalità)
VALUES
  (12, 'Esra', 'Peoples', 'Indonesia');
INSERT INTO guida (codguida, nome, cognome, nazionalità)
VALUES
  (13, 'Bord', 'Kewish', 'China');
INSERT INTO guida (codguida, nome, cognome, nazionalità)
VALUES
  (14, 'Elana', 'Glencros', 'Indonesia');
INSERT INTO guida (codguida, nome, cognome, nazionalità)
VALUES
  (15, 'Stephana', 'Vuittet', 'Indonesia');
  /*
                Data fot tipo_visita 
                */
INSERT INTO tipo_visita (codtipovisita, monumento, durata, città)
VALUES
  (
    1,
    'Ali Baba Goes to Town',
    TO_DATE('4:30', 'MI:SS'),
    'Flor da Mata'
  );
INSERT INTO tipo_visita (codtipovisita, monumento, durata, città)
VALUES
  (
    2,
    'Return of the Vampire, The',
    TO_DATE('1:58', 'MI:SS'),
    'Rozivka'
  );
INSERT INTO tipo_visita (codtipovisita, monumento, durata, città)
VALUES
  (
    3,
    'Cruise, The',
    TO_DATE('5:02', 'MI:SS'),
    'Lugang'
  );
INSERT INTO tipo_visita (codtipovisita, monumento, durata, città)
VALUES
  (
    4,
    'Kiss the Girls',
    TO_DATE('0:17', 'MI:SS'),
    'Vykhino-Zhulebino'
  );
INSERT INTO tipo_visita (codtipovisita, monumento, durata, città)
VALUES
  (5, 'Vampires', TO_DATE('5:57', 'MI:SS'), 'Ganjun');
INSERT INTO tipo_visita (codtipovisita, monumento, durata, città)
VALUES
  (6, 'Z', TO_DATE('0:30', 'MI:SS'), 'Huaidao');
INSERT INTO tipo_visita (codtipovisita, monumento, durata, città)
VALUES
  (
    7,
    'Third Man, The',
    TO_DATE('5:05', 'MI:SS'),
    'Lunyuk Ode'
  );
INSERT INTO tipo_visita (codtipovisita, monumento, durata, città)
VALUES
  (
    8,
    'Come September',
    TO_DATE('7:51', 'MI:SS'),
    'Pasembon'
  );
INSERT INTO tipo_visita (codtipovisita, monumento, durata, città)
VALUES
  (
    9,
    'CAST A Deadly Spell',
    TO_DATE('4:51', 'MI:SS'),
    'Qianjin'
  );
INSERT INTO tipo_visita (codtipovisita, monumento, durata, città)
VALUES
  (
    10,
    'Summer and Smoke',
    TO_DATE('5:08', 'MI:SS'),
    'Krasne'
  );
INSERT INTO tipo_visita (codtipovisita, monumento, durata, città)
VALUES
  (
    11,
    'Baxter, The',
    TO_DATE('6:06', 'MI:SS'),
    'Baru Timur'
  );
INSERT INTO tipo_visita (codtipovisita, monumento, durata, città)
VALUES
  (
    12,
    'These Final Hours',
    TO_DATE('3:39', 'MI:SS'),
    'Stockholm'
  );
INSERT INTO tipo_visita (codtipovisita, monumento, durata, città)
VALUES
  (
    13,
    'I''m Gonna Explode (a.k.a. I''m Going to Explode) (Voy a explotar)',
    TO_DATE('2:33', 'MI:SS'),
    'Bertoua'
  );
INSERT INTO tipo_visita (codtipovisita, monumento, durata, città)
VALUES
  (
    14,
    'Music IN the Air',
    TO_DATE('0:06', 'MI:SS'),
    'Hlusk'
  );
INSERT INTO tipo_visita (codtipovisita, monumento, durata, città)
VALUES
  (
    15,
    '3 Sailors and a Girl (Three Sailors and a Girl)',
    TO_DATE('4:58', 'MI:SS'),
    'Nentón'
  );
  /*
              Data for gruppo
              */
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (1, 3, 'Tamil');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (2, 20, 'West Frisian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (3, 19, 'Albanian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (4, 8, 'Bosnian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (5, 10, 'Irish Gaelic');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (6, 11, 'Khmer');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (7, 17, 'Amharic');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (8, 18, 'Icelandic');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (9, 10, 'Georgian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (10, 10, 'Chinese');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (11, 5, 'Amharic');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (12, 12, 'Malagasy');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (13, 11, 'Luxembourgish');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (14, 7, 'Haitian Creole');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (15, 17, 'Tswana');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (16, 14, 'Italian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (17, 19, 'Czech');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (18, 11, 'Tajik');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (19, 20, 'Catalan');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (20, 7, 'Armenian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (21, 10, 'Belarusian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (22, 12, 'Polish');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (23, 5, 'Quechua');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (24, 2, 'Hebrew');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (25, 18, 'Swati');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (26, 14, 'Portuguese');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (27, 16, 'Malay');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (28, 19, 'Arabic');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (29, 1, 'Catalan');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (30, 15, 'Tswana');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (31, 18, 'Punjabi');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (32, 16, 'Kazakh');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (33, 20, 'Telugu');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (34, 2, 'Dzongkha');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (35, 14, 'Azeri');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (36, 13, 'Persian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (37, 3, 'Swedish');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (38, 15, 'Georgian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (39, 19, 'Bislama');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (40, 14, 'West Frisian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (41, 20, 'Tajik');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (42, 1, 'Danish');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (43, 8, 'Māori');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (44, 1, 'Filipino');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (45, 16, 'Tajik');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (46, 9, 'Kashmiri');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (47, 8, 'Oriya');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (48, 17, 'Armenian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (49, 3, 'Albanian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (50, 16, 'Sotho');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (51, 15, 'Filipino');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (52, 4, 'Papiamento');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (53, 14, 'Belarusian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (54, 1, 'Guaraní');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (55, 19, 'Norwegian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (56, 1, 'Ndebele');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (57, 20, 'Spanish');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (58, 16, 'Malay');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (59, 17, 'Quechua');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (60, 11, 'Swedish');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (61, 19, 'Luxembourgish');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (62, 2, 'Italian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (63, 3, 'Mongolian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (64, 5, 'Finnish');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (65, 10, 'Kazakh');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (66, 7, 'Assamese');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (67, 13, 'English');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (68, 3, 'Norwegian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (69, 3, 'Hindi');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (70, 1, 'Croatian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (71, 11, 'Czech');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (72, 16, 'Portuguese');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (73, 16, 'Marathi');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (74, 14, 'Swati');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (75, 18, 'Assamese');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (76, 7, 'Hiri Motu');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (77, 11, 'Bislama');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (78, 3, 'Moldovan');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (79, 13, 'Pashto');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (80, 5, 'Swahili');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (81, 11, 'Haitian Creole');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (82, 9, 'Northern Sotho');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (83, 9, 'Dari');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (84, 8, 'Hungarian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (85, 17, 'Tok Pisin');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (86, 5, 'Dhivehi');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (87, 14, 'Croatian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (88, 1, 'Moldovan');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (89, 10, 'Polish');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (90, 20, 'German');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (91, 2, 'German');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (92, 7, 'Azeri');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (93, 9, 'Montenegrin');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (94, 6, 'Papiamento');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (95, 2, 'Tajik');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (96, 13, 'Dutch');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (97, 18, 'Irish Gaelic');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (98, 14, 'Ndebele');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (99, 18, 'Maltese');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (100, 9, 'Dhivehi');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (101, 19, 'Tok Pisin');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (102, 7, 'Pashto');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (103, 6, 'Northern Sotho');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (104, 15, 'Gagauz');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (105, 12, 'Malay');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (106, 11, 'Indonesian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (107, 17, 'Malay');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (108, 7, 'Tok Pisin');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (109, 15, 'Moldovan');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (110, 7, 'Kannada');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (111, 6, 'English');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (112, 10, 'Croatian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (113, 17, 'Gujarati');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (114, 14, 'Fijian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (115, 20, 'Tetum');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (116, 13, 'Somali');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (117, 11, 'Tswana');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (118, 10, 'Romanian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (119, 15, 'Sotho');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (120, 6, 'Chinese');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (121, 20, 'Georgian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (122, 5, 'Afrikaans');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (123, 9, 'Tetum');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (124, 5, 'Assamese');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (125, 13, 'Kashmiri');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (126, 6, 'Ndebele');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (127, 6, 'Kyrgyz');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (128, 1, 'Kannada');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (129, 19, 'Danish');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (130, 2, 'Danish');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (131, 17, 'Marathi');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (132, 7, 'Japanese');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (133, 6, 'Mongolian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (134, 18, 'Finnish');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (135, 2, 'Malayalam');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (136, 5, 'Tsonga');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (137, 10, 'Albanian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (138, 7, 'English');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (139, 8, 'Greek');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (140, 2, 'Yiddish');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (141, 18, 'Tajik');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (142, 16, 'Italian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (143, 19, 'Tsonga');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (144, 11, 'Croatian');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (145, 13, 'French');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (146, 8, 'Nepali');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (147, 4, 'Icelandic');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (148, 6, 'Arabic');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (149, 14, 'Thai');
INSERT INTO gruppo (codgr, numeropartecipanti, lingua)
VALUES
  (150, 7, 'Danish');
  /*
          Data for visita_guidata_effettuata
          */
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    110,
    TO_DATE('2020-02-11', 'YY-MM-DD'),
    TO_DATE('06:05:35', 'HH24:MI:SS'),
    14,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    106,
    TO_DATE('2019-12-11', 'YY-MM-DD'),
    TO_DATE('16:36:48', 'HH24:MI:SS'),
    14,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    137,
    TO_DATE('2019-06-07', 'YY-MM-DD'),
    TO_DATE('04:10:00', 'HH24:MI:SS'),
    1,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    147,
    TO_DATE('2019-08-22', 'YY-MM-DD'),
    TO_DATE('05:14:55', 'HH24:MI:SS'),
    9,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    60,
    TO_DATE('2019-07-24', 'YY-MM-DD'),
    TO_DATE('01:20:49', 'HH24:MI:SS'),
    9,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    6,
    TO_DATE('2019-11-24', 'YY-MM-DD'),
    TO_DATE('08:52:13', 'HH24:MI:SS'),
    12,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    71,
    TO_DATE('2020-02-22', 'YY-MM-DD'),
    TO_DATE('01:46:22', 'HH24:MI:SS'),
    9,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    148,
    TO_DATE('2019-09-30', 'YY-MM-DD'),
    TO_DATE('22:16:15', 'HH24:MI:SS'),
    10,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    105,
    TO_DATE('2019-12-20', 'YY-MM-DD'),
    TO_DATE('14:49:14', 'HH24:MI:SS'),
    1,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    78,
    TO_DATE('2019-08-10', 'YY-MM-DD'),
    TO_DATE('10:05:58', 'HH24:MI:SS'),
    5,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    74,
    TO_DATE('2019-05-06', 'YY-MM-DD'),
    TO_DATE('08:49:58', 'HH24:MI:SS'),
    13,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    37,
    TO_DATE('2020-02-22', 'YY-MM-DD'),
    TO_DATE('08:47:31', 'HH24:MI:SS'),
    1,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    31,
    TO_DATE('2020-01-03', 'YY-MM-DD'),
    TO_DATE('04:44:34', 'HH24:MI:SS'),
    2,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    8,
    TO_DATE('2019-11-20', 'YY-MM-DD'),
    TO_DATE('10:30:32', 'HH24:MI:SS'),
    5,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    70,
    TO_DATE('2019-10-24', 'YY-MM-DD'),
    TO_DATE('18:26:44', 'HH24:MI:SS'),
    1,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    115,
    TO_DATE('2019-06-29', 'YY-MM-DD'),
    TO_DATE('21:56:59', 'HH24:MI:SS'),
    2,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    116,
    TO_DATE('2020-02-23', 'YY-MM-DD'),
    TO_DATE('23:03:05', 'HH24:MI:SS'),
    10,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    116,
    TO_DATE('2019-06-04', 'YY-MM-DD'),
    TO_DATE('22:05:49', 'HH24:MI:SS'),
    4,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    105,
    TO_DATE('2020-01-10', 'YY-MM-DD'),
    TO_DATE('22:35:33', 'HH24:MI:SS'),
    7,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    108,
    TO_DATE('2020-02-12', 'YY-MM-DD'),
    TO_DATE('04:01:07', 'HH24:MI:SS'),
    4,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    136,
    TO_DATE('2019-05-01', 'YY-MM-DD'),
    TO_DATE('10:50:28', 'HH24:MI:SS'),
    15,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    88,
    TO_DATE('2020-01-15', 'YY-MM-DD'),
    TO_DATE('14:03:00', 'HH24:MI:SS'),
    8,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    81,
    TO_DATE('2019-07-28', 'YY-MM-DD'),
    TO_DATE('23:18:18', 'HH24:MI:SS'),
    14,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    16,
    TO_DATE('2019-07-17', 'YY-MM-DD'),
    TO_DATE('09:27:16', 'HH24:MI:SS'),
    12,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    83,
    TO_DATE('2019-09-13', 'YY-MM-DD'),
    TO_DATE('18:44:29', 'HH24:MI:SS'),
    5,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    134,
    TO_DATE('2019-12-31', 'YY-MM-DD'),
    TO_DATE('12:37:04', 'HH24:MI:SS'),
    14,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    92,
    TO_DATE('2019-06-29', 'YY-MM-DD'),
    TO_DATE('10:54:06', 'HH24:MI:SS'),
    5,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    1,
    TO_DATE('2019-07-09', 'YY-MM-DD'),
    TO_DATE('17:06:05', 'HH24:MI:SS'),
    8,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    24,
    TO_DATE('2019-06-02', 'YY-MM-DD'),
    TO_DATE('03:46:18', 'HH24:MI:SS'),
    14,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    21,
    TO_DATE('2020-01-20', 'YY-MM-DD'),
    TO_DATE('17:19:34', 'HH24:MI:SS'),
    14,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    145,
    TO_DATE('2020-01-02', 'YY-MM-DD'),
    TO_DATE('15:40:43', 'HH24:MI:SS'),
    15,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    45,
    TO_DATE('2020-01-14', 'YY-MM-DD'),
    TO_DATE('10:28:45', 'HH24:MI:SS'),
    9,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    116,
    TO_DATE('2020-02-05', 'YY-MM-DD'),
    TO_DATE('17:01:19', 'HH24:MI:SS'),
    3,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    70,
    TO_DATE('2020-01-03', 'YY-MM-DD'),
    TO_DATE('21:39:18', 'HH24:MI:SS'),
    14,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    56,
    TO_DATE('2020-03-24', 'YY-MM-DD'),
    TO_DATE('20:24:51', 'HH24:MI:SS'),
    13,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    70,
    TO_DATE('2019-09-08', 'YY-MM-DD'),
    TO_DATE('05:38:42', 'HH24:MI:SS'),
    8,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    137,
    TO_DATE('2020-02-20', 'YY-MM-DD'),
    TO_DATE('08:23:25', 'HH24:MI:SS'),
    15,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    127,
    TO_DATE('2019-08-09', 'YY-MM-DD'),
    TO_DATE('21:21:07', 'HH24:MI:SS'),
    5,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    70,
    TO_DATE('2019-05-12', 'YY-MM-DD'),
    TO_DATE('17:11:19', 'HH24:MI:SS'),
    14,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    84,
    TO_DATE('2020-04-23', 'YY-MM-DD'),
    TO_DATE('23:11:26', 'HH24:MI:SS'),
    9,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    32,
    TO_DATE('2019-09-13', 'YY-MM-DD'),
    TO_DATE('08:07:40', 'HH24:MI:SS'),
    3,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    124,
    TO_DATE('2019-10-27', 'YY-MM-DD'),
    TO_DATE('01:10:49', 'HH24:MI:SS'),
    12,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    90,
    TO_DATE('2020-04-06', 'YY-MM-DD'),
    TO_DATE('01:22:19', 'HH24:MI:SS'),
    5,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    112,
    TO_DATE('2019-06-02', 'YY-MM-DD'),
    TO_DATE('13:56:31', 'HH24:MI:SS'),
    7,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    141,
    TO_DATE('2019-05-23', 'YY-MM-DD'),
    TO_DATE('20:55:49', 'HH24:MI:SS'),
    1,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    99,
    TO_DATE('2019-05-17', 'YY-MM-DD'),
    TO_DATE('06:54:40', 'HH24:MI:SS'),
    6,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    130,
    TO_DATE('2019-10-06', 'YY-MM-DD'),
    TO_DATE('20:41:47', 'HH24:MI:SS'),
    2,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    78,
    TO_DATE('2019-07-18', 'YY-MM-DD'),
    TO_DATE('20:00:43', 'HH24:MI:SS'),
    3,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    29,
    TO_DATE('2019-11-19', 'YY-MM-DD'),
    TO_DATE('23:41:29', 'HH24:MI:SS'),
    14,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    50,
    TO_DATE('2019-08-17', 'YY-MM-DD'),
    TO_DATE('22:26:30', 'HH24:MI:SS'),
    11,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    100,
    TO_DATE('2019-11-17', 'YY-MM-DD'),
    TO_DATE('16:53:16', 'HH24:MI:SS'),
    12,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    4,
    TO_DATE('2019-05-24', 'YY-MM-DD'),
    TO_DATE('22:50:09', 'HH24:MI:SS'),
    2,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    92,
    TO_DATE('2019-12-26', 'YY-MM-DD'),
    TO_DATE('13:34:43', 'HH24:MI:SS'),
    3,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    6,
    TO_DATE('2019-10-29', 'YY-MM-DD'),
    TO_DATE('04:56:32', 'HH24:MI:SS'),
    14,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    132,
    TO_DATE('2019-11-09', 'YY-MM-DD'),
    TO_DATE('22:18:54', 'HH24:MI:SS'),
    3,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    48,
    TO_DATE('2020-03-25', 'YY-MM-DD'),
    TO_DATE('03:15:45', 'HH24:MI:SS'),
    6,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    105,
    TO_DATE('2019-04-29', 'YY-MM-DD'),
    TO_DATE('02:20:24', 'HH24:MI:SS'),
    2,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    67,
    TO_DATE('2020-01-23', 'YY-MM-DD'),
    TO_DATE('17:52:33', 'HH24:MI:SS'),
    10,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    28,
    TO_DATE('2019-12-23', 'YY-MM-DD'),
    TO_DATE('05:30:21', 'HH24:MI:SS'),
    3,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    114,
    TO_DATE('2020-04-22', 'YY-MM-DD'),
    TO_DATE('20:53:46', 'HH24:MI:SS'),
    10,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    62,
    TO_DATE('2019-06-10', 'YY-MM-DD'),
    TO_DATE('06:16:06', 'HH24:MI:SS'),
    10,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    67,
    TO_DATE('2019-08-09', 'YY-MM-DD'),
    TO_DATE('16:17:39', 'HH24:MI:SS'),
    6,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    10,
    TO_DATE('2019-08-10', 'YY-MM-DD'),
    TO_DATE('07:08:21', 'HH24:MI:SS'),
    1,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    96,
    TO_DATE('2020-02-01', 'YY-MM-DD'),
    TO_DATE('13:43:39', 'HH24:MI:SS'),
    8,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    42,
    TO_DATE('2019-08-30', 'YY-MM-DD'),
    TO_DATE('16:38:42', 'HH24:MI:SS'),
    2,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    147,
    TO_DATE('2019-08-15', 'YY-MM-DD'),
    TO_DATE('12:48:55', 'HH24:MI:SS'),
    15,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    135,
    TO_DATE('2019-11-02', 'YY-MM-DD'),
    TO_DATE('21:12:44', 'HH24:MI:SS'),
    12,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    7,
    TO_DATE('2019-12-15', 'YY-MM-DD'),
    TO_DATE('04:33:59', 'HH24:MI:SS'),
    10,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    83,
    TO_DATE('2020-01-29', 'YY-MM-DD'),
    TO_DATE('05:12:05', 'HH24:MI:SS'),
    13,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    146,
    TO_DATE('2019-07-08', 'YY-MM-DD'),
    TO_DATE('21:02:40', 'HH24:MI:SS'),
    6,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    139,
    TO_DATE('2020-04-09', 'YY-MM-DD'),
    TO_DATE('03:56:26', 'HH24:MI:SS'),
    12,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    19,
    TO_DATE('2019-11-08', 'YY-MM-DD'),
    TO_DATE('03:22:46', 'HH24:MI:SS'),
    6,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    54,
    TO_DATE('2019-12-12', 'YY-MM-DD'),
    TO_DATE('08:45:16', 'HH24:MI:SS'),
    6,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    23,
    TO_DATE('2019-05-20', 'YY-MM-DD'),
    TO_DATE('15:34:51', 'HH24:MI:SS'),
    15,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    113,
    TO_DATE('2019-11-21', 'YY-MM-DD'),
    TO_DATE('14:03:19', 'HH24:MI:SS'),
    15,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    42,
    TO_DATE('2019-05-29', 'YY-MM-DD'),
    TO_DATE('09:20:41', 'HH24:MI:SS'),
    1,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    82,
    TO_DATE('2019-12-16', 'YY-MM-DD'),
    TO_DATE('20:03:52', 'HH24:MI:SS'),
    15,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    61,
    TO_DATE('2019-12-03', 'YY-MM-DD'),
    TO_DATE('01:53:08', 'HH24:MI:SS'),
    1,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    10,
    TO_DATE('2019-09-11', 'YY-MM-DD'),
    TO_DATE('18:10:30', 'HH24:MI:SS'),
    9,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    45,
    TO_DATE('2019-10-03', 'YY-MM-DD'),
    TO_DATE('02:23:51', 'HH24:MI:SS'),
    15,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    7,
    TO_DATE('2019-07-07', 'YY-MM-DD'),
    TO_DATE('19:52:26', 'HH24:MI:SS'),
    4,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    145,
    TO_DATE('2020-03-30', 'YY-MM-DD'),
    TO_DATE('13:58:23', 'HH24:MI:SS'),
    15,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    56,
    TO_DATE('2019-06-24', 'YY-MM-DD'),
    TO_DATE('04:59:18', 'HH24:MI:SS'),
    14,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    55,
    TO_DATE('2019-12-02', 'YY-MM-DD'),
    TO_DATE('17:07:00', 'HH24:MI:SS'),
    3,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    36,
    TO_DATE('2019-08-17', 'YY-MM-DD'),
    TO_DATE('08:02:12', 'HH24:MI:SS'),
    5,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    134,
    TO_DATE('2020-02-25', 'YY-MM-DD'),
    TO_DATE('12:48:43', 'HH24:MI:SS'),
    2,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    2,
    TO_DATE('2019-11-06', 'YY-MM-DD'),
    TO_DATE('17:35:47', 'HH24:MI:SS'),
    11,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    95,
    TO_DATE('2019-06-22', 'YY-MM-DD'),
    TO_DATE('04:52:36', 'HH24:MI:SS'),
    14,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    2,
    TO_DATE('2020-02-04', 'YY-MM-DD'),
    TO_DATE('20:45:38', 'HH24:MI:SS'),
    10,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    68,
    TO_DATE('2019-06-17', 'YY-MM-DD'),
    TO_DATE('16:29:15', 'HH24:MI:SS'),
    2,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    60,
    TO_DATE('2019-06-01', 'YY-MM-DD'),
    TO_DATE('06:20:15', 'HH24:MI:SS'),
    5,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    83,
    TO_DATE('2019-05-11', 'YY-MM-DD'),
    TO_DATE('01:22:21', 'HH24:MI:SS'),
    8,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    144,
    TO_DATE('2019-12-04', 'YY-MM-DD'),
    TO_DATE('15:33:44', 'HH24:MI:SS'),
    5,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    44,
    TO_DATE('2019-05-12', 'YY-MM-DD'),
    TO_DATE('00:22:01', 'HH24:MI:SS'),
    1,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    126,
    TO_DATE('2019-11-20', 'YY-MM-DD'),
    TO_DATE('17:07:43', 'HH24:MI:SS'),
    4,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    58,
    TO_DATE('2019-06-17', 'YY-MM-DD'),
    TO_DATE('15:05:16', 'HH24:MI:SS'),
    3,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    97,
    TO_DATE('2019-07-11', 'YY-MM-DD'),
    TO_DATE('13:59:58', 'HH24:MI:SS'),
    8,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    140,
    TO_DATE('2020-03-28', 'YY-MM-DD'),
    TO_DATE('05:14:38', 'HH24:MI:SS'),
    9,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    78,
    TO_DATE('2020-02-12', 'YY-MM-DD'),
    TO_DATE('12:29:47', 'HH24:MI:SS'),
    13,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    94,
    TO_DATE('2019-07-03', 'YY-MM-DD'),
    TO_DATE('13:22:20', 'HH24:MI:SS'),
    2,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    146,
    TO_DATE('2019-06-07', 'YY-MM-DD'),
    TO_DATE('21:25:11', 'HH24:MI:SS'),
    4,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    4,
    TO_DATE('2019-12-26', 'YY-MM-DD'),
    TO_DATE('09:27:30', 'HH24:MI:SS'),
    13,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    11,
    TO_DATE('2020-02-23', 'YY-MM-DD'),
    TO_DATE('22:13:40', 'HH24:MI:SS'),
    4,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    95,
    TO_DATE('2019-08-15', 'YY-MM-DD'),
    TO_DATE('19:04:24', 'HH24:MI:SS'),
    1,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    85,
    TO_DATE('2019-05-17', 'YY-MM-DD'),
    TO_DATE('20:13:48', 'HH24:MI:SS'),
    13,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    116,
    TO_DATE('2019-07-14', 'YY-MM-DD'),
    TO_DATE('15:50:19', 'HH24:MI:SS'),
    10,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    15,
    TO_DATE('2020-03-16', 'YY-MM-DD'),
    TO_DATE('22:45:55', 'HH24:MI:SS'),
    11,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    88,
    TO_DATE('2019-09-18', 'YY-MM-DD'),
    TO_DATE('03:57:33', 'HH24:MI:SS'),
    5,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    70,
    TO_DATE('2019-12-27', 'YY-MM-DD'),
    TO_DATE('09:25:38', 'HH24:MI:SS'),
    1,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    65,
    TO_DATE('2020-03-28', 'YY-MM-DD'),
    TO_DATE('19:44:32', 'HH24:MI:SS'),
    13,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    91,
    TO_DATE('2020-03-25', 'YY-MM-DD'),
    TO_DATE('03:43:10', 'HH24:MI:SS'),
    9,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    99,
    TO_DATE('2019-12-11', 'YY-MM-DD'),
    TO_DATE('22:29:58', 'HH24:MI:SS'),
    5,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    31,
    TO_DATE('2019-11-03', 'YY-MM-DD'),
    TO_DATE('08:53:55', 'HH24:MI:SS'),
    4,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    136,
    TO_DATE('2019-06-21', 'YY-MM-DD'),
    TO_DATE('05:42:11', 'HH24:MI:SS'),
    5,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    53,
    TO_DATE('2019-06-09', 'YY-MM-DD'),
    TO_DATE('01:36:55', 'HH24:MI:SS'),
    4,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    122,
    TO_DATE('2020-02-01', 'YY-MM-DD'),
    TO_DATE('19:43:19', 'HH24:MI:SS'),
    1,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    39,
    TO_DATE('2020-01-19', 'YY-MM-DD'),
    TO_DATE('18:36:15', 'HH24:MI:SS'),
    7,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    111,
    TO_DATE('2019-08-24', 'YY-MM-DD'),
    TO_DATE('19:52:27', 'HH24:MI:SS'),
    11,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    38,
    TO_DATE('2019-10-26', 'YY-MM-DD'),
    TO_DATE('05:24:31', 'HH24:MI:SS'),
    7,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    41,
    TO_DATE('2019-12-04', 'YY-MM-DD'),
    TO_DATE('18:13:01', 'HH24:MI:SS'),
    8,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    22,
    TO_DATE('2020-03-04', 'YY-MM-DD'),
    TO_DATE('16:44:46', 'HH24:MI:SS'),
    9,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    56,
    TO_DATE('2020-01-01', 'YY-MM-DD'),
    TO_DATE('19:20:38', 'HH24:MI:SS'),
    14,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    122,
    TO_DATE('2019-10-31', 'YY-MM-DD'),
    TO_DATE('20:31:22', 'HH24:MI:SS'),
    15,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    83,
    TO_DATE('2019-06-30', 'YY-MM-DD'),
    TO_DATE('00:41:31', 'HH24:MI:SS'),
    2,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    87,
    TO_DATE('2019-05-17', 'YY-MM-DD'),
    TO_DATE('07:33:44', 'HH24:MI:SS'),
    2,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    75,
    TO_DATE('2019-09-13', 'YY-MM-DD'),
    TO_DATE('06:30:03', 'HH24:MI:SS'),
    1,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    131,
    TO_DATE('2020-01-29', 'YY-MM-DD'),
    TO_DATE('22:49:02', 'HH24:MI:SS'),
    3,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    145,
    TO_DATE('2019-06-26', 'YY-MM-DD'),
    TO_DATE('07:43:49', 'HH24:MI:SS'),
    1,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    53,
    TO_DATE('2019-08-10', 'YY-MM-DD'),
    TO_DATE('18:22:07', 'HH24:MI:SS'),
    13,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    147,
    TO_DATE('2019-06-25', 'YY-MM-DD'),
    TO_DATE('15:58:51', 'HH24:MI:SS'),
    13,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    141,
    TO_DATE('2019-07-12', 'YY-MM-DD'),
    TO_DATE('21:30:59', 'HH24:MI:SS'),
    11,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    124,
    TO_DATE('2019-11-20', 'YY-MM-DD'),
    TO_DATE('11:03:31', 'HH24:MI:SS'),
    14,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    96,
    TO_DATE('2020-04-02', 'YY-MM-DD'),
    TO_DATE('03:56:15', 'HH24:MI:SS'),
    1,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    139,
    TO_DATE('2019-09-27', 'YY-MM-DD'),
    TO_DATE('15:01:52', 'HH24:MI:SS'),
    1,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    56,
    TO_DATE('2020-04-14', 'YY-MM-DD'),
    TO_DATE('03:03:25', 'HH24:MI:SS'),
    13,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    20,
    TO_DATE('2019-06-21', 'YY-MM-DD'),
    TO_DATE('08:50:24', 'HH24:MI:SS'),
    6,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    88,
    TO_DATE('2019-07-23', 'YY-MM-DD'),
    TO_DATE('11:50:43', 'HH24:MI:SS'),
    2,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    82,
    TO_DATE('2020-04-08', 'YY-MM-DD'),
    TO_DATE('23:49:05', 'HH24:MI:SS'),
    13,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    19,
    TO_DATE('2020-01-18', 'YY-MM-DD'),
    TO_DATE('23:45:30', 'HH24:MI:SS'),
    12,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    27,
    TO_DATE('2020-03-21', 'YY-MM-DD'),
    TO_DATE('00:24:23', 'HH24:MI:SS'),
    3,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    123,
    TO_DATE('2019-08-11', 'YY-MM-DD'),
    TO_DATE('13:31:25', 'HH24:MI:SS'),
    6,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    106,
    TO_DATE('2019-06-07', 'YY-MM-DD'),
    TO_DATE('21:43:14', 'HH24:MI:SS'),
    11,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    110,
    TO_DATE('2020-01-11', 'YY-MM-DD'),
    TO_DATE('04:49:36', 'HH24:MI:SS'),
    10,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    142,
    TO_DATE('2019-06-23', 'YY-MM-DD'),
    TO_DATE('09:08:48', 'HH24:MI:SS'),
    2,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    61,
    TO_DATE('2019-04-29', 'YY-MM-DD'),
    TO_DATE('05:25:32', 'HH24:MI:SS'),
    13,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    44,
    TO_DATE('2019-12-22', 'YY-MM-DD'),
    TO_DATE('23:33:22', 'HH24:MI:SS'),
    11,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    62,
    TO_DATE('2019-10-18', 'YY-MM-DD'),
    TO_DATE('13:31:15', 'HH24:MI:SS'),
    10,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    134,
    TO_DATE('2020-04-13', 'YY-MM-DD'),
    TO_DATE('16:36:35', 'HH24:MI:SS'),
    15,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    146,
    TO_DATE('2020-02-07', 'YY-MM-DD'),
    TO_DATE('01:03:22', 'HH24:MI:SS'),
    1,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    123,
    TO_DATE('2019-06-15', 'YY-MM-DD'),
    TO_DATE('15:10:44', 'HH24:MI:SS'),
    9,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    35,
    TO_DATE('2020-01-04', 'YY-MM-DD'),
    TO_DATE('16:44:31', 'HH24:MI:SS'),
    4,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    57,
    TO_DATE('2019-10-14', 'YY-MM-DD'),
    TO_DATE('12:43:31', 'HH24:MI:SS'),
    12,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    31,
    TO_DATE('2019-07-13', 'YY-MM-DD'),
    TO_DATE('05:58:30', 'HH24:MI:SS'),
    3,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    147,
    TO_DATE('2019-06-20', 'YY-MM-DD'),
    TO_DATE('06:36:39', 'HH24:MI:SS'),
    2,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    68,
    TO_DATE('2020-02-11', 'YY-MM-DD'),
    TO_DATE('08:58:32', 'HH24:MI:SS'),
    11,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    70,
    TO_DATE('2019-05-18', 'YY-MM-DD'),
    TO_DATE('06:06:15', 'HH24:MI:SS'),
    7,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    51,
    TO_DATE('2019-06-25', 'YY-MM-DD'),
    TO_DATE('20:57:51', 'HH24:MI:SS'),
    14,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    149,
    TO_DATE('2019-11-04', 'YY-MM-DD'),
    TO_DATE('20:28:56', 'HH24:MI:SS'),
    13,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    48,
    TO_DATE('2019-08-27', 'YY-MM-DD'),
    TO_DATE('16:14:01', 'HH24:MI:SS'),
    10,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    119,
    TO_DATE('2019-07-21', 'YY-MM-DD'),
    TO_DATE('14:36:28', 'HH24:MI:SS'),
    1,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    71,
    TO_DATE('2019-10-30', 'YY-MM-DD'),
    TO_DATE('17:44:55', 'HH24:MI:SS'),
    8,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    97,
    TO_DATE('2019-09-02', 'YY-MM-DD'),
    TO_DATE('14:06:20', 'HH24:MI:SS'),
    6,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    83,
    TO_DATE('2019-09-12', 'YY-MM-DD'),
    TO_DATE('15:32:36', 'HH24:MI:SS'),
    13,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    113,
    TO_DATE('2019-08-02', 'YY-MM-DD'),
    TO_DATE('08:13:55', 'HH24:MI:SS'),
    1,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    48,
    TO_DATE('2019-12-11', 'YY-MM-DD'),
    TO_DATE('15:26:48', 'HH24:MI:SS'),
    15,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    67,
    TO_DATE('2019-09-08', 'YY-MM-DD'),
    TO_DATE('04:30:39', 'HH24:MI:SS'),
    3,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    108,
    TO_DATE('2020-04-04', 'YY-MM-DD'),
    TO_DATE('19:59:57', 'HH24:MI:SS'),
    12,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    128,
    TO_DATE('2020-03-23', 'YY-MM-DD'),
    TO_DATE('01:50:10', 'HH24:MI:SS'),
    2,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    83,
    TO_DATE('2019-08-13', 'YY-MM-DD'),
    TO_DATE('00:19:06', 'HH24:MI:SS'),
    1,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    51,
    TO_DATE('2019-07-01', 'YY-MM-DD'),
    TO_DATE('13:37:22', 'HH24:MI:SS'),
    8,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    125,
    TO_DATE('2019-10-23', 'YY-MM-DD'),
    TO_DATE('05:26:24', 'HH24:MI:SS'),
    6,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    139,
    TO_DATE('2019-08-08', 'YY-MM-DD'),
    TO_DATE('08:01:33', 'HH24:MI:SS'),
    14,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    133,
    TO_DATE('2020-03-17', 'YY-MM-DD'),
    TO_DATE('12:17:15', 'HH24:MI:SS'),
    10,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    70,
    TO_DATE('2020-04-23', 'YY-MM-DD'),
    TO_DATE('22:46:48', 'HH24:MI:SS'),
    3,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    109,
    TO_DATE('2019-10-30', 'YY-MM-DD'),
    TO_DATE('10:00:56', 'HH24:MI:SS'),
    3,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    24,
    TO_DATE('2019-10-10', 'YY-MM-DD'),
    TO_DATE('21:35:00', 'HH24:MI:SS'),
    9,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    55,
    TO_DATE('2019-05-23', 'YY-MM-DD'),
    TO_DATE('06:11:26', 'HH24:MI:SS'),
    10,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    142,
    TO_DATE('2019-08-27', 'YY-MM-DD'),
    TO_DATE('15:10:11', 'HH24:MI:SS'),
    11,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    85,
    TO_DATE('2019-10-28', 'YY-MM-DD'),
    TO_DATE('05:12:24', 'HH24:MI:SS'),
    12,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    128,
    TO_DATE('2020-02-17', 'YY-MM-DD'),
    TO_DATE('07:16:29', 'HH24:MI:SS'),
    5,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    139,
    TO_DATE('2020-03-16', 'YY-MM-DD'),
    TO_DATE('06:04:10', 'HH24:MI:SS'),
    9,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    69,
    TO_DATE('2019-06-07', 'YY-MM-DD'),
    TO_DATE('13:32:48', 'HH24:MI:SS'),
    7,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    117,
    TO_DATE('2020-02-20', 'YY-MM-DD'),
    TO_DATE('20:34:34', 'HH24:MI:SS'),
    9,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    89,
    TO_DATE('2019-07-14', 'YY-MM-DD'),
    TO_DATE('10:41:23', 'HH24:MI:SS'),
    9,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    111,
    TO_DATE('2019-12-26', 'YY-MM-DD'),
    TO_DATE('15:43:31', 'HH24:MI:SS'),
    7,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    47,
    TO_DATE('2019-12-15', 'YY-MM-DD'),
    TO_DATE('02:54:28', 'HH24:MI:SS'),
    7,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    141,
    TO_DATE('2019-12-28', 'YY-MM-DD'),
    TO_DATE('18:24:01', 'HH24:MI:SS'),
    8,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    45,
    TO_DATE('2019-08-23', 'YY-MM-DD'),
    TO_DATE('22:30:56', 'HH24:MI:SS'),
    14,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    138,
    TO_DATE('2019-11-14', 'YY-MM-DD'),
    TO_DATE('10:49:15', 'HH24:MI:SS'),
    6,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    91,
    TO_DATE('2019-10-16', 'YY-MM-DD'),
    TO_DATE('20:35:26', 'HH24:MI:SS'),
    7,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    8,
    TO_DATE('2019-09-09', 'YY-MM-DD'),
    TO_DATE('14:14:50', 'HH24:MI:SS'),
    3,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    38,
    TO_DATE('2020-02-12', 'YY-MM-DD'),
    TO_DATE('07:37:47', 'HH24:MI:SS'),
    7,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    17,
    TO_DATE('2019-12-21', 'YY-MM-DD'),
    TO_DATE('14:05:46', 'HH24:MI:SS'),
    3,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    14,
    TO_DATE('2019-10-17', 'YY-MM-DD'),
    TO_DATE('08:25:11', 'HH24:MI:SS'),
    11,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    31,
    TO_DATE('2019-11-17', 'YY-MM-DD'),
    TO_DATE('12:45:12', 'HH24:MI:SS'),
    7,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    45,
    TO_DATE('2019-12-14', 'YY-MM-DD'),
    TO_DATE('07:32:45', 'HH24:MI:SS'),
    3,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    29,
    TO_DATE('2019-11-20', 'YY-MM-DD'),
    TO_DATE('12:56:29', 'HH24:MI:SS'),
    8,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    89,
    TO_DATE('2020-01-30', 'YY-MM-DD'),
    TO_DATE('22:23:46', 'HH24:MI:SS'),
    6,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    76,
    TO_DATE('2020-03-20', 'YY-MM-DD'),
    TO_DATE('09:31:33', 'HH24:MI:SS'),
    13,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    4,
    TO_DATE('2019-11-03', 'YY-MM-DD'),
    TO_DATE('07:48:45', 'HH24:MI:SS'),
    6,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    76,
    TO_DATE('2020-02-11', 'YY-MM-DD'),
    TO_DATE('02:44:11', 'HH24:MI:SS'),
    14,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    119,
    TO_DATE('2020-01-04', 'YY-MM-DD'),
    TO_DATE('10:17:09', 'HH24:MI:SS'),
    7,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    7,
    TO_DATE('2019-05-08', 'YY-MM-DD'),
    TO_DATE('10:17:02', 'HH24:MI:SS'),
    1,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    73,
    TO_DATE('2019-08-06', 'YY-MM-DD'),
    TO_DATE('17:44:04', 'HH24:MI:SS'),
    15,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    117,
    TO_DATE('2020-01-10', 'YY-MM-DD'),
    TO_DATE('06:07:34', 'HH24:MI:SS'),
    2,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    49,
    TO_DATE('2019-05-31', 'YY-MM-DD'),
    TO_DATE('08:58:10', 'HH24:MI:SS'),
    14,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    129,
    TO_DATE('2019-05-12', 'YY-MM-DD'),
    TO_DATE('03:30:10', 'HH24:MI:SS'),
    10,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    41,
    TO_DATE('2019-11-25', 'YY-MM-DD'),
    TO_DATE('19:06:04', 'HH24:MI:SS'),
    7,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    131,
    TO_DATE('2019-07-12', 'YY-MM-DD'),
    TO_DATE('21:02:19', 'HH24:MI:SS'),
    5,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    45,
    TO_DATE('2019-07-14', 'YY-MM-DD'),
    TO_DATE('23:17:11', 'HH24:MI:SS'),
    1,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    48,
    TO_DATE('2019-12-08', 'YY-MM-DD'),
    TO_DATE('11:07:30', 'HH24:MI:SS'),
    12,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    1,
    TO_DATE('2019-12-31', 'YY-MM-DD'),
    TO_DATE('01:22:41', 'HH24:MI:SS'),
    2,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    98,
    TO_DATE('2019-05-13', 'YY-MM-DD'),
    TO_DATE('02:57:54', 'HH24:MI:SS'),
    2,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    33,
    TO_DATE('2020-03-25', 'YY-MM-DD'),
    TO_DATE('02:51:55', 'HH24:MI:SS'),
    1,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    139,
    TO_DATE('2019-10-12', 'YY-MM-DD'),
    TO_DATE('20:31:20', 'HH24:MI:SS'),
    2,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    8,
    TO_DATE('2020-02-21', 'YY-MM-DD'),
    TO_DATE('22:33:53', 'HH24:MI:SS'),
    9,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    32,
    TO_DATE('2019-07-05', 'YY-MM-DD'),
    TO_DATE('14:22:12', 'HH24:MI:SS'),
    5,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    40,
    TO_DATE('2019-11-21', 'YY-MM-DD'),
    TO_DATE('15:55:45', 'HH24:MI:SS'),
    4,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    80,
    TO_DATE('2019-11-27', 'YY-MM-DD'),
    TO_DATE('05:19:40', 'HH24:MI:SS'),
    12,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    89,
    TO_DATE('2019-08-21', 'YY-MM-DD'),
    TO_DATE('19:32:03', 'HH24:MI:SS'),
    14,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    94,
    TO_DATE('2019-05-29', 'YY-MM-DD'),
    TO_DATE('21:59:07', 'HH24:MI:SS'),
    14,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    63,
    TO_DATE('2019-10-19', 'YY-MM-DD'),
    TO_DATE('09:05:07', 'HH24:MI:SS'),
    5,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    46,
    TO_DATE('2019-10-24', 'YY-MM-DD'),
    TO_DATE('03:11:21', 'HH24:MI:SS'),
    7,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    43,
    TO_DATE('2019-12-31', 'YY-MM-DD'),
    TO_DATE('02:52:25', 'HH24:MI:SS'),
    11,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    134,
    TO_DATE('2020-02-24', 'YY-MM-DD'),
    TO_DATE('18:22:45', 'HH24:MI:SS'),
    4,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    17,
    TO_DATE('2019-07-14', 'YY-MM-DD'),
    TO_DATE('22:31:54', 'HH24:MI:SS'),
    4,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    148,
    TO_DATE('2019-07-05', 'YY-MM-DD'),
    TO_DATE('08:47:28', 'HH24:MI:SS'),
    3,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    80,
    TO_DATE('2019-05-18', 'YY-MM-DD'),
    TO_DATE('05:04:10', 'HH24:MI:SS'),
    11,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    30,
    TO_DATE('2020-04-10', 'YY-MM-DD'),
    TO_DATE('07:29:58', 'HH24:MI:SS'),
    3,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    102,
    TO_DATE('2019-07-19', 'YY-MM-DD'),
    TO_DATE('10:09:21', 'HH24:MI:SS'),
    13,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    103,
    TO_DATE('2019-12-31', 'YY-MM-DD'),
    TO_DATE('23:56:56', 'HH24:MI:SS'),
    4,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    47,
    TO_DATE('2019-08-31', 'YY-MM-DD'),
    TO_DATE('09:09:23', 'HH24:MI:SS'),
    15,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    44,
    TO_DATE('2020-01-06', 'YY-MM-DD'),
    TO_DATE('12:05:00', 'HH24:MI:SS'),
    1,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    50,
    TO_DATE('2019-08-22', 'YY-MM-DD'),
    TO_DATE('01:05:47', 'HH24:MI:SS'),
    4,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    64,
    TO_DATE('2019-09-05', 'YY-MM-DD'),
    TO_DATE('18:06:58', 'HH24:MI:SS'),
    1,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    91,
    TO_DATE('2019-08-27', 'YY-MM-DD'),
    TO_DATE('23:13:34', 'HH24:MI:SS'),
    10,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    73,
    TO_DATE('2019-05-28', 'YY-MM-DD'),
    TO_DATE('14:17:38', 'HH24:MI:SS'),
    10,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    17,
    TO_DATE('2019-10-29', 'YY-MM-DD'),
    TO_DATE('18:07:11', 'HH24:MI:SS'),
    10,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    25,
    TO_DATE('2019-08-01', 'YY-MM-DD'),
    TO_DATE('02:12:26', 'HH24:MI:SS'),
    5,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    54,
    TO_DATE('2020-04-21', 'YY-MM-DD'),
    TO_DATE('00:44:08', 'HH24:MI:SS'),
    12,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    71,
    TO_DATE('2020-01-04', 'YY-MM-DD'),
    TO_DATE('06:20:12', 'HH24:MI:SS'),
    15,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    9,
    TO_DATE('2019-09-17', 'YY-MM-DD'),
    TO_DATE('22:26:17', 'HH24:MI:SS'),
    8,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    20,
    TO_DATE('2019-10-12', 'YY-MM-DD'),
    TO_DATE('15:32:29', 'HH24:MI:SS'),
    6,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    104,
    TO_DATE('2019-09-16', 'YY-MM-DD'),
    TO_DATE('07:17:35', 'HH24:MI:SS'),
    8,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    143,
    TO_DATE('2019-07-05', 'YY-MM-DD'),
    TO_DATE('23:27:53', 'HH24:MI:SS'),
    6,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    71,
    TO_DATE('2020-01-16', 'YY-MM-DD'),
    TO_DATE('17:08:33', 'HH24:MI:SS'),
    5,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    84,
    TO_DATE('2019-08-14', 'YY-MM-DD'),
    TO_DATE('18:27:05', 'HH24:MI:SS'),
    9,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    135,
    TO_DATE('2019-09-10', 'YY-MM-DD'),
    TO_DATE('16:05:41', 'HH24:MI:SS'),
    2,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    132,
    TO_DATE('2019-09-14', 'YY-MM-DD'),
    TO_DATE('08:18:54', 'HH24:MI:SS'),
    9,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    116,
    TO_DATE('2020-04-02', 'YY-MM-DD'),
    TO_DATE('07:36:50', 'HH24:MI:SS'),
    14,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    82,
    TO_DATE('2020-03-06', 'YY-MM-DD'),
    TO_DATE('22:39:02', 'HH24:MI:SS'),
    8,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    148,
    TO_DATE('2019-12-05', 'YY-MM-DD'),
    TO_DATE('14:09:19', 'HH24:MI:SS'),
    1,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    91,
    TO_DATE('2019-05-28', 'YY-MM-DD'),
    TO_DATE('08:19:17', 'HH24:MI:SS'),
    11,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    14,
    TO_DATE('2019-05-11', 'YY-MM-DD'),
    TO_DATE('14:28:30', 'HH24:MI:SS'),
    6,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    36,
    TO_DATE('2019-05-22', 'YY-MM-DD'),
    TO_DATE('08:55:14', 'HH24:MI:SS'),
    7,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    19,
    TO_DATE('2019-11-29', 'YY-MM-DD'),
    TO_DATE('02:49:59', 'HH24:MI:SS'),
    4,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    106,
    TO_DATE('2019-12-02', 'YY-MM-DD'),
    TO_DATE('02:51:36', 'HH24:MI:SS'),
    14,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    53,
    TO_DATE('2020-01-30', 'YY-MM-DD'),
    TO_DATE('13:01:05', 'HH24:MI:SS'),
    1,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    87,
    TO_DATE('2019-08-28', 'YY-MM-DD'),
    TO_DATE('23:55:49', 'HH24:MI:SS'),
    7,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    146,
    TO_DATE('2020-04-01', 'YY-MM-DD'),
    TO_DATE('08:19:04', 'HH24:MI:SS'),
    3,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    62,
    TO_DATE('2019-06-04', 'YY-MM-DD'),
    TO_DATE('17:02:59', 'HH24:MI:SS'),
    6,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    132,
    TO_DATE('2019-08-10', 'YY-MM-DD'),
    TO_DATE('19:18:50', 'HH24:MI:SS'),
    15,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    98,
    TO_DATE('2020-03-09', 'YY-MM-DD'),
    TO_DATE('06:25:18', 'HH24:MI:SS'),
    10,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    54,
    TO_DATE('2019-05-11', 'YY-MM-DD'),
    TO_DATE('06:59:50', 'HH24:MI:SS'),
    9,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    86,
    TO_DATE('2020-01-14', 'YY-MM-DD'),
    TO_DATE('22:50:38', 'HH24:MI:SS'),
    4,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    123,
    TO_DATE('2019-06-21', 'YY-MM-DD'),
    TO_DATE('23:35:07', 'HH24:MI:SS'),
    5,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    39,
    TO_DATE('2019-12-11', 'YY-MM-DD'),
    TO_DATE('18:07:40', 'HH24:MI:SS'),
    4,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    143,
    TO_DATE('2019-09-13', 'YY-MM-DD'),
    TO_DATE('01:15:30', 'HH24:MI:SS'),
    13,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    48,
    TO_DATE('2020-04-16', 'YY-MM-DD'),
    TO_DATE('18:56:47', 'HH24:MI:SS'),
    2,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    62,
    TO_DATE('2020-02-25', 'YY-MM-DD'),
    TO_DATE('23:48:13', 'HH24:MI:SS'),
    9,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    91,
    TO_DATE('2019-10-14', 'YY-MM-DD'),
    TO_DATE('22:15:55', 'HH24:MI:SS'),
    11,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    59,
    TO_DATE('2019-11-12', 'YY-MM-DD'),
    TO_DATE('03:45:38', 'HH24:MI:SS'),
    6,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    148,
    TO_DATE('2019-07-28', 'YY-MM-DD'),
    TO_DATE('10:57:30', 'HH24:MI:SS'),
    5,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    24,
    TO_DATE('2019-12-01', 'YY-MM-DD'),
    TO_DATE('12:36:43', 'HH24:MI:SS'),
    3,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    113,
    TO_DATE('2019-11-17', 'YY-MM-DD'),
    TO_DATE('21:52:29', 'HH24:MI:SS'),
    8,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    68,
    TO_DATE('2019-08-19', 'YY-MM-DD'),
    TO_DATE('14:04:50', 'HH24:MI:SS'),
    3,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    44,
    TO_DATE('2019-06-30', 'YY-MM-DD'),
    TO_DATE('16:55:57', 'HH24:MI:SS'),
    7,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    119,
    TO_DATE('2020-01-19', 'YY-MM-DD'),
    TO_DATE('12:10:20', 'HH24:MI:SS'),
    13,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    20,
    TO_DATE('2020-03-08', 'YY-MM-DD'),
    TO_DATE('02:55:28', 'HH24:MI:SS'),
    5,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    86,
    TO_DATE('2020-03-04', 'YY-MM-DD'),
    TO_DATE('19:18:46', 'HH24:MI:SS'),
    12,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    108,
    TO_DATE('2020-01-20', 'YY-MM-DD'),
    TO_DATE('02:04:52', 'HH24:MI:SS'),
    4,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    119,
    TO_DATE('2019-05-10', 'YY-MM-DD'),
    TO_DATE('15:41:51', 'HH24:MI:SS'),
    7,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    66,
    TO_DATE('2019-09-03', 'YY-MM-DD'),
    TO_DATE('04:12:38', 'HH24:MI:SS'),
    1,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    121,
    TO_DATE('2019-11-10', 'YY-MM-DD'),
    TO_DATE('00:32:18', 'HH24:MI:SS'),
    13,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    127,
    TO_DATE('2019-12-10', 'YY-MM-DD'),
    TO_DATE('21:10:04', 'HH24:MI:SS'),
    11,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    25,
    TO_DATE('2019-11-04', 'YY-MM-DD'),
    TO_DATE('17:44:34', 'HH24:MI:SS'),
    3,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    82,
    TO_DATE('2019-06-19', 'YY-MM-DD'),
    TO_DATE('12:17:30', 'HH24:MI:SS'),
    15,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    129,
    TO_DATE('2020-01-21', 'YY-MM-DD'),
    TO_DATE('13:15:32', 'HH24:MI:SS'),
    8,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    129,
    TO_DATE('2019-06-18', 'YY-MM-DD'),
    TO_DATE('14:24:54', 'HH24:MI:SS'),
    14,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    99,
    TO_DATE('2019-06-12', 'YY-MM-DD'),
    TO_DATE('07:02:37', 'HH24:MI:SS'),
    10,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    136,
    TO_DATE('2020-02-24', 'YY-MM-DD'),
    TO_DATE('18:41:31', 'HH24:MI:SS'),
    7,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    141,
    TO_DATE('2019-12-16', 'YY-MM-DD'),
    TO_DATE('19:33:00', 'HH24:MI:SS'),
    13,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    132,
    TO_DATE('2019-08-03', 'YY-MM-DD'),
    TO_DATE('18:39:45', 'HH24:MI:SS'),
    13,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    73,
    TO_DATE('2020-04-02', 'YY-MM-DD'),
    TO_DATE('04:52:50', 'HH24:MI:SS'),
    15,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    39,
    TO_DATE('2020-01-16', 'YY-MM-DD'),
    TO_DATE('16:16:36', 'HH24:MI:SS'),
    10,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    36,
    TO_DATE('2019-12-03', 'YY-MM-DD'),
    TO_DATE('21:02:43', 'HH24:MI:SS'),
    5,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    106,
    TO_DATE('2019-07-29', 'YY-MM-DD'),
    TO_DATE('08:45:54', 'HH24:MI:SS'),
    6,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    80,
    TO_DATE('2019-07-24', 'YY-MM-DD'),
    TO_DATE('04:37:37', 'HH24:MI:SS'),
    6,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    35,
    TO_DATE('2019-08-09', 'YY-MM-DD'),
    TO_DATE('00:13:01', 'HH24:MI:SS'),
    9,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    96,
    TO_DATE('2019-10-15', 'YY-MM-DD'),
    TO_DATE('14:57:48', 'HH24:MI:SS'),
    4,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    2,
    TO_DATE('2019-08-08', 'YY-MM-DD'),
    TO_DATE('02:20:44', 'HH24:MI:SS'),
    13,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    113,
    TO_DATE('2019-04-26', 'YY-MM-DD'),
    TO_DATE('17:28:59', 'HH24:MI:SS'),
    9,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    78,
    TO_DATE('2019-07-11', 'YY-MM-DD'),
    TO_DATE('20:31:30', 'HH24:MI:SS'),
    3,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    99,
    TO_DATE('2019-10-02', 'YY-MM-DD'),
    TO_DATE('21:03:31', 'HH24:MI:SS'),
    8,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    92,
    TO_DATE('2019-08-02', 'YY-MM-DD'),
    TO_DATE('16:40:55', 'HH24:MI:SS'),
    12,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    95,
    TO_DATE('2019-09-11', 'YY-MM-DD'),
    TO_DATE('15:57:18', 'HH24:MI:SS'),
    14,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    129,
    TO_DATE('2020-02-25', 'YY-MM-DD'),
    TO_DATE('21:23:48', 'HH24:MI:SS'),
    11,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    117,
    TO_DATE('2019-10-27', 'YY-MM-DD'),
    TO_DATE('19:43:55', 'HH24:MI:SS'),
    10,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    87,
    TO_DATE('2019-11-17', 'YY-MM-DD'),
    TO_DATE('15:37:42', 'HH24:MI:SS'),
    8,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    87,
    TO_DATE('2020-03-04', 'YY-MM-DD'),
    TO_DATE('10:37:21', 'HH24:MI:SS'),
    7,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    3,
    TO_DATE('2019-05-06', 'YY-MM-DD'),
    TO_DATE('07:58:07', 'HH24:MI:SS'),
    11,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    146,
    TO_DATE('2019-06-28', 'YY-MM-DD'),
    TO_DATE('07:58:28', 'HH24:MI:SS'),
    10,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    119,
    TO_DATE('2019-05-07', 'YY-MM-DD'),
    TO_DATE('16:22:18', 'HH24:MI:SS'),
    7,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    101,
    TO_DATE('2019-05-27', 'YY-MM-DD'),
    TO_DATE('21:48:13', 'HH24:MI:SS'),
    9,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    138,
    TO_DATE('2019-08-08', 'YY-MM-DD'),
    TO_DATE('10:42:24', 'HH24:MI:SS'),
    7,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    16,
    TO_DATE('2020-03-10', 'YY-MM-DD'),
    TO_DATE('11:40:52', 'HH24:MI:SS'),
    3,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    145,
    TO_DATE('2020-04-15', 'YY-MM-DD'),
    TO_DATE('05:59:33', 'HH24:MI:SS'),
    9,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    56,
    TO_DATE('2019-12-04', 'YY-MM-DD'),
    TO_DATE('20:24:17', 'HH24:MI:SS'),
    7,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    106,
    TO_DATE('2020-03-01', 'YY-MM-DD'),
    TO_DATE('13:26:54', 'HH24:MI:SS'),
    1,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    91,
    TO_DATE('2019-07-18', 'YY-MM-DD'),
    TO_DATE('08:03:47', 'HH24:MI:SS'),
    12,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    39,
    TO_DATE('2019-06-02', 'YY-MM-DD'),
    TO_DATE('14:11:56', 'HH24:MI:SS'),
    12,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    87,
    TO_DATE('2019-05-28', 'YY-MM-DD'),
    TO_DATE('18:33:45', 'HH24:MI:SS'),
    2,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    147,
    TO_DATE('2020-02-04', 'YY-MM-DD'),
    TO_DATE('07:44:41', 'HH24:MI:SS'),
    3,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    138,
    TO_DATE('2020-01-01', 'YY-MM-DD'),
    TO_DATE('23:19:02', 'HH24:MI:SS'),
    13,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    86,
    TO_DATE('2020-04-07', 'YY-MM-DD'),
    TO_DATE('13:26:36', 'HH24:MI:SS'),
    13,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    11,
    TO_DATE('2019-10-16', 'YY-MM-DD'),
    TO_DATE('14:24:17', 'HH24:MI:SS'),
    15,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    57,
    TO_DATE('2019-08-27', 'YY-MM-DD'),
    TO_DATE('00:10:00', 'HH24:MI:SS'),
    7,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    96,
    TO_DATE('2019-12-25', 'YY-MM-DD'),
    TO_DATE('09:59:51', 'HH24:MI:SS'),
    6,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    84,
    TO_DATE('2019-07-10', 'YY-MM-DD'),
    TO_DATE('21:53:57', 'HH24:MI:SS'),
    5,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    140,
    TO_DATE('2020-04-07', 'YY-MM-DD'),
    TO_DATE('01:50:33', 'HH24:MI:SS'),
    2,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    107,
    TO_DATE('2019-05-09', 'YY-MM-DD'),
    TO_DATE('07:56:56', 'HH24:MI:SS'),
    13,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    43,
    TO_DATE('2019-10-08', 'YY-MM-DD'),
    TO_DATE('09:36:14', 'HH24:MI:SS'),
    12,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    34,
    TO_DATE('2019-12-07', 'YY-MM-DD'),
    TO_DATE('07:29:16', 'HH24:MI:SS'),
    8,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    49,
    TO_DATE('2019-07-06', 'YY-MM-DD'),
    TO_DATE('08:04:19', 'HH24:MI:SS'),
    13,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    145,
    TO_DATE('2020-04-18', 'YY-MM-DD'),
    TO_DATE('22:29:26', 'HH24:MI:SS'),
    9,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    136,
    TO_DATE('2019-11-28', 'YY-MM-DD'),
    TO_DATE('08:43:22', 'HH24:MI:SS'),
    3,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    22,
    TO_DATE('2019-11-05', 'YY-MM-DD'),
    TO_DATE('11:09:50', 'HH24:MI:SS'),
    3,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    23,
    TO_DATE('2019-11-08', 'YY-MM-DD'),
    TO_DATE('21:19:43', 'HH24:MI:SS'),
    13,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    36,
    TO_DATE('2020-01-02', 'YY-MM-DD'),
    TO_DATE('22:03:10', 'HH24:MI:SS'),
    7,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    113,
    TO_DATE('2020-03-13', 'YY-MM-DD'),
    TO_DATE('13:04:45', 'HH24:MI:SS'),
    14,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    138,
    TO_DATE('2020-02-14', 'YY-MM-DD'),
    TO_DATE('23:51:18', 'HH24:MI:SS'),
    9,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    116,
    TO_DATE('2019-08-23', 'YY-MM-DD'),
    TO_DATE('18:47:39', 'HH24:MI:SS'),
    13,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    128,
    TO_DATE('2019-08-04', 'YY-MM-DD'),
    TO_DATE('03:59:43', 'HH24:MI:SS'),
    7,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    28,
    TO_DATE('2019-11-26', 'YY-MM-DD'),
    TO_DATE('22:14:32', 'HH24:MI:SS'),
    8,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    119,
    TO_DATE('2020-03-25', 'YY-MM-DD'),
    TO_DATE('00:48:50', 'HH24:MI:SS'),
    4,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    88,
    TO_DATE('2019-12-16', 'YY-MM-DD'),
    TO_DATE('20:37:19', 'HH24:MI:SS'),
    8,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    54,
    TO_DATE('2019-06-15', 'YY-MM-DD'),
    TO_DATE('20:14:02', 'HH24:MI:SS'),
    6,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    86,
    TO_DATE('2020-02-19', 'YY-MM-DD'),
    TO_DATE('01:05:12', 'HH24:MI:SS'),
    11,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    50,
    TO_DATE('2019-08-07', 'YY-MM-DD'),
    TO_DATE('00:19:39', 'HH24:MI:SS'),
    7,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    20,
    TO_DATE('2019-06-15', 'YY-MM-DD'),
    TO_DATE('14:51:17', 'HH24:MI:SS'),
    1,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    116,
    TO_DATE('2019-06-28', 'YY-MM-DD'),
    TO_DATE('00:24:56', 'HH24:MI:SS'),
    15,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    119,
    TO_DATE('2019-07-28', 'YY-MM-DD'),
    TO_DATE('04:21:39', 'HH24:MI:SS'),
    7,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    40,
    TO_DATE('2019-08-16', 'YY-MM-DD'),
    TO_DATE('22:04:10', 'HH24:MI:SS'),
    4,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    108,
    TO_DATE('2020-01-26', 'YY-MM-DD'),
    TO_DATE('06:53:32', 'HH24:MI:SS'),
    2,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    86,
    TO_DATE('2019-11-14', 'YY-MM-DD'),
    TO_DATE('01:20:20', 'HH24:MI:SS'),
    2,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    78,
    TO_DATE('2019-06-01', 'YY-MM-DD'),
    TO_DATE('14:16:47', 'HH24:MI:SS'),
    7,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    73,
    TO_DATE('2019-07-28', 'YY-MM-DD'),
    TO_DATE('07:44:03', 'HH24:MI:SS'),
    8,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    67,
    TO_DATE('2020-03-12', 'YY-MM-DD'),
    TO_DATE('19:59:19', 'HH24:MI:SS'),
    11,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    29,
    TO_DATE('2020-02-29', 'YY-MM-DD'),
    TO_DATE('14:45:04', 'HH24:MI:SS'),
    5,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    65,
    TO_DATE('2019-06-07', 'YY-MM-DD'),
    TO_DATE('14:42:04', 'HH24:MI:SS'),
    6,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    123,
    TO_DATE('2020-02-25', 'YY-MM-DD'),
    TO_DATE('08:04:03', 'HH24:MI:SS'),
    5,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    36,
    TO_DATE('2019-10-11', 'YY-MM-DD'),
    TO_DATE('11:07:46', 'HH24:MI:SS'),
    9,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    81,
    TO_DATE('2019-11-17', 'YY-MM-DD'),
    TO_DATE('03:48:17', 'HH24:MI:SS'),
    8,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    134,
    TO_DATE('2019-05-28', 'YY-MM-DD'),
    TO_DATE('08:57:14', 'HH24:MI:SS'),
    6,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    91,
    TO_DATE('2019-12-16', 'YY-MM-DD'),
    TO_DATE('01:38:40', 'HH24:MI:SS'),
    15,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    41,
    TO_DATE('2020-04-02', 'YY-MM-DD'),
    TO_DATE('08:21:20', 'HH24:MI:SS'),
    10,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    79,
    TO_DATE('2019-10-25', 'YY-MM-DD'),
    TO_DATE('12:44:29', 'HH24:MI:SS'),
    2,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    68,
    TO_DATE('2019-08-15', 'YY-MM-DD'),
    TO_DATE('18:07:23', 'HH24:MI:SS'),
    15,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    130,
    TO_DATE('2019-07-09', 'YY-MM-DD'),
    TO_DATE('16:14:37', 'HH24:MI:SS'),
    3,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    148,
    TO_DATE('2019-07-21', 'YY-MM-DD'),
    TO_DATE('13:37:34', 'HH24:MI:SS'),
    5,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    90,
    TO_DATE('2020-01-18', 'YY-MM-DD'),
    TO_DATE('11:55:58', 'HH24:MI:SS'),
    14,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    24,
    TO_DATE('2019-10-20', 'YY-MM-DD'),
    TO_DATE('13:39:34', 'HH24:MI:SS'),
    10,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    113,
    TO_DATE('2019-06-01', 'YY-MM-DD'),
    TO_DATE('01:39:41', 'HH24:MI:SS'),
    12,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    126,
    TO_DATE('2019-08-29', 'YY-MM-DD'),
    TO_DATE('09:40:19', 'HH24:MI:SS'),
    5,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    18,
    TO_DATE('2020-01-21', 'YY-MM-DD'),
    TO_DATE('09:06:18', 'HH24:MI:SS'),
    1,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    34,
    TO_DATE('2019-12-25', 'YY-MM-DD'),
    TO_DATE('01:31:58', 'HH24:MI:SS'),
    2,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    99,
    TO_DATE('2019-11-25', 'YY-MM-DD'),
    TO_DATE('02:34:53', 'HH24:MI:SS'),
    8,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    137,
    TO_DATE('2019-09-25', 'YY-MM-DD'),
    TO_DATE('04:17:05', 'HH24:MI:SS'),
    8,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    41,
    TO_DATE('2019-11-04', 'YY-MM-DD'),
    TO_DATE('10:35:14', 'HH24:MI:SS'),
    9,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    65,
    TO_DATE('2019-08-16', 'YY-MM-DD'),
    TO_DATE('14:30:20', 'HH24:MI:SS'),
    10,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    55,
    TO_DATE('2019-07-16', 'YY-MM-DD'),
    TO_DATE('19:25:15', 'HH24:MI:SS'),
    11,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    35,
    TO_DATE('2020-03-13', 'YY-MM-DD'),
    TO_DATE('00:42:50', 'HH24:MI:SS'),
    15,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    137,
    TO_DATE('2019-12-13', 'YY-MM-DD'),
    TO_DATE('11:17:07', 'HH24:MI:SS'),
    15,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    25,
    TO_DATE('2019-06-06', 'YY-MM-DD'),
    TO_DATE('08:34:07', 'HH24:MI:SS'),
    14,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    46,
    TO_DATE('2020-04-07', 'YY-MM-DD'),
    TO_DATE('00:37:40', 'HH24:MI:SS'),
    7,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    71,
    TO_DATE('2019-12-22', 'YY-MM-DD'),
    TO_DATE('15:01:30', 'HH24:MI:SS'),
    6,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    1,
    TO_DATE('2020-04-24', 'YY-MM-DD'),
    TO_DATE('10:56:44', 'HH24:MI:SS'),
    8,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    34,
    TO_DATE('2019-08-13', 'YY-MM-DD'),
    TO_DATE('11:10:35', 'HH24:MI:SS'),
    14,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    58,
    TO_DATE('2019-07-21', 'YY-MM-DD'),
    TO_DATE('11:56:58', 'HH24:MI:SS'),
    1,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    71,
    TO_DATE('2019-12-01', 'YY-MM-DD'),
    TO_DATE('00:20:47', 'HH24:MI:SS'),
    6,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    138,
    TO_DATE('2019-06-01', 'YY-MM-DD'),
    TO_DATE('21:08:07', 'HH24:MI:SS'),
    13,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    137,
    TO_DATE('2019-11-02', 'YY-MM-DD'),
    TO_DATE('15:29:38', 'HH24:MI:SS'),
    6,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    126,
    TO_DATE('2020-02-29', 'YY-MM-DD'),
    TO_DATE('00:41:50', 'HH24:MI:SS'),
    3,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    44,
    TO_DATE('2020-01-23', 'YY-MM-DD'),
    TO_DATE('15:00:24', 'HH24:MI:SS'),
    10,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    94,
    TO_DATE('2020-04-04', 'YY-MM-DD'),
    TO_DATE('16:14:27', 'HH24:MI:SS'),
    13,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    71,
    TO_DATE('2019-09-24', 'YY-MM-DD'),
    TO_DATE('01:11:22', 'HH24:MI:SS'),
    14,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    73,
    TO_DATE('2019-07-18', 'YY-MM-DD'),
    TO_DATE('04:46:45', 'HH24:MI:SS'),
    10,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    38,
    TO_DATE('2020-02-11', 'YY-MM-DD'),
    TO_DATE('10:54:24', 'HH24:MI:SS'),
    15,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    140,
    TO_DATE('2020-01-30', 'YY-MM-DD'),
    TO_DATE('08:44:10', 'HH24:MI:SS'),
    9,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    141,
    TO_DATE('2020-03-28', 'YY-MM-DD'),
    TO_DATE('08:17:45', 'HH24:MI:SS'),
    8,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    95,
    TO_DATE('2019-07-16', 'YY-MM-DD'),
    TO_DATE('11:26:10', 'HH24:MI:SS'),
    4,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    40,
    TO_DATE('2019-08-05', 'YY-MM-DD'),
    TO_DATE('03:27:41', 'HH24:MI:SS'),
    10,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    15,
    TO_DATE('2019-05-14', 'YY-MM-DD'),
    TO_DATE('04:12:49', 'HH24:MI:SS'),
    4,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    149,
    TO_DATE('2020-01-20', 'YY-MM-DD'),
    TO_DATE('04:20:54', 'HH24:MI:SS'),
    14,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    109,
    TO_DATE('2019-08-21', 'YY-MM-DD'),
    TO_DATE('12:34:52', 'HH24:MI:SS'),
    14,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    80,
    TO_DATE('2020-03-18', 'YY-MM-DD'),
    TO_DATE('09:48:07', 'HH24:MI:SS'),
    5,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    10,
    TO_DATE('2019-11-01', 'YY-MM-DD'),
    TO_DATE('21:53:38', 'HH24:MI:SS'),
    2,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    35,
    TO_DATE('2019-07-31', 'YY-MM-DD'),
    TO_DATE('23:12:30', 'HH24:MI:SS'),
    8,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    4,
    TO_DATE('2020-04-03', 'YY-MM-DD'),
    TO_DATE('11:33:44', 'HH24:MI:SS'),
    10,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    136,
    TO_DATE('2019-11-06', 'YY-MM-DD'),
    TO_DATE('08:15:51', 'HH24:MI:SS'),
    11,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    28,
    TO_DATE('2019-12-31', 'YY-MM-DD'),
    TO_DATE('03:28:09', 'HH24:MI:SS'),
    5,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    39,
    TO_DATE('2019-07-13', 'YY-MM-DD'),
    TO_DATE('01:41:13', 'HH24:MI:SS'),
    15,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    124,
    TO_DATE('2020-02-22', 'YY-MM-DD'),
    TO_DATE('10:31:13', 'HH24:MI:SS'),
    7,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    148,
    TO_DATE('2019-07-05', 'YY-MM-DD'),
    TO_DATE('23:51:00', 'HH24:MI:SS'),
    12,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    121,
    TO_DATE('2019-09-30', 'YY-MM-DD'),
    TO_DATE('19:54:45', 'HH24:MI:SS'),
    13,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    5,
    TO_DATE('2020-04-22', 'YY-MM-DD'),
    TO_DATE('13:28:26', 'HH24:MI:SS'),
    6,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    132,
    TO_DATE('2019-09-25', 'YY-MM-DD'),
    TO_DATE('19:04:38', 'HH24:MI:SS'),
    12,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    119,
    TO_DATE('2019-11-28', 'YY-MM-DD'),
    TO_DATE('16:14:38', 'HH24:MI:SS'),
    12,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    48,
    TO_DATE('2020-04-02', 'YY-MM-DD'),
    TO_DATE('19:11:05', 'HH24:MI:SS'),
    11,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    107,
    TO_DATE('2020-01-07', 'YY-MM-DD'),
    TO_DATE('12:27:28', 'HH24:MI:SS'),
    4,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    119,
    TO_DATE('2019-06-30', 'YY-MM-DD'),
    TO_DATE('18:47:42', 'HH24:MI:SS'),
    8,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    38,
    TO_DATE('2019-09-13', 'YY-MM-DD'),
    TO_DATE('23:37:37', 'HH24:MI:SS'),
    14,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    119,
    TO_DATE('2020-04-12', 'YY-MM-DD'),
    TO_DATE('20:49:19', 'HH24:MI:SS'),
    6,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    106,
    TO_DATE('2019-12-03', 'YY-MM-DD'),
    TO_DATE('21:18:21', 'HH24:MI:SS'),
    2,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    39,
    TO_DATE('2019-08-22', 'YY-MM-DD'),
    TO_DATE('05:17:10', 'HH24:MI:SS'),
    9,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    97,
    TO_DATE('2019-12-21', 'YY-MM-DD'),
    TO_DATE('16:40:25', 'HH24:MI:SS'),
    8,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    31,
    TO_DATE('2020-01-10', 'YY-MM-DD'),
    TO_DATE('23:31:49', 'HH24:MI:SS'),
    5,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    138,
    TO_DATE('2019-07-15', 'YY-MM-DD'),
    TO_DATE('19:26:12', 'HH24:MI:SS'),
    1,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    91,
    TO_DATE('2019-05-11', 'YY-MM-DD'),
    TO_DATE('18:53:55', 'HH24:MI:SS'),
    4,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    37,
    TO_DATE('2019-08-13', 'YY-MM-DD'),
    TO_DATE('19:51:24', 'HH24:MI:SS'),
    13,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    138,
    TO_DATE('2020-02-24', 'YY-MM-DD'),
    TO_DATE('13:37:45', 'HH24:MI:SS'),
    14,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    148,
    TO_DATE('2019-09-22', 'YY-MM-DD'),
    TO_DATE('15:26:18', 'HH24:MI:SS'),
    6,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    140,
    TO_DATE('2019-07-01', 'YY-MM-DD'),
    TO_DATE('19:54:18', 'HH24:MI:SS'),
    15,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    28,
    TO_DATE('2020-03-06', 'YY-MM-DD'),
    TO_DATE('20:30:17', 'HH24:MI:SS'),
    15,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    19,
    TO_DATE('2020-01-15', 'YY-MM-DD'),
    TO_DATE('09:24:17', 'HH24:MI:SS'),
    9,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    83,
    TO_DATE('2019-12-15', 'YY-MM-DD'),
    TO_DATE('12:58:16', 'HH24:MI:SS'),
    13,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    96,
    TO_DATE('2019-09-14', 'YY-MM-DD'),
    TO_DATE('18:28:02', 'HH24:MI:SS'),
    13,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    24,
    TO_DATE('2020-01-26', 'YY-MM-DD'),
    TO_DATE('11:27:04', 'HH24:MI:SS'),
    6,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    97,
    TO_DATE('2019-09-20', 'YY-MM-DD'),
    TO_DATE('17:46:03', 'HH24:MI:SS'),
    13,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    56,
    TO_DATE('2019-12-11', 'YY-MM-DD'),
    TO_DATE('21:25:13', 'HH24:MI:SS'),
    14,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    135,
    TO_DATE('2020-03-23', 'YY-MM-DD'),
    TO_DATE('19:30:19', 'HH24:MI:SS'),
    1,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    14,
    TO_DATE('2019-08-25', 'YY-MM-DD'),
    TO_DATE('01:09:16', 'HH24:MI:SS'),
    3,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    70,
    TO_DATE('2020-01-24', 'YY-MM-DD'),
    TO_DATE('19:59:23', 'HH24:MI:SS'),
    12,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    54,
    TO_DATE('2019-09-01', 'YY-MM-DD'),
    TO_DATE('09:19:27', 'HH24:MI:SS'),
    12,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    91,
    TO_DATE('2019-06-15', 'YY-MM-DD'),
    TO_DATE('20:57:42', 'HH24:MI:SS'),
    15,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    148,
    TO_DATE('2019-12-19', 'YY-MM-DD'),
    TO_DATE('02:47:49', 'HH24:MI:SS'),
    1,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    110,
    TO_DATE('2019-06-24', 'YY-MM-DD'),
    TO_DATE('04:19:50', 'HH24:MI:SS'),
    1,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    64,
    TO_DATE('2019-12-30', 'YY-MM-DD'),
    TO_DATE('19:15:54', 'HH24:MI:SS'),
    5,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    55,
    TO_DATE('2020-01-22', 'YY-MM-DD'),
    TO_DATE('13:50:36', 'HH24:MI:SS'),
    11,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    3,
    TO_DATE('2020-03-17', 'YY-MM-DD'),
    TO_DATE('18:05:42', 'HH24:MI:SS'),
    2,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    43,
    TO_DATE('2019-10-23', 'YY-MM-DD'),
    TO_DATE('11:42:34', 'HH24:MI:SS'),
    13,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    79,
    TO_DATE('2020-04-03', 'YY-MM-DD'),
    TO_DATE('07:39:43', 'HH24:MI:SS'),
    15,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    43,
    TO_DATE('2019-12-05', 'YY-MM-DD'),
    TO_DATE('14:30:09', 'HH24:MI:SS'),
    10,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    111,
    TO_DATE('2019-08-29', 'YY-MM-DD'),
    TO_DATE('03:15:53', 'HH24:MI:SS'),
    6,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    97,
    TO_DATE('2019-06-05', 'YY-MM-DD'),
    TO_DATE('08:07:12', 'HH24:MI:SS'),
    13,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    37,
    TO_DATE('2019-07-25', 'YY-MM-DD'),
    TO_DATE('04:26:11', 'HH24:MI:SS'),
    14,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    28,
    TO_DATE('2019-08-24', 'YY-MM-DD'),
    TO_DATE('09:12:34', 'HH24:MI:SS'),
    14,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    120,
    TO_DATE('2019-11-26', 'YY-MM-DD'),
    TO_DATE('08:45:03', 'HH24:MI:SS'),
    14,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    115,
    TO_DATE('2019-04-26', 'YY-MM-DD'),
    TO_DATE('21:17:15', 'HH24:MI:SS'),
    11,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    122,
    TO_DATE('2019-05-03', 'YY-MM-DD'),
    TO_DATE('15:23:01', 'HH24:MI:SS'),
    12,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    14,
    TO_DATE('2019-09-14', 'YY-MM-DD'),
    TO_DATE('06:07:27', 'HH24:MI:SS'),
    7,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    131,
    TO_DATE('2020-01-27', 'YY-MM-DD'),
    TO_DATE('01:16:19', 'HH24:MI:SS'),
    6,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    74,
    TO_DATE('2020-02-09', 'YY-MM-DD'),
    TO_DATE('02:38:12', 'HH24:MI:SS'),
    2,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    6,
    TO_DATE('2020-04-20', 'YY-MM-DD'),
    TO_DATE('10:27:14', 'HH24:MI:SS'),
    5,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    102,
    TO_DATE('2019-12-18', 'YY-MM-DD'),
    TO_DATE('12:24:40', 'HH24:MI:SS'),
    8,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    33,
    TO_DATE('2020-03-25', 'YY-MM-DD'),
    TO_DATE('01:02:23', 'HH24:MI:SS'),
    13,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    43,
    TO_DATE('2019-06-19', 'YY-MM-DD'),
    TO_DATE('06:43:13', 'HH24:MI:SS'),
    11,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    138,
    TO_DATE('2019-05-01', 'YY-MM-DD'),
    TO_DATE('02:11:21', 'HH24:MI:SS'),
    5,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    35,
    TO_DATE('2020-01-06', 'YY-MM-DD'),
    TO_DATE('18:27:39', 'HH24:MI:SS'),
    2,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    88,
    TO_DATE('2019-10-18', 'YY-MM-DD'),
    TO_DATE('13:29:15', 'HH24:MI:SS'),
    2,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    38,
    TO_DATE('2019-08-29', 'YY-MM-DD'),
    TO_DATE('00:59:41', 'HH24:MI:SS'),
    12,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    115,
    TO_DATE('2019-09-04', 'YY-MM-DD'),
    TO_DATE('10:53:19', 'HH24:MI:SS'),
    8,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    57,
    TO_DATE('2019-09-01', 'YY-MM-DD'),
    TO_DATE('05:32:30', 'HH24:MI:SS'),
    6,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    12,
    TO_DATE('2019-12-24', 'YY-MM-DD'),
    TO_DATE('16:38:55', 'HH24:MI:SS'),
    4,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    139,
    TO_DATE('2019-10-12', 'YY-MM-DD'),
    TO_DATE('20:40:53', 'HH24:MI:SS'),
    7,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    124,
    TO_DATE('2019-09-14', 'YY-MM-DD'),
    TO_DATE('13:02:14', 'HH24:MI:SS'),
    7,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    39,
    TO_DATE('2019-11-17', 'YY-MM-DD'),
    TO_DATE('03:16:48', 'HH24:MI:SS'),
    6,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    6,
    TO_DATE('2019-11-24', 'YY-MM-DD'),
    TO_DATE('23:28:31', 'HH24:MI:SS'),
    7,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    110,
    TO_DATE('2019-11-18', 'YY-MM-DD'),
    TO_DATE('11:09:20', 'HH24:MI:SS'),
    12,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    56,
    TO_DATE('2019-06-17', 'YY-MM-DD'),
    TO_DATE('16:12:39', 'HH24:MI:SS'),
    14,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    28,
    TO_DATE('2019-06-24', 'YY-MM-DD'),
    TO_DATE('05:02:41', 'HH24:MI:SS'),
    2,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    1,
    TO_DATE('2019-08-08', 'YY-MM-DD'),
    TO_DATE('21:57:45', 'HH24:MI:SS'),
    8,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    127,
    TO_DATE('2020-01-13', 'YY-MM-DD'),
    TO_DATE('18:38:13', 'HH24:MI:SS'),
    8,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    42,
    TO_DATE('2020-01-04', 'YY-MM-DD'),
    TO_DATE('17:21:09', 'HH24:MI:SS'),
    1,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    145,
    TO_DATE('2019-11-15', 'YY-MM-DD'),
    TO_DATE('01:42:50', 'HH24:MI:SS'),
    1,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    112,
    TO_DATE('2019-07-16', 'YY-MM-DD'),
    TO_DATE('13:46:56', 'HH24:MI:SS'),
    7,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    46,
    TO_DATE('2019-07-28', 'YY-MM-DD'),
    TO_DATE('11:51:10', 'HH24:MI:SS'),
    11,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    19,
    TO_DATE('2019-05-04', 'YY-MM-DD'),
    TO_DATE('17:32:29', 'HH24:MI:SS'),
    6,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    12,
    TO_DATE('2019-07-15', 'YY-MM-DD'),
    TO_DATE('20:09:15', 'HH24:MI:SS'),
    8,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    18,
    TO_DATE('2019-11-25', 'YY-MM-DD'),
    TO_DATE('04:28:41', 'HH24:MI:SS'),
    3,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    130,
    TO_DATE('2019-05-03', 'YY-MM-DD'),
    TO_DATE('23:08:48', 'HH24:MI:SS'),
    6,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    47,
    TO_DATE('2019-08-22', 'YY-MM-DD'),
    TO_DATE('17:28:57', 'HH24:MI:SS'),
    4,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    140,
    TO_DATE('2020-03-26', 'YY-MM-DD'),
    TO_DATE('03:11:47', 'HH24:MI:SS'),
    10,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    50,
    TO_DATE('2019-07-29', 'YY-MM-DD'),
    TO_DATE('21:43:27', 'HH24:MI:SS'),
    4,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    9,
    TO_DATE('2020-04-09', 'YY-MM-DD'),
    TO_DATE('18:43:35', 'HH24:MI:SS'),
    15,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    55,
    TO_DATE('2019-08-14', 'YY-MM-DD'),
    TO_DATE('05:51:06', 'HH24:MI:SS'),
    12,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    55,
    TO_DATE('2019-07-28', 'YY-MM-DD'),
    TO_DATE('23:17:49', 'HH24:MI:SS'),
    11,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    116,
    TO_DATE('2019-12-07', 'YY-MM-DD'),
    TO_DATE('12:45:19', 'HH24:MI:SS'),
    11,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    59,
    TO_DATE('2019-07-19', 'YY-MM-DD'),
    TO_DATE('01:20:55', 'HH24:MI:SS'),
    5,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    116,
    TO_DATE('2019-11-30', 'YY-MM-DD'),
    TO_DATE('20:19:53', 'HH24:MI:SS'),
    1,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    143,
    TO_DATE('2020-03-09', 'YY-MM-DD'),
    TO_DATE('19:43:33', 'HH24:MI:SS'),
    2,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    102,
    TO_DATE('2020-01-01', 'YY-MM-DD'),
    TO_DATE('05:40:40', 'HH24:MI:SS'),
    14,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    119,
    TO_DATE('2019-09-25', 'YY-MM-DD'),
    TO_DATE('11:17:51', 'HH24:MI:SS'),
    2,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    83,
    TO_DATE('2020-01-24', 'YY-MM-DD'),
    TO_DATE('09:51:24', 'HH24:MI:SS'),
    8,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    75,
    TO_DATE('2019-07-20', 'YY-MM-DD'),
    TO_DATE('14:27:56', 'HH24:MI:SS'),
    4,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    69,
    TO_DATE('2020-04-10', 'YY-MM-DD'),
    TO_DATE('15:14:28', 'HH24:MI:SS'),
    3,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    98,
    TO_DATE('2019-08-09', 'YY-MM-DD'),
    TO_DATE('10:43:16', 'HH24:MI:SS'),
    13,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    14,
    TO_DATE('2020-02-28', 'YY-MM-DD'),
    TO_DATE('07:56:55', 'HH24:MI:SS'),
    15,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    35,
    TO_DATE('2020-03-05', 'YY-MM-DD'),
    TO_DATE('17:38:59', 'HH24:MI:SS'),
    5,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    103,
    TO_DATE('2020-01-06', 'YY-MM-DD'),
    TO_DATE('05:42:20', 'HH24:MI:SS'),
    2,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    55,
    TO_DATE('2019-10-22', 'YY-MM-DD'),
    TO_DATE('17:14:50', 'HH24:MI:SS'),
    6,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    77,
    TO_DATE('2020-01-11', 'YY-MM-DD'),
    TO_DATE('09:43:00', 'HH24:MI:SS'),
    8,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    10,
    TO_DATE('2019-08-11', 'YY-MM-DD'),
    TO_DATE('02:11:34', 'HH24:MI:SS'),
    9,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    81,
    TO_DATE('2019-06-12', 'YY-MM-DD'),
    TO_DATE('15:46:23', 'HH24:MI:SS'),
    1,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    41,
    TO_DATE('2020-03-27', 'YY-MM-DD'),
    TO_DATE('09:35:12', 'HH24:MI:SS'),
    1,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    97,
    TO_DATE('2019-05-09', 'YY-MM-DD'),
    TO_DATE('00:58:29', 'HH24:MI:SS'),
    9,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    63,
    TO_DATE('2020-01-02', 'YY-MM-DD'),
    TO_DATE('09:30:39', 'HH24:MI:SS'),
    3,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    9,
    TO_DATE('2020-03-01', 'YY-MM-DD'),
    TO_DATE('13:41:10', 'HH24:MI:SS'),
    9,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    47,
    TO_DATE('2019-08-01', 'YY-MM-DD'),
    TO_DATE('11:12:31', 'HH24:MI:SS'),
    4,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    115,
    TO_DATE('2020-03-09', 'YY-MM-DD'),
    TO_DATE('23:54:04', 'HH24:MI:SS'),
    3,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    25,
    TO_DATE('2020-03-15', 'YY-MM-DD'),
    TO_DATE('16:09:44', 'HH24:MI:SS'),
    9,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    143,
    TO_DATE('2019-07-11', 'YY-MM-DD'),
    TO_DATE('10:33:39', 'HH24:MI:SS'),
    11,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    84,
    TO_DATE('2020-01-01', 'YY-MM-DD'),
    TO_DATE('09:44:05', 'HH24:MI:SS'),
    4,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    9,
    TO_DATE('2020-01-21', 'YY-MM-DD'),
    TO_DATE('03:00:48', 'HH24:MI:SS'),
    8,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    148,
    TO_DATE('2019-05-23', 'YY-MM-DD'),
    TO_DATE('19:57:58', 'HH24:MI:SS'),
    11,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    20,
    TO_DATE('2020-04-07', 'YY-MM-DD'),
    TO_DATE('15:15:24', 'HH24:MI:SS'),
    8,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    25,
    TO_DATE('2019-08-03', 'YY-MM-DD'),
    TO_DATE('04:08:50', 'HH24:MI:SS'),
    6,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    46,
    TO_DATE('2020-03-31', 'YY-MM-DD'),
    TO_DATE('13:09:09', 'HH24:MI:SS'),
    1,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    47,
    TO_DATE('2020-03-19', 'YY-MM-DD'),
    TO_DATE('13:14:14', 'HH24:MI:SS'),
    2,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    76,
    TO_DATE('2019-07-22', 'YY-MM-DD'),
    TO_DATE('09:17:41', 'HH24:MI:SS'),
    5,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    57,
    TO_DATE('2019-11-21', 'YY-MM-DD'),
    TO_DATE('02:51:17', 'HH24:MI:SS'),
    8,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    11,
    TO_DATE('2020-03-24', 'YY-MM-DD'),
    TO_DATE('19:25:00', 'HH24:MI:SS'),
    8,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    5,
    TO_DATE('2020-04-23', 'YY-MM-DD'),
    TO_DATE('12:24:28', 'HH24:MI:SS'),
    2,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    65,
    TO_DATE('2019-05-30', 'YY-MM-DD'),
    TO_DATE('14:55:03', 'HH24:MI:SS'),
    14,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    31,
    TO_DATE('2019-07-18', 'YY-MM-DD'),
    TO_DATE('15:01:57', 'HH24:MI:SS'),
    9,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    39,
    TO_DATE('2019-10-02', 'YY-MM-DD'),
    TO_DATE('10:35:28', 'HH24:MI:SS'),
    12,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    71,
    TO_DATE('2019-07-06', 'YY-MM-DD'),
    TO_DATE('09:16:15', 'HH24:MI:SS'),
    10,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    124,
    TO_DATE('2019-07-29', 'YY-MM-DD'),
    TO_DATE('09:37:50', 'HH24:MI:SS'),
    1,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    62,
    TO_DATE('2019-07-02', 'YY-MM-DD'),
    TO_DATE('16:22:48', 'HH24:MI:SS'),
    13,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    69,
    TO_DATE('2019-11-02', 'YY-MM-DD'),
    TO_DATE('23:36:00', 'HH24:MI:SS'),
    9,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    44,
    TO_DATE('2020-01-02', 'YY-MM-DD'),
    TO_DATE('11:59:01', 'HH24:MI:SS'),
    3,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    126,
    TO_DATE('2019-11-25', 'YY-MM-DD'),
    TO_DATE('17:39:21', 'HH24:MI:SS'),
    2,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    105,
    TO_DATE('2019-08-16', 'YY-MM-DD'),
    TO_DATE('22:57:10', 'HH24:MI:SS'),
    10,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    18,
    TO_DATE('2019-11-09', 'YY-MM-DD'),
    TO_DATE('13:24:23', 'HH24:MI:SS'),
    14,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    70,
    TO_DATE('2019-12-02', 'YY-MM-DD'),
    TO_DATE('11:22:55', 'HH24:MI:SS'),
    4,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    47,
    TO_DATE('2020-03-26', 'YY-MM-DD'),
    TO_DATE('21:55:54', 'HH24:MI:SS'),
    13,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    89,
    TO_DATE('2019-12-21', 'YY-MM-DD'),
    TO_DATE('19:25:24', 'HH24:MI:SS'),
    2,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    129,
    TO_DATE('2019-05-09', 'YY-MM-DD'),
    TO_DATE('08:00:38', 'HH24:MI:SS'),
    7,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    117,
    TO_DATE('2020-01-21', 'YY-MM-DD'),
    TO_DATE('04:57:06', 'HH24:MI:SS'),
    15,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    137,
    TO_DATE('2019-07-28', 'YY-MM-DD'),
    TO_DATE('23:20:40', 'HH24:MI:SS'),
    15,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    12,
    TO_DATE('2019-07-27', 'YY-MM-DD'),
    TO_DATE('08:52:44', 'HH24:MI:SS'),
    8,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    74,
    TO_DATE('2019-06-03', 'YY-MM-DD'),
    TO_DATE('22:39:54', 'HH24:MI:SS'),
    14,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    136,
    TO_DATE('2019-12-29', 'YY-MM-DD'),
    TO_DATE('07:34:45', 'HH24:MI:SS'),
    2,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    113,
    TO_DATE('2019-05-20', 'YY-MM-DD'),
    TO_DATE('07:25:25', 'HH24:MI:SS'),
    13,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    2,
    TO_DATE('2020-04-07', 'YY-MM-DD'),
    TO_DATE('01:25:03', 'HH24:MI:SS'),
    5,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    145,
    TO_DATE('2019-09-11', 'YY-MM-DD'),
    TO_DATE('14:23:36', 'HH24:MI:SS'),
    15,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    123,
    TO_DATE('2020-03-09', 'YY-MM-DD'),
    TO_DATE('18:23:36', 'HH24:MI:SS'),
    14,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    73,
    TO_DATE('2019-12-07', 'YY-MM-DD'),
    TO_DATE('18:54:09', 'HH24:MI:SS'),
    8,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    54,
    TO_DATE('2020-04-24', 'YY-MM-DD'),
    TO_DATE('02:55:38', 'HH24:MI:SS'),
    15,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    97,
    TO_DATE('2020-04-16', 'YY-MM-DD'),
    TO_DATE('07:39:05', 'HH24:MI:SS'),
    15,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    22,
    TO_DATE('2019-05-06', 'YY-MM-DD'),
    TO_DATE('18:14:10', 'HH24:MI:SS'),
    1,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    133,
    TO_DATE('2020-04-17', 'YY-MM-DD'),
    TO_DATE('12:33:27', 'HH24:MI:SS'),
    5,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    98,
    TO_DATE('2019-11-05', 'YY-MM-DD'),
    TO_DATE('14:44:40', 'HH24:MI:SS'),
    1,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    84,
    TO_DATE('2020-03-24', 'YY-MM-DD'),
    TO_DATE('01:50:25', 'HH24:MI:SS'),
    2,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    13,
    TO_DATE('2020-02-19', 'YY-MM-DD'),
    TO_DATE('07:08:53', 'HH24:MI:SS'),
    4,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    7,
    TO_DATE('2020-01-24', 'YY-MM-DD'),
    TO_DATE('06:43:00', 'HH24:MI:SS'),
    8,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    19,
    TO_DATE('2019-08-07', 'YY-MM-DD'),
    TO_DATE('02:11:22', 'HH24:MI:SS'),
    1,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    62,
    TO_DATE('2019-05-03', 'YY-MM-DD'),
    TO_DATE('08:16:10', 'HH24:MI:SS'),
    15,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    23,
    TO_DATE('2019-10-20', 'YY-MM-DD'),
    TO_DATE('18:19:56', 'HH24:MI:SS'),
    8,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    137,
    TO_DATE('2019-07-31', 'YY-MM-DD'),
    TO_DATE('15:32:31', 'HH24:MI:SS'),
    1,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    30,
    TO_DATE('2020-04-07', 'YY-MM-DD'),
    TO_DATE('22:15:18', 'HH24:MI:SS'),
    10,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    63,
    TO_DATE('2020-01-21', 'YY-MM-DD'),
    TO_DATE('21:19:44', 'HH24:MI:SS'),
    13,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    131,
    TO_DATE('2019-05-13', 'YY-MM-DD'),
    TO_DATE('04:42:22', 'HH24:MI:SS'),
    2,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    136,
    TO_DATE('2019-11-07', 'YY-MM-DD'),
    TO_DATE('13:51:58', 'HH24:MI:SS'),
    11,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    35,
    TO_DATE('2019-10-09', 'YY-MM-DD'),
    TO_DATE('09:14:53', 'HH24:MI:SS'),
    3,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    30,
    TO_DATE('2019-05-24', 'YY-MM-DD'),
    TO_DATE('01:10:39', 'HH24:MI:SS'),
    10,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    13,
    TO_DATE('2020-02-16', 'YY-MM-DD'),
    TO_DATE('09:27:36', 'HH24:MI:SS'),
    3,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    89,
    TO_DATE('2020-04-20', 'YY-MM-DD'),
    TO_DATE('03:51:16', 'HH24:MI:SS'),
    4,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    76,
    TO_DATE('2019-08-23', 'YY-MM-DD'),
    TO_DATE('21:05:17', 'HH24:MI:SS'),
    1,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    67,
    TO_DATE('2019-12-22', 'YY-MM-DD'),
    TO_DATE('01:22:00', 'HH24:MI:SS'),
    2,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    66,
    TO_DATE('2020-02-03', 'YY-MM-DD'),
    TO_DATE('01:00:23', 'HH24:MI:SS'),
    11,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    132,
    TO_DATE('2019-06-11', 'YY-MM-DD'),
    TO_DATE('21:44:19', 'HH24:MI:SS'),
    6,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    137,
    TO_DATE('2019-10-02', 'YY-MM-DD'),
    TO_DATE('13:19:22', 'HH24:MI:SS'),
    3,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    74,
    TO_DATE('2019-07-18', 'YY-MM-DD'),
    TO_DATE('09:06:15', 'HH24:MI:SS'),
    9,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    131,
    TO_DATE('2020-04-19', 'YY-MM-DD'),
    TO_DATE('21:56:02', 'HH24:MI:SS'),
    12,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    129,
    TO_DATE('2019-08-16', 'YY-MM-DD'),
    TO_DATE('01:42:27', 'HH24:MI:SS'),
    8,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    72,
    TO_DATE('2020-04-14', 'YY-MM-DD'),
    TO_DATE('18:57:23', 'HH24:MI:SS'),
    4,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    83,
    TO_DATE('2019-05-12', 'YY-MM-DD'),
    TO_DATE('20:12:49', 'HH24:MI:SS'),
    8,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    64,
    TO_DATE('2019-08-27', 'YY-MM-DD'),
    TO_DATE('20:21:58', 'HH24:MI:SS'),
    10,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    146,
    TO_DATE('2020-03-16', 'YY-MM-DD'),
    TO_DATE('11:01:18', 'HH24:MI:SS'),
    10,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    135,
    TO_DATE('2019-09-22', 'YY-MM-DD'),
    TO_DATE('08:10:01', 'HH24:MI:SS'),
    12,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    92,
    TO_DATE('2019-09-09', 'YY-MM-DD'),
    TO_DATE('22:38:12', 'HH24:MI:SS'),
    9,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    74,
    TO_DATE('2019-07-22', 'YY-MM-DD'),
    TO_DATE('21:26:04', 'HH24:MI:SS'),
    1,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    43,
    TO_DATE('2020-01-04', 'YY-MM-DD'),
    TO_DATE('13:00:35', 'HH24:MI:SS'),
    12,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    137,
    TO_DATE('2020-03-28', 'YY-MM-DD'),
    TO_DATE('00:21:16', 'HH24:MI:SS'),
    3,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    114,
    TO_DATE('2019-05-02', 'YY-MM-DD'),
    TO_DATE('12:27:13', 'HH24:MI:SS'),
    10,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    32,
    TO_DATE('2019-12-10', 'YY-MM-DD'),
    TO_DATE('12:22:59', 'HH24:MI:SS'),
    1,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    36,
    TO_DATE('2019-11-18', 'YY-MM-DD'),
    TO_DATE('05:16:59', 'HH24:MI:SS'),
    10,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    111,
    TO_DATE('2019-08-21', 'YY-MM-DD'),
    TO_DATE('15:50:45', 'HH24:MI:SS'),
    7,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    144,
    TO_DATE('2019-10-19', 'YY-MM-DD'),
    TO_DATE('11:37:23', 'HH24:MI:SS'),
    8,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    30,
    TO_DATE('2019-08-29', 'YY-MM-DD'),
    TO_DATE('21:09:14', 'HH24:MI:SS'),
    10,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    19,
    TO_DATE('2020-03-12', 'YY-MM-DD'),
    TO_DATE('16:33:33', 'HH24:MI:SS'),
    14,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    37,
    TO_DATE('2019-11-12', 'YY-MM-DD'),
    TO_DATE('01:03:41', 'HH24:MI:SS'),
    14,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    81,
    TO_DATE('2020-03-25', 'YY-MM-DD'),
    TO_DATE('14:36:41', 'HH24:MI:SS'),
    12,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    146,
    TO_DATE('2019-07-04', 'YY-MM-DD'),
    TO_DATE('22:36:40', 'HH24:MI:SS'),
    10,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    104,
    TO_DATE('2019-12-08', 'YY-MM-DD'),
    TO_DATE('15:29:12', 'HH24:MI:SS'),
    12,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    120,
    TO_DATE('2019-10-24', 'YY-MM-DD'),
    TO_DATE('07:02:59', 'HH24:MI:SS'),
    11,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    109,
    TO_DATE('2019-12-15', 'YY-MM-DD'),
    TO_DATE('00:22:18', 'HH24:MI:SS'),
    10,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    142,
    TO_DATE('2020-02-22', 'YY-MM-DD'),
    TO_DATE('16:42:44', 'HH24:MI:SS'),
    12,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    125,
    TO_DATE('2019-05-03', 'YY-MM-DD'),
    TO_DATE('16:54:16', 'HH24:MI:SS'),
    15,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    143,
    TO_DATE('2019-07-08', 'YY-MM-DD'),
    TO_DATE('15:25:26', 'HH24:MI:SS'),
    7,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    53,
    TO_DATE('2019-12-31', 'YY-MM-DD'),
    TO_DATE('10:42:53', 'HH24:MI:SS'),
    9,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    74,
    TO_DATE('2019-10-21', 'YY-MM-DD'),
    TO_DATE('07:23:12', 'HH24:MI:SS'),
    15,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    92,
    TO_DATE('2019-06-08', 'YY-MM-DD'),
    TO_DATE('04:11:08', 'HH24:MI:SS'),
    2,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    69,
    TO_DATE('2019-06-13', 'YY-MM-DD'),
    TO_DATE('00:00:49', 'HH24:MI:SS'),
    5,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    21,
    TO_DATE('2019-11-16', 'YY-MM-DD'),
    TO_DATE('12:43:09', 'HH24:MI:SS'),
    6,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    113,
    TO_DATE('2019-12-15', 'YY-MM-DD'),
    TO_DATE('10:07:54', 'HH24:MI:SS'),
    5,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    90,
    TO_DATE('2019-08-30', 'YY-MM-DD'),
    TO_DATE('21:47:02', 'HH24:MI:SS'),
    9,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    84,
    TO_DATE('2019-07-19', 'YY-MM-DD'),
    TO_DATE('04:01:32', 'HH24:MI:SS'),
    9,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    71,
    TO_DATE('2019-12-09', 'YY-MM-DD'),
    TO_DATE('01:17:40', 'HH24:MI:SS'),
    14,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    2,
    TO_DATE('2020-04-05', 'YY-MM-DD'),
    TO_DATE('12:41:53', 'HH24:MI:SS'),
    1,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    111,
    TO_DATE('2019-06-01', 'YY-MM-DD'),
    TO_DATE('23:07:10', 'HH24:MI:SS'),
    9,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    121,
    TO_DATE('2019-10-11', 'YY-MM-DD'),
    TO_DATE('22:10:21', 'HH24:MI:SS'),
    6,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    139,
    TO_DATE('2019-10-29', 'YY-MM-DD'),
    TO_DATE('12:51:22', 'HH24:MI:SS'),
    6,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    101,
    TO_DATE('2020-02-24', 'YY-MM-DD'),
    TO_DATE('02:59:47', 'HH24:MI:SS'),
    15,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    138,
    TO_DATE('2019-10-20', 'YY-MM-DD'),
    TO_DATE('07:37:05', 'HH24:MI:SS'),
    2,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    74,
    TO_DATE('2019-07-07', 'YY-MM-DD'),
    TO_DATE('03:37:55', 'HH24:MI:SS'),
    11,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    82,
    TO_DATE('2020-01-19', 'YY-MM-DD'),
    TO_DATE('23:01:30', 'HH24:MI:SS'),
    15,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    54,
    TO_DATE('2019-09-19', 'YY-MM-DD'),
    TO_DATE('19:11:16', 'HH24:MI:SS'),
    13,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    132,
    TO_DATE('2019-08-11', 'YY-MM-DD'),
    TO_DATE('18:03:04', 'HH24:MI:SS'),
    15,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    4,
    TO_DATE('2019-12-31', 'YY-MM-DD'),
    TO_DATE('09:56:31', 'HH24:MI:SS'),
    6,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    13,
    TO_DATE('2020-01-10', 'YY-MM-DD'),
    TO_DATE('18:57:55', 'HH24:MI:SS'),
    11,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    33,
    TO_DATE('2020-03-20', 'YY-MM-DD'),
    TO_DATE('17:02:08', 'HH24:MI:SS'),
    5,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    21,
    TO_DATE('2019-11-21', 'YY-MM-DD'),
    TO_DATE('12:01:08', 'HH24:MI:SS'),
    10,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    27,
    TO_DATE('2019-06-25', 'YY-MM-DD'),
    TO_DATE('10:52:09', 'HH24:MI:SS'),
    3,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    8,
    TO_DATE('2020-03-27', 'YY-MM-DD'),
    TO_DATE('23:31:56', 'HH24:MI:SS'),
    7,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    75,
    TO_DATE('2020-04-24', 'YY-MM-DD'),
    TO_DATE('18:48:41', 'HH24:MI:SS'),
    2,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    146,
    TO_DATE('2019-08-28', 'YY-MM-DD'),
    TO_DATE('19:50:08', 'HH24:MI:SS'),
    1,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    87,
    TO_DATE('2020-02-19', 'YY-MM-DD'),
    TO_DATE('22:55:05', 'HH24:MI:SS'),
    6,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    10,
    TO_DATE('2019-12-18', 'YY-MM-DD'),
    TO_DATE('19:04:27', 'HH24:MI:SS'),
    2,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    29,
    TO_DATE('2020-02-06', 'YY-MM-DD'),
    TO_DATE('23:55:30', 'HH24:MI:SS'),
    3,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    120,
    TO_DATE('2019-11-17', 'YY-MM-DD'),
    TO_DATE('14:23:56', 'HH24:MI:SS'),
    4,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    124,
    TO_DATE('2020-01-13', 'YY-MM-DD'),
    TO_DATE('15:30:50', 'HH24:MI:SS'),
    4,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    44,
    TO_DATE('2020-01-17', 'YY-MM-DD'),
    TO_DATE('01:43:37', 'HH24:MI:SS'),
    6,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    71,
    TO_DATE('2020-03-28', 'YY-MM-DD'),
    TO_DATE('16:13:38', 'HH24:MI:SS'),
    4,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    137,
    TO_DATE('2020-04-04', 'YY-MM-DD'),
    TO_DATE('13:48:21', 'HH24:MI:SS'),
    3,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    65,
    TO_DATE('2019-06-01', 'YY-MM-DD'),
    TO_DATE('19:36:48', 'HH24:MI:SS'),
    2,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    104,
    TO_DATE('2019-10-22', 'YY-MM-DD'),
    TO_DATE('06:19:18', 'HH24:MI:SS'),
    9,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    131,
    TO_DATE('2019-08-30', 'YY-MM-DD'),
    TO_DATE('03:47:27', 'HH24:MI:SS'),
    3,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    114,
    TO_DATE('2020-01-15', 'YY-MM-DD'),
    TO_DATE('21:22:13', 'HH24:MI:SS'),
    11,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    36,
    TO_DATE('2020-03-28', 'YY-MM-DD'),
    TO_DATE('04:48:11', 'HH24:MI:SS'),
    7,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    71,
    TO_DATE('2019-11-12', 'YY-MM-DD'),
    TO_DATE('14:38:31', 'HH24:MI:SS'),
    11,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    103,
    TO_DATE('2020-04-07', 'YY-MM-DD'),
    TO_DATE('12:47:25', 'HH24:MI:SS'),
    5,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    10,
    TO_DATE('2019-11-08', 'YY-MM-DD'),
    TO_DATE('14:31:56', 'HH24:MI:SS'),
    8,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    6,
    TO_DATE('2020-03-29', 'YY-MM-DD'),
    TO_DATE('00:46:41', 'HH24:MI:SS'),
    3,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    39,
    TO_DATE('2019-10-24', 'YY-MM-DD'),
    TO_DATE('03:51:27', 'HH24:MI:SS'),
    13,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    64,
    TO_DATE('2019-10-09', 'YY-MM-DD'),
    TO_DATE('17:52:12', 'HH24:MI:SS'),
    5,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    83,
    TO_DATE('2019-10-22', 'YY-MM-DD'),
    TO_DATE('08:41:50', 'HH24:MI:SS'),
    11,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    65,
    TO_DATE('2020-01-31', 'YY-MM-DD'),
    TO_DATE('19:13:48', 'HH24:MI:SS'),
    1,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    135,
    TO_DATE('2020-03-21', 'YY-MM-DD'),
    TO_DATE('19:28:21', 'HH24:MI:SS'),
    4,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    139,
    TO_DATE('2019-07-19', 'YY-MM-DD'),
    TO_DATE('01:34:54', 'HH24:MI:SS'),
    12,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    106,
    TO_DATE('2019-06-03', 'YY-MM-DD'),
    TO_DATE('13:16:24', 'HH24:MI:SS'),
    2,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    48,
    TO_DATE('2019-11-28', 'YY-MM-DD'),
    TO_DATE('16:57:40', 'HH24:MI:SS'),
    11,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    134,
    TO_DATE('2019-07-20', 'YY-MM-DD'),
    TO_DATE('00:48:05', 'HH24:MI:SS'),
    9,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    77,
    TO_DATE('2020-04-07', 'YY-MM-DD'),
    TO_DATE('05:07:59', 'HH24:MI:SS'),
    2,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    49,
    TO_DATE('2019-09-28', 'YY-MM-DD'),
    TO_DATE('22:31:35', 'HH24:MI:SS'),
    13,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    25,
    TO_DATE('2019-05-21', 'YY-MM-DD'),
    TO_DATE('22:55:13', 'HH24:MI:SS'),
    7,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    47,
    TO_DATE('2020-01-23', 'YY-MM-DD'),
    TO_DATE('11:50:09', 'HH24:MI:SS'),
    9,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    129,
    TO_DATE('2020-04-13', 'YY-MM-DD'),
    TO_DATE('00:39:41', 'HH24:MI:SS'),
    2,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    113,
    TO_DATE('2019-06-07', 'YY-MM-DD'),
    TO_DATE('16:44:26', 'HH24:MI:SS'),
    7,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    92,
    TO_DATE('2019-07-17', 'YY-MM-DD'),
    TO_DATE('03:05:41', 'HH24:MI:SS'),
    3,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    40,
    TO_DATE('2019-12-19', 'YY-MM-DD'),
    TO_DATE('16:02:32', 'HH24:MI:SS'),
    14,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    115,
    TO_DATE('2019-10-16', 'YY-MM-DD'),
    TO_DATE('11:16:24', 'HH24:MI:SS'),
    5,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    103,
    TO_DATE('2020-03-01', 'YY-MM-DD'),
    TO_DATE('16:09:24', 'HH24:MI:SS'),
    4,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    110,
    TO_DATE('2019-07-06', 'YY-MM-DD'),
    TO_DATE('12:16:43', 'HH24:MI:SS'),
    2,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    102,
    TO_DATE('2019-04-28', 'YY-MM-DD'),
    TO_DATE('20:56:04', 'HH24:MI:SS'),
    8,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    47,
    TO_DATE('2019-12-26', 'YY-MM-DD'),
    TO_DATE('19:40:17', 'HH24:MI:SS'),
    4,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    43,
    TO_DATE('2019-06-29', 'YY-MM-DD'),
    TO_DATE('14:36:40', 'HH24:MI:SS'),
    5,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    4,
    TO_DATE('2019-08-13', 'YY-MM-DD'),
    TO_DATE('06:12:41', 'HH24:MI:SS'),
    14,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    58,
    TO_DATE('2019-12-12', 'YY-MM-DD'),
    TO_DATE('12:11:25', 'HH24:MI:SS'),
    10,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    68,
    TO_DATE('2020-03-05', 'YY-MM-DD'),
    TO_DATE('03:11:54', 'HH24:MI:SS'),
    4,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    127,
    TO_DATE('2020-03-10', 'YY-MM-DD'),
    TO_DATE('11:44:45', 'HH24:MI:SS'),
    12,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    95,
    TO_DATE('2020-02-19', 'YY-MM-DD'),
    TO_DATE('02:54:29', 'HH24:MI:SS'),
    11,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    17,
    TO_DATE('2019-12-02', 'YY-MM-DD'),
    TO_DATE('15:14:56', 'HH24:MI:SS'),
    1,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    95,
    TO_DATE('2019-09-12', 'YY-MM-DD'),
    TO_DATE('10:43:35', 'HH24:MI:SS'),
    4,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    132,
    TO_DATE('2020-01-30', 'YY-MM-DD'),
    TO_DATE('22:17:35', 'HH24:MI:SS'),
    9,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    86,
    TO_DATE('2019-05-16', 'YY-MM-DD'),
    TO_DATE('17:10:44', 'HH24:MI:SS'),
    14,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    119,
    TO_DATE('2019-09-09', 'YY-MM-DD'),
    TO_DATE('10:48:20', 'HH24:MI:SS'),
    9,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    80,
    TO_DATE('2019-07-01', 'YY-MM-DD'),
    TO_DATE('22:13:29', 'HH24:MI:SS'),
    15,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    104,
    TO_DATE('2019-09-30', 'YY-MM-DD'),
    TO_DATE('08:21:26', 'HH24:MI:SS'),
    8,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    118,
    TO_DATE('2020-03-13', 'YY-MM-DD'),
    TO_DATE('01:25:23', 'HH24:MI:SS'),
    10,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    30,
    TO_DATE('2019-05-08', 'YY-MM-DD'),
    TO_DATE('20:52:45', 'HH24:MI:SS'),
    8,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    110,
    TO_DATE('2019-08-22', 'YY-MM-DD'),
    TO_DATE('05:42:09', 'HH24:MI:SS'),
    2,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    134,
    TO_DATE('2019-07-17', 'YY-MM-DD'),
    TO_DATE('15:21:50', 'HH24:MI:SS'),
    2,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    137,
    TO_DATE('2019-05-21', 'YY-MM-DD'),
    TO_DATE('08:37:28', 'HH24:MI:SS'),
    11,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    97,
    TO_DATE('2020-02-14', 'YY-MM-DD'),
    TO_DATE('13:31:37', 'HH24:MI:SS'),
    7,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    70,
    TO_DATE('2020-04-23', 'YY-MM-DD'),
    TO_DATE('22:38:22', 'HH24:MI:SS'),
    8,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    133,
    TO_DATE('2019-08-01', 'YY-MM-DD'),
    TO_DATE('23:04:39', 'HH24:MI:SS'),
    13,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    53,
    TO_DATE('2020-03-06', 'YY-MM-DD'),
    TO_DATE('16:06:30', 'HH24:MI:SS'),
    1,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    57,
    TO_DATE('2020-02-08', 'YY-MM-DD'),
    TO_DATE('23:54:25', 'HH24:MI:SS'),
    4,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    2,
    TO_DATE('2019-08-01', 'YY-MM-DD'),
    TO_DATE('11:56:28', 'HH24:MI:SS'),
    13,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    116,
    TO_DATE('2019-06-30', 'YY-MM-DD'),
    TO_DATE('12:36:35', 'HH24:MI:SS'),
    14,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    135,
    TO_DATE('2019-07-07', 'YY-MM-DD'),
    TO_DATE('21:03:12', 'HH24:MI:SS'),
    9,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    104,
    TO_DATE('2019-10-11', 'YY-MM-DD'),
    TO_DATE('16:37:33', 'HH24:MI:SS'),
    10,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    1,
    TO_DATE('2019-08-18', 'YY-MM-DD'),
    TO_DATE('21:07:24', 'HH24:MI:SS'),
    8,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    127,
    TO_DATE('2019-07-13', 'YY-MM-DD'),
    TO_DATE('21:44:10', 'HH24:MI:SS'),
    15,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    20,
    TO_DATE('2019-08-10', 'YY-MM-DD'),
    TO_DATE('07:43:13', 'HH24:MI:SS'),
    1,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    96,
    TO_DATE('2019-10-11', 'YY-MM-DD'),
    TO_DATE('14:24:30', 'HH24:MI:SS'),
    13,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    92,
    TO_DATE('2020-02-17', 'YY-MM-DD'),
    TO_DATE('07:19:07', 'HH24:MI:SS'),
    13,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    119,
    TO_DATE('2019-12-21', 'YY-MM-DD'),
    TO_DATE('03:55:29', 'HH24:MI:SS'),
    12,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    117,
    TO_DATE('2019-06-14', 'YY-MM-DD'),
    TO_DATE('03:40:50', 'HH24:MI:SS'),
    10,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    37,
    TO_DATE('2019-06-13', 'YY-MM-DD'),
    TO_DATE('09:06:37', 'HH24:MI:SS'),
    3,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    96,
    TO_DATE('2019-10-14', 'YY-MM-DD'),
    TO_DATE('01:49:27', 'HH24:MI:SS'),
    11,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    18,
    TO_DATE('2020-03-05', 'YY-MM-DD'),
    TO_DATE('00:29:08', 'HH24:MI:SS'),
    10,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    85,
    TO_DATE('2019-06-04', 'YY-MM-DD'),
    TO_DATE('14:43:12', 'HH24:MI:SS'),
    9,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    104,
    TO_DATE('2019-06-01', 'YY-MM-DD'),
    TO_DATE('02:52:37', 'HH24:MI:SS'),
    3,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    109,
    TO_DATE('2019-12-08', 'YY-MM-DD'),
    TO_DATE('11:25:21', 'HH24:MI:SS'),
    3,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    41,
    TO_DATE('2019-10-10', 'YY-MM-DD'),
    TO_DATE('01:07:01', 'HH24:MI:SS'),
    11,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    96,
    TO_DATE('2019-12-11', 'YY-MM-DD'),
    TO_DATE('20:10:21', 'HH24:MI:SS'),
    7,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    118,
    TO_DATE('2020-01-28', 'YY-MM-DD'),
    TO_DATE('15:20:59', 'HH24:MI:SS'),
    8,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    115,
    TO_DATE('2019-05-10', 'YY-MM-DD'),
    TO_DATE('19:35:49', 'HH24:MI:SS'),
    5,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    147,
    TO_DATE('2019-05-20', 'YY-MM-DD'),
    TO_DATE('03:43:45', 'HH24:MI:SS'),
    12,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    24,
    TO_DATE('2020-02-02', 'YY-MM-DD'),
    TO_DATE('17:11:27', 'HH24:MI:SS'),
    10,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    94,
    TO_DATE('2019-10-31', 'YY-MM-DD'),
    TO_DATE('20:12:55', 'HH24:MI:SS'),
    7,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    65,
    TO_DATE('2019-05-23', 'YY-MM-DD'),
    TO_DATE('22:04:44', 'HH24:MI:SS'),
    4,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    130,
    TO_DATE('2020-03-29', 'YY-MM-DD'),
    TO_DATE('18:44:40', 'HH24:MI:SS'),
    5,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    109,
    TO_DATE('2020-03-07', 'YY-MM-DD'),
    TO_DATE('11:53:42', 'HH24:MI:SS'),
    11,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    11,
    TO_DATE('2019-05-04', 'YY-MM-DD'),
    TO_DATE('03:26:12', 'HH24:MI:SS'),
    4,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    126,
    TO_DATE('2019-08-17', 'YY-MM-DD'),
    TO_DATE('21:45:06', 'HH24:MI:SS'),
    2,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    149,
    TO_DATE('2020-01-12', 'YY-MM-DD'),
    TO_DATE('19:10:29', 'HH24:MI:SS'),
    7,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    144,
    TO_DATE('2020-01-07', 'YY-MM-DD'),
    TO_DATE('02:38:02', 'HH24:MI:SS'),
    6,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    14,
    TO_DATE('2020-02-17', 'YY-MM-DD'),
    TO_DATE('03:06:51', 'HH24:MI:SS'),
    6,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    66,
    TO_DATE('2020-02-10', 'YY-MM-DD'),
    TO_DATE('13:27:16', 'HH24:MI:SS'),
    15,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    135,
    TO_DATE('2019-06-20', 'YY-MM-DD'),
    TO_DATE('15:43:28', 'HH24:MI:SS'),
    7,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    86,
    TO_DATE('2019-10-26', 'YY-MM-DD'),
    TO_DATE('15:00:02', 'HH24:MI:SS'),
    7,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    14,
    TO_DATE('2019-12-08', 'YY-MM-DD'),
    TO_DATE('09:10:05', 'HH24:MI:SS'),
    7,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    98,
    TO_DATE('2019-06-14', 'YY-MM-DD'),
    TO_DATE('02:22:38', 'HH24:MI:SS'),
    8,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    94,
    TO_DATE('2020-01-08', 'YY-MM-DD'),
    TO_DATE('15:58:51', 'HH24:MI:SS'),
    2,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    71,
    TO_DATE('2020-01-06', 'YY-MM-DD'),
    TO_DATE('04:27:35', 'HH24:MI:SS'),
    12,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    72,
    TO_DATE('2019-06-19', 'YY-MM-DD'),
    TO_DATE('07:25:46', 'HH24:MI:SS'),
    12,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    19,
    TO_DATE('2020-01-30', 'YY-MM-DD'),
    TO_DATE('17:33:01', 'HH24:MI:SS'),
    12,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    124,
    TO_DATE('2019-10-04', 'YY-MM-DD'),
    TO_DATE('09:33:47', 'HH24:MI:SS'),
    11,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    2,
    TO_DATE('2020-04-09', 'YY-MM-DD'),
    TO_DATE('04:25:42', 'HH24:MI:SS'),
    1,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    42,
    TO_DATE('2020-04-16', 'YY-MM-DD'),
    TO_DATE('12:31:44', 'HH24:MI:SS'),
    9,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    127,
    TO_DATE('2020-01-23', 'YY-MM-DD'),
    TO_DATE('20:29:55', 'HH24:MI:SS'),
    1,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    46,
    TO_DATE('2019-07-19', 'YY-MM-DD'),
    TO_DATE('15:25:10', 'HH24:MI:SS'),
    2,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    115,
    TO_DATE('2019-06-10', 'YY-MM-DD'),
    TO_DATE('22:52:56', 'HH24:MI:SS'),
    6,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    96,
    TO_DATE('2020-02-15', 'YY-MM-DD'),
    TO_DATE('18:31:14', 'HH24:MI:SS'),
    2,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    100,
    TO_DATE('2020-01-01', 'YY-MM-DD'),
    TO_DATE('17:32:30', 'HH24:MI:SS'),
    14,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    135,
    TO_DATE('2019-06-17', 'YY-MM-DD'),
    TO_DATE('11:15:43', 'HH24:MI:SS'),
    11,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    135,
    TO_DATE('2020-02-14', 'YY-MM-DD'),
    TO_DATE('08:43:37', 'HH24:MI:SS'),
    9,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    132,
    TO_DATE('2019-12-31', 'YY-MM-DD'),
    TO_DATE('17:35:12', 'HH24:MI:SS'),
    1,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    146,
    TO_DATE('2020-01-20', 'YY-MM-DD'),
    TO_DATE('12:08:09', 'HH24:MI:SS'),
    12,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    39,
    TO_DATE('2019-09-10', 'YY-MM-DD'),
    TO_DATE('23:56:02', 'HH24:MI:SS'),
    7,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    5,
    TO_DATE('2019-10-24', 'YY-MM-DD'),
    TO_DATE('10:46:16', 'HH24:MI:SS'),
    1,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    127,
    TO_DATE('2019-05-06', 'YY-MM-DD'),
    TO_DATE('10:36:19', 'HH24:MI:SS'),
    14,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    145,
    TO_DATE('2019-09-30', 'YY-MM-DD'),
    TO_DATE('17:38:40', 'HH24:MI:SS'),
    13,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    74,
    TO_DATE('2020-04-04', 'YY-MM-DD'),
    TO_DATE('23:39:39', 'HH24:MI:SS'),
    15,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    146,
    TO_DATE('2020-03-20', 'YY-MM-DD'),
    TO_DATE('16:19:21', 'HH24:MI:SS'),
    12,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    142,
    TO_DATE('2020-01-09', 'YY-MM-DD'),
    TO_DATE('02:04:07', 'HH24:MI:SS'),
    9,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    148,
    TO_DATE('2019-06-01', 'YY-MM-DD'),
    TO_DATE('04:47:05', 'HH24:MI:SS'),
    9,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    19,
    TO_DATE('2019-09-15', 'YY-MM-DD'),
    TO_DATE('21:26:52', 'HH24:MI:SS'),
    9,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    139,
    TO_DATE('2019-12-02', 'YY-MM-DD'),
    TO_DATE('09:58:23', 'HH24:MI:SS'),
    6,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    134,
    TO_DATE('2020-01-12', 'YY-MM-DD'),
    TO_DATE('04:02:29', 'HH24:MI:SS'),
    13,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    38,
    TO_DATE('2019-05-21', 'YY-MM-DD'),
    TO_DATE('13:30:15', 'HH24:MI:SS'),
    8,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    99,
    TO_DATE('2019-10-12', 'YY-MM-DD'),
    TO_DATE('08:39:12', 'HH24:MI:SS'),
    3,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    78,
    TO_DATE('2019-10-08', 'YY-MM-DD'),
    TO_DATE('04:07:36', 'HH24:MI:SS'),
    8,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    21,
    TO_DATE('2019-11-28', 'YY-MM-DD'),
    TO_DATE('18:48:16', 'HH24:MI:SS'),
    13,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    69,
    TO_DATE('2019-07-25', 'YY-MM-DD'),
    TO_DATE('17:01:52', 'HH24:MI:SS'),
    14,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    15,
    TO_DATE('2020-03-22', 'YY-MM-DD'),
    TO_DATE('09:48:55', 'HH24:MI:SS'),
    7,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    108,
    TO_DATE('2019-07-11', 'YY-MM-DD'),
    TO_DATE('08:20:34', 'HH24:MI:SS'),
    3,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    15,
    TO_DATE('2019-06-28', 'YY-MM-DD'),
    TO_DATE('20:00:19', 'HH24:MI:SS'),
    13,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    46,
    TO_DATE('2019-06-01', 'YY-MM-DD'),
    TO_DATE('14:57:58', 'HH24:MI:SS'),
    15,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    3,
    TO_DATE('2019-07-20', 'YY-MM-DD'),
    TO_DATE('11:54:55', 'HH24:MI:SS'),
    15,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    13,
    TO_DATE('2020-02-10', 'YY-MM-DD'),
    TO_DATE('02:17:29', 'HH24:MI:SS'),
    2,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    72,
    TO_DATE('2019-06-18', 'YY-MM-DD'),
    TO_DATE('12:53:23', 'HH24:MI:SS'),
    9,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    92,
    TO_DATE('2019-07-28', 'YY-MM-DD'),
    TO_DATE('14:50:28', 'HH24:MI:SS'),
    14,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    123,
    TO_DATE('2019-12-06', 'YY-MM-DD'),
    TO_DATE('04:44:12', 'HH24:MI:SS'),
    11,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    30,
    TO_DATE('2020-02-03', 'YY-MM-DD'),
    TO_DATE('02:48:50', 'HH24:MI:SS'),
    11,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    81,
    TO_DATE('2020-01-09', 'YY-MM-DD'),
    TO_DATE('02:30:22', 'HH24:MI:SS'),
    4,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    49,
    TO_DATE('2019-07-14', 'YY-MM-DD'),
    TO_DATE('05:22:50', 'HH24:MI:SS'),
    4,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    65,
    TO_DATE('2019-07-14', 'YY-MM-DD'),
    TO_DATE('20:15:20', 'HH24:MI:SS'),
    5,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    28,
    TO_DATE('2020-02-27', 'YY-MM-DD'),
    TO_DATE('01:43:46', 'HH24:MI:SS'),
    4,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    131,
    TO_DATE('2019-07-06', 'YY-MM-DD'),
    TO_DATE('18:52:01', 'HH24:MI:SS'),
    14,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    29,
    TO_DATE('2020-02-08', 'YY-MM-DD'),
    TO_DATE('07:16:20', 'HH24:MI:SS'),
    14,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    38,
    TO_DATE('2019-09-19', 'YY-MM-DD'),
    TO_DATE('08:29:02', 'HH24:MI:SS'),
    2,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    100,
    TO_DATE('2019-09-10', 'YY-MM-DD'),
    TO_DATE('03:57:26', 'HH24:MI:SS'),
    8,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    29,
    TO_DATE('2019-05-04', 'YY-MM-DD'),
    TO_DATE('18:23:59', 'HH24:MI:SS'),
    14,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    133,
    TO_DATE('2019-04-28', 'YY-MM-DD'),
    TO_DATE('13:30:45', 'HH24:MI:SS'),
    12,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    141,
    TO_DATE('2019-07-05', 'YY-MM-DD'),
    TO_DATE('19:42:55', 'HH24:MI:SS'),
    13,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    62,
    TO_DATE('2019-12-11', 'YY-MM-DD'),
    TO_DATE('04:52:24', 'HH24:MI:SS'),
    9,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    116,
    TO_DATE('2019-04-30', 'YY-MM-DD'),
    TO_DATE('19:17:42', 'HH24:MI:SS'),
    12,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    4,
    TO_DATE('2019-12-03', 'YY-MM-DD'),
    TO_DATE('11:13:17', 'HH24:MI:SS'),
    10,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    122,
    TO_DATE('2019-11-14', 'YY-MM-DD'),
    TO_DATE('02:27:38', 'HH24:MI:SS'),
    12,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    120,
    TO_DATE('2020-01-27', 'YY-MM-DD'),
    TO_DATE('21:16:44', 'HH24:MI:SS'),
    9,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    64,
    TO_DATE('2020-02-01', 'YY-MM-DD'),
    TO_DATE('07:54:55', 'HH24:MI:SS'),
    14,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    148,
    TO_DATE('2019-09-14', 'YY-MM-DD'),
    TO_DATE('09:52:44', 'HH24:MI:SS'),
    5,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    78,
    TO_DATE('2019-08-03', 'YY-MM-DD'),
    TO_DATE('04:31:58', 'HH24:MI:SS'),
    12,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    39,
    TO_DATE('2019-10-11', 'YY-MM-DD'),
    TO_DATE('01:25:48', 'HH24:MI:SS'),
    8,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    9,
    TO_DATE('2020-04-20', 'YY-MM-DD'),
    TO_DATE('18:43:41', 'HH24:MI:SS'),
    8,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    140,
    TO_DATE('2019-10-12', 'YY-MM-DD'),
    TO_DATE('10:56:33', 'HH24:MI:SS'),
    10,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    113,
    TO_DATE('2019-06-25', 'YY-MM-DD'),
    TO_DATE('00:05:54', 'HH24:MI:SS'),
    3,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    53,
    TO_DATE('2020-01-10', 'YY-MM-DD'),
    TO_DATE('07:53:06', 'HH24:MI:SS'),
    11,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    130,
    TO_DATE('2019-10-09', 'YY-MM-DD'),
    TO_DATE('15:42:00', 'HH24:MI:SS'),
    6,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    25,
    TO_DATE('2019-07-23', 'YY-MM-DD'),
    TO_DATE('08:33:56', 'HH24:MI:SS'),
    9,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    99,
    TO_DATE('2019-05-26', 'YY-MM-DD'),
    TO_DATE('00:14:57', 'HH24:MI:SS'),
    12,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    22,
    TO_DATE('2020-03-21', 'YY-MM-DD'),
    TO_DATE('13:56:55', 'HH24:MI:SS'),
    14,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    142,
    TO_DATE('2019-07-21', 'YY-MM-DD'),
    TO_DATE('20:59:59', 'HH24:MI:SS'),
    10,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    83,
    TO_DATE('2019-05-13', 'YY-MM-DD'),
    TO_DATE('19:54:20', 'HH24:MI:SS'),
    15,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    146,
    TO_DATE('2019-11-02', 'YY-MM-DD'),
    TO_DATE('04:15:32', 'HH24:MI:SS'),
    5,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    138,
    TO_DATE('2020-03-02', 'YY-MM-DD'),
    TO_DATE('01:42:59', 'HH24:MI:SS'),
    4,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    55,
    TO_DATE('2019-12-27', 'YY-MM-DD'),
    TO_DATE('13:41:18', 'HH24:MI:SS'),
    5,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    15,
    TO_DATE('2019-09-24', 'YY-MM-DD'),
    TO_DATE('11:17:04', 'HH24:MI:SS'),
    10,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    131,
    TO_DATE('2019-08-28', 'YY-MM-DD'),
    TO_DATE('22:33:15', 'HH24:MI:SS'),
    8,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    17,
    TO_DATE('2020-02-26', 'YY-MM-DD'),
    TO_DATE('09:23:48', 'HH24:MI:SS'),
    11,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    109,
    TO_DATE('2020-04-16', 'YY-MM-DD'),
    TO_DATE('03:50:24', 'HH24:MI:SS'),
    5,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    59,
    TO_DATE('2019-04-29', 'YY-MM-DD'),
    TO_DATE('00:33:45', 'HH24:MI:SS'),
    3,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    138,
    TO_DATE('2019-05-26', 'YY-MM-DD'),
    TO_DATE('19:49:58', 'HH24:MI:SS'),
    8,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    53,
    TO_DATE('2020-04-01', 'YY-MM-DD'),
    TO_DATE('16:46:29', 'HH24:MI:SS'),
    1,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    74,
    TO_DATE('2019-07-24', 'YY-MM-DD'),
    TO_DATE('00:25:29', 'HH24:MI:SS'),
    2,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    97,
    TO_DATE('2019-11-20', 'YY-MM-DD'),
    TO_DATE('03:04:32', 'HH24:MI:SS'),
    11,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    83,
    TO_DATE('2020-01-19', 'YY-MM-DD'),
    TO_DATE('04:07:31', 'HH24:MI:SS'),
    15,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    69,
    TO_DATE('2020-04-23', 'YY-MM-DD'),
    TO_DATE('08:17:43', 'HH24:MI:SS'),
    13,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    49,
    TO_DATE('2020-01-05', 'YY-MM-DD'),
    TO_DATE('03:56:24', 'HH24:MI:SS'),
    6,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    64,
    TO_DATE('2020-01-22', 'YY-MM-DD'),
    TO_DATE('07:15:13', 'HH24:MI:SS'),
    6,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    78,
    TO_DATE('2020-04-12', 'YY-MM-DD'),
    TO_DATE('10:10:16', 'HH24:MI:SS'),
    6,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    106,
    TO_DATE('2019-09-10', 'YY-MM-DD'),
    TO_DATE('02:38:10', 'HH24:MI:SS'),
    14,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    1,
    TO_DATE('2019-10-30', 'YY-MM-DD'),
    TO_DATE('19:39:22', 'HH24:MI:SS'),
    13,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    96,
    TO_DATE('2020-01-06', 'YY-MM-DD'),
    TO_DATE('16:08:45', 'HH24:MI:SS'),
    3,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    107,
    TO_DATE('2020-03-03', 'YY-MM-DD'),
    TO_DATE('12:17:38', 'HH24:MI:SS'),
    8,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    126,
    TO_DATE('2020-03-28', 'YY-MM-DD'),
    TO_DATE('16:39:02', 'HH24:MI:SS'),
    4,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    145,
    TO_DATE('2020-01-30', 'YY-MM-DD'),
    TO_DATE('03:55:12', 'HH24:MI:SS'),
    5,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    139,
    TO_DATE('2019-05-27', 'YY-MM-DD'),
    TO_DATE('12:54:28', 'HH24:MI:SS'),
    5,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    76,
    TO_DATE('2019-08-23', 'YY-MM-DD'),
    TO_DATE('21:24:23', 'HH24:MI:SS'),
    1,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    43,
    TO_DATE('2019-10-21', 'YY-MM-DD'),
    TO_DATE('15:43:52', 'HH24:MI:SS'),
    13,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    127,
    TO_DATE('2020-03-21', 'YY-MM-DD'),
    TO_DATE('04:08:51', 'HH24:MI:SS'),
    1,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    136,
    TO_DATE('2019-07-23', 'YY-MM-DD'),
    TO_DATE('12:25:21', 'HH24:MI:SS'),
    7,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    112,
    TO_DATE('2019-09-12', 'YY-MM-DD'),
    TO_DATE('00:07:48', 'HH24:MI:SS'),
    1,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    112,
    TO_DATE('2019-05-10', 'YY-MM-DD'),
    TO_DATE('16:26:18', 'HH24:MI:SS'),
    9,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    68,
    TO_DATE('2019-12-20', 'YY-MM-DD'),
    TO_DATE('16:28:59', 'HH24:MI:SS'),
    11,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    20,
    TO_DATE('2020-01-23', 'YY-MM-DD'),
    TO_DATE('09:11:32', 'HH24:MI:SS'),
    3,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    44,
    TO_DATE('2019-07-03', 'YY-MM-DD'),
    TO_DATE('02:57:57', 'HH24:MI:SS'),
    4,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    134,
    TO_DATE('2020-01-15', 'YY-MM-DD'),
    TO_DATE('18:16:32', 'HH24:MI:SS'),
    4,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    131,
    TO_DATE('2019-09-18', 'YY-MM-DD'),
    TO_DATE('22:39:44', 'HH24:MI:SS'),
    13,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    95,
    TO_DATE('2019-12-22', 'YY-MM-DD'),
    TO_DATE('11:27:54', 'HH24:MI:SS'),
    4,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    71,
    TO_DATE('2019-12-25', 'YY-MM-DD'),
    TO_DATE('00:44:37', 'HH24:MI:SS'),
    11,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    131,
    TO_DATE('2019-07-08', 'YY-MM-DD'),
    TO_DATE('12:02:03', 'HH24:MI:SS'),
    12,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    4,
    TO_DATE('2020-03-21', 'YY-MM-DD'),
    TO_DATE('19:01:26', 'HH24:MI:SS'),
    10,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    80,
    TO_DATE('2020-03-28', 'YY-MM-DD'),
    TO_DATE('07:23:15', 'HH24:MI:SS'),
    10,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    89,
    TO_DATE('2020-03-27', 'YY-MM-DD'),
    TO_DATE('11:48:55', 'HH24:MI:SS'),
    14,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    63,
    TO_DATE('2020-02-01', 'YY-MM-DD'),
    TO_DATE('16:28:38', 'HH24:MI:SS'),
    12,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    118,
    TO_DATE('2019-09-30', 'YY-MM-DD'),
    TO_DATE('11:36:39', 'HH24:MI:SS'),
    13,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    76,
    TO_DATE('2020-02-09', 'YY-MM-DD'),
    TO_DATE('03:58:48', 'HH24:MI:SS'),
    4,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    18,
    TO_DATE('2019-12-10', 'YY-MM-DD'),
    TO_DATE('11:44:33', 'HH24:MI:SS'),
    15,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    67,
    TO_DATE('2019-11-12', 'YY-MM-DD'),
    TO_DATE('10:07:13', 'HH24:MI:SS'),
    7,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    143,
    TO_DATE('2019-09-15', 'YY-MM-DD'),
    TO_DATE('01:31:33', 'HH24:MI:SS'),
    13,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    90,
    TO_DATE('2019-08-09', 'YY-MM-DD'),
    TO_DATE('12:45:01', 'HH24:MI:SS'),
    11,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    112,
    TO_DATE('2020-03-21', 'YY-MM-DD'),
    TO_DATE('05:39:03', 'HH24:MI:SS'),
    4,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    108,
    TO_DATE('2019-12-25', 'YY-MM-DD'),
    TO_DATE('03:47:19', 'HH24:MI:SS'),
    6,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    79,
    TO_DATE('2019-08-13', 'YY-MM-DD'),
    TO_DATE('13:12:21', 'HH24:MI:SS'),
    13,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    22,
    TO_DATE('2020-01-09', 'YY-MM-DD'),
    TO_DATE('15:51:08', 'HH24:MI:SS'),
    5,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    145,
    TO_DATE('2020-02-01', 'YY-MM-DD'),
    TO_DATE('03:05:10', 'HH24:MI:SS'),
    14,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    142,
    TO_DATE('2019-07-21', 'YY-MM-DD'),
    TO_DATE('14:49:01', 'HH24:MI:SS'),
    3,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    10,
    TO_DATE('2019-07-28', 'YY-MM-DD'),
    TO_DATE('14:07:36', 'HH24:MI:SS'),
    6,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    5,
    TO_DATE('2019-07-11', 'YY-MM-DD'),
    TO_DATE('13:35:45', 'HH24:MI:SS'),
    5,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    41,
    TO_DATE('2019-08-18', 'YY-MM-DD'),
    TO_DATE('00:06:01', 'HH24:MI:SS'),
    4,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    128,
    TO_DATE('2020-02-20', 'YY-MM-DD'),
    TO_DATE('23:31:02', 'HH24:MI:SS'),
    14,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    132,
    TO_DATE('2020-01-08', 'YY-MM-DD'),
    TO_DATE('07:09:54', 'HH24:MI:SS'),
    12,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    115,
    TO_DATE('2019-10-20', 'YY-MM-DD'),
    TO_DATE('08:46:27', 'HH24:MI:SS'),
    10,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    145,
    TO_DATE('2019-09-30', 'YY-MM-DD'),
    TO_DATE('04:24:13', 'HH24:MI:SS'),
    1,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    28,
    TO_DATE('2020-01-31', 'YY-MM-DD'),
    TO_DATE('21:52:28', 'HH24:MI:SS'),
    6,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    20,
    TO_DATE('2020-01-03', 'YY-MM-DD'),
    TO_DATE('17:20:15', 'HH24:MI:SS'),
    5,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    5,
    TO_DATE('2019-07-04', 'YY-MM-DD'),
    TO_DATE('08:10:32', 'HH24:MI:SS'),
    13,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    61,
    TO_DATE('2020-03-19', 'YY-MM-DD'),
    TO_DATE('02:35:01', 'HH24:MI:SS'),
    9,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    73,
    TO_DATE('2019-07-16', 'YY-MM-DD'),
    TO_DATE('17:33:59', 'HH24:MI:SS'),
    6,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    147,
    TO_DATE('2019-10-30', 'YY-MM-DD'),
    TO_DATE('14:51:50', 'HH24:MI:SS'),
    10,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    20,
    TO_DATE('2019-08-08', 'YY-MM-DD'),
    TO_DATE('11:10:38', 'HH24:MI:SS'),
    2,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    102,
    TO_DATE('2019-05-02', 'YY-MM-DD'),
    TO_DATE('17:45:58', 'HH24:MI:SS'),
    15,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    83,
    TO_DATE('2020-04-17', 'YY-MM-DD'),
    TO_DATE('02:31:12', 'HH24:MI:SS'),
    3,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    32,
    TO_DATE('2020-02-20', 'YY-MM-DD'),
    TO_DATE('14:57:39', 'HH24:MI:SS'),
    3,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    120,
    TO_DATE('2019-08-08', 'YY-MM-DD'),
    TO_DATE('20:16:06', 'HH24:MI:SS'),
    14,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    15,
    TO_DATE('2020-01-22', 'YY-MM-DD'),
    TO_DATE('20:00:50', 'HH24:MI:SS'),
    6,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    109,
    TO_DATE('2019-07-09', 'YY-MM-DD'),
    TO_DATE('11:31:37', 'HH24:MI:SS'),
    4,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    53,
    TO_DATE('2019-10-09', 'YY-MM-DD'),
    TO_DATE('09:13:55', 'HH24:MI:SS'),
    2,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    135,
    TO_DATE('2019-10-24', 'YY-MM-DD'),
    TO_DATE('10:35:01', 'HH24:MI:SS'),
    15,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    122,
    TO_DATE('2020-01-01', 'YY-MM-DD'),
    TO_DATE('07:25:58', 'HH24:MI:SS'),
    6,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    107,
    TO_DATE('2019-07-13', 'YY-MM-DD'),
    TO_DATE('16:49:06', 'HH24:MI:SS'),
    6,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    104,
    TO_DATE('2020-03-10', 'YY-MM-DD'),
    TO_DATE('05:02:45', 'HH24:MI:SS'),
    15,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    128,
    TO_DATE('2020-02-29', 'YY-MM-DD'),
    TO_DATE('23:25:31', 'HH24:MI:SS'),
    11,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    95,
    TO_DATE('2019-11-26', 'YY-MM-DD'),
    TO_DATE('15:32:04', 'HH24:MI:SS'),
    1,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    105,
    TO_DATE('2020-03-08', 'YY-MM-DD'),
    TO_DATE('16:24:26', 'HH24:MI:SS'),
    4,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    27,
    TO_DATE('2019-12-25', 'YY-MM-DD'),
    TO_DATE('09:09:06', 'HH24:MI:SS'),
    5,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    107,
    TO_DATE('2019-06-22', 'YY-MM-DD'),
    TO_DATE('15:53:15', 'HH24:MI:SS'),
    6,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    63,
    TO_DATE('2019-11-26', 'YY-MM-DD'),
    TO_DATE('11:42:50', 'HH24:MI:SS'),
    5,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    58,
    TO_DATE('2020-04-06', 'YY-MM-DD'),
    TO_DATE('09:47:08', 'HH24:MI:SS'),
    12,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    11,
    TO_DATE('2019-06-23', 'YY-MM-DD'),
    TO_DATE('23:22:45', 'HH24:MI:SS'),
    9,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    59,
    TO_DATE('2019-09-21', 'YY-MM-DD'),
    TO_DATE('01:26:18', 'HH24:MI:SS'),
    1,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    130,
    TO_DATE('2020-01-11', 'YY-MM-DD'),
    TO_DATE('17:57:59', 'HH24:MI:SS'),
    8,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    47,
    TO_DATE('2019-04-28', 'YY-MM-DD'),
    TO_DATE('20:35:04', 'HH24:MI:SS'),
    14,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    136,
    TO_DATE('2019-11-25', 'YY-MM-DD'),
    TO_DATE('11:10:29', 'HH24:MI:SS'),
    6,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    90,
    TO_DATE('2019-08-31', 'YY-MM-DD'),
    TO_DATE('13:49:06', 'HH24:MI:SS'),
    9,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    130,
    TO_DATE('2020-04-23', 'YY-MM-DD'),
    TO_DATE('11:53:35', 'HH24:MI:SS'),
    14,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    106,
    TO_DATE('2019-09-03', 'YY-MM-DD'),
    TO_DATE('08:16:30', 'HH24:MI:SS'),
    11,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    103,
    TO_DATE('2020-03-25', 'YY-MM-DD'),
    TO_DATE('16:46:14', 'HH24:MI:SS'),
    11,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    72,
    TO_DATE('2019-07-09', 'YY-MM-DD'),
    TO_DATE('09:54:55', 'HH24:MI:SS'),
    6,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    3,
    TO_DATE('2019-09-23', 'YY-MM-DD'),
    TO_DATE('04:36:09', 'HH24:MI:SS'),
    12,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    25,
    TO_DATE('2019-07-14', 'YY-MM-DD'),
    TO_DATE('19:36:16', 'HH24:MI:SS'),
    4,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    44,
    TO_DATE('2019-08-19', 'YY-MM-DD'),
    TO_DATE('05:08:38', 'HH24:MI:SS'),
    2,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    133,
    TO_DATE('2019-06-29', 'YY-MM-DD'),
    TO_DATE('00:17:26', 'HH24:MI:SS'),
    11,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    123,
    TO_DATE('2020-04-23', 'YY-MM-DD'),
    TO_DATE('15:04:49', 'HH24:MI:SS'),
    12,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    131,
    TO_DATE('2019-08-24', 'YY-MM-DD'),
    TO_DATE('23:28:59', 'HH24:MI:SS'),
    14,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    69,
    TO_DATE('2019-07-13', 'YY-MM-DD'),
    TO_DATE('11:56:01', 'HH24:MI:SS'),
    3,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    29,
    TO_DATE('2019-12-04', 'YY-MM-DD'),
    TO_DATE('02:59:41', 'HH24:MI:SS'),
    3,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    66,
    TO_DATE('2019-09-30', 'YY-MM-DD'),
    TO_DATE('08:51:52', 'HH24:MI:SS'),
    6,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    83,
    TO_DATE('2020-02-24', 'YY-MM-DD'),
    TO_DATE('13:55:00', 'HH24:MI:SS'),
    4,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    54,
    TO_DATE('2020-02-16', 'YY-MM-DD'),
    TO_DATE('10:44:15', 'HH24:MI:SS'),
    14,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    107,
    TO_DATE('2020-04-06', 'YY-MM-DD'),
    TO_DATE('16:39:35', 'HH24:MI:SS'),
    7,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    108,
    TO_DATE('2019-12-22', 'YY-MM-DD'),
    TO_DATE('03:16:00', 'HH24:MI:SS'),
    1,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    133,
    TO_DATE('2020-02-02', 'YY-MM-DD'),
    TO_DATE('13:29:50', 'HH24:MI:SS'),
    15,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    97,
    TO_DATE('2019-08-21', 'YY-MM-DD'),
    TO_DATE('16:38:22', 'HH24:MI:SS'),
    9,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    111,
    TO_DATE('2019-08-26', 'YY-MM-DD'),
    TO_DATE('17:13:38', 'HH24:MI:SS'),
    9,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    147,
    TO_DATE('2019-10-02', 'YY-MM-DD'),
    TO_DATE('01:46:12', 'HH24:MI:SS'),
    3,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    147,
    TO_DATE('2019-07-15', 'YY-MM-DD'),
    TO_DATE('18:19:57', 'HH24:MI:SS'),
    1,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    30,
    TO_DATE('2020-04-12', 'YY-MM-DD'),
    TO_DATE('01:14:46', 'HH24:MI:SS'),
    11,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    22,
    TO_DATE('2020-04-24', 'YY-MM-DD'),
    TO_DATE('09:26:21', 'HH24:MI:SS'),
    6,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    149,
    TO_DATE('2020-02-15', 'YY-MM-DD'),
    TO_DATE('15:59:10', 'HH24:MI:SS'),
    14,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    15,
    TO_DATE('2019-11-14', 'YY-MM-DD'),
    TO_DATE('14:54:29', 'HH24:MI:SS'),
    5,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    34,
    TO_DATE('2019-12-09', 'YY-MM-DD'),
    TO_DATE('20:36:02', 'HH24:MI:SS'),
    5,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    124,
    TO_DATE('2019-07-17', 'YY-MM-DD'),
    TO_DATE('13:31:45', 'HH24:MI:SS'),
    1,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    14,
    TO_DATE('2019-08-03', 'YY-MM-DD'),
    TO_DATE('13:27:31', 'HH24:MI:SS'),
    1,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    56,
    TO_DATE('2019-06-16', 'YY-MM-DD'),
    TO_DATE('10:24:41', 'HH24:MI:SS'),
    3,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    22,
    TO_DATE('2020-01-31', 'YY-MM-DD'),
    TO_DATE('00:43:44', 'HH24:MI:SS'),
    3,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    135,
    TO_DATE('2019-05-15', 'YY-MM-DD'),
    TO_DATE('12:04:27', 'HH24:MI:SS'),
    12,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    125,
    TO_DATE('2019-09-17', 'YY-MM-DD'),
    TO_DATE('22:09:54', 'HH24:MI:SS'),
    14,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    80,
    TO_DATE('2020-01-01', 'YY-MM-DD'),
    TO_DATE('09:51:55', 'HH24:MI:SS'),
    7,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    116,
    TO_DATE('2019-12-05', 'YY-MM-DD'),
    TO_DATE('13:54:44', 'HH24:MI:SS'),
    7,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    108,
    TO_DATE('2019-12-31', 'YY-MM-DD'),
    TO_DATE('20:26:12', 'HH24:MI:SS'),
    15,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    38,
    TO_DATE('2019-05-07', 'YY-MM-DD'),
    TO_DATE('21:01:46', 'HH24:MI:SS'),
    10,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    150,
    TO_DATE('2019-10-18', 'YY-MM-DD'),
    TO_DATE('19:35:22', 'HH24:MI:SS'),
    9,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    17,
    TO_DATE('2020-03-02', 'YY-MM-DD'),
    TO_DATE('19:53:25', 'HH24:MI:SS'),
    8,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    129,
    TO_DATE('2020-02-26', 'YY-MM-DD'),
    TO_DATE('06:10:47', 'HH24:MI:SS'),
    4,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    23,
    TO_DATE('2019-11-19', 'YY-MM-DD'),
    TO_DATE('22:30:08', 'HH24:MI:SS'),
    12,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    6,
    TO_DATE('2019-06-08', 'YY-MM-DD'),
    TO_DATE('15:56:22', 'HH24:MI:SS'),
    5,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    53,
    TO_DATE('2020-02-17', 'YY-MM-DD'),
    TO_DATE('02:01:49', 'HH24:MI:SS'),
    10,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    53,
    TO_DATE('2019-07-10', 'YY-MM-DD'),
    TO_DATE('16:54:01', 'HH24:MI:SS'),
    11,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    86,
    TO_DATE('2019-11-07', 'YY-MM-DD'),
    TO_DATE('04:03:29', 'HH24:MI:SS'),
    2,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    43,
    TO_DATE('2019-09-12', 'YY-MM-DD'),
    TO_DATE('05:17:14', 'HH24:MI:SS'),
    4,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    29,
    TO_DATE('2019-12-23', 'YY-MM-DD'),
    TO_DATE('14:03:58', 'HH24:MI:SS'),
    10,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    5,
    TO_DATE('2019-06-22', 'YY-MM-DD'),
    TO_DATE('15:35:15', 'HH24:MI:SS'),
    13,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    14,
    TO_DATE('2019-12-20', 'YY-MM-DD'),
    TO_DATE('20:52:34', 'HH24:MI:SS'),
    9,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    144,
    TO_DATE('2019-11-25', 'YY-MM-DD'),
    TO_DATE('22:02:00', 'HH24:MI:SS'),
    8,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    129,
    TO_DATE('2020-01-01', 'YY-MM-DD'),
    TO_DATE('00:45:09', 'HH24:MI:SS'),
    12,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    81,
    TO_DATE('2020-04-15', 'YY-MM-DD'),
    TO_DATE('22:59:28', 'HH24:MI:SS'),
    5,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    139,
    TO_DATE('2019-05-24', 'YY-MM-DD'),
    TO_DATE('08:48:17', 'HH24:MI:SS'),
    11,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    41,
    TO_DATE('2019-07-14', 'YY-MM-DD'),
    TO_DATE('05:58:24', 'HH24:MI:SS'),
    5,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    65,
    TO_DATE('2019-07-14', 'YY-MM-DD'),
    TO_DATE('21:26:31', 'HH24:MI:SS'),
    7,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    142,
    TO_DATE('2019-07-14', 'YY-MM-DD'),
    TO_DATE('18:53:58', 'HH24:MI:SS'),
    10,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    31,
    TO_DATE('2019-11-26', 'YY-MM-DD'),
    TO_DATE('06:04:34', 'HH24:MI:SS'),
    6,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    84,
    TO_DATE('2019-08-20', 'YY-MM-DD'),
    TO_DATE('05:14:40', 'HH24:MI:SS'),
    8,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    50,
    TO_DATE('2020-02-25', 'YY-MM-DD'),
    TO_DATE('23:11:38', 'HH24:MI:SS'),
    10,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    44,
    TO_DATE('2020-04-20', 'YY-MM-DD'),
    TO_DATE('08:43:33', 'HH24:MI:SS'),
    15,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    58,
    TO_DATE('2019-10-19', 'YY-MM-DD'),
    TO_DATE('16:04:32', 'HH24:MI:SS'),
    7,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    77,
    TO_DATE('2020-02-27', 'YY-MM-DD'),
    TO_DATE('11:19:43', 'HH24:MI:SS'),
    14,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    34,
    TO_DATE('2019-06-24', 'YY-MM-DD'),
    TO_DATE('12:34:08', 'HH24:MI:SS'),
    13,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    113,
    TO_DATE('2019-09-04', 'YY-MM-DD'),
    TO_DATE('19:08:45', 'HH24:MI:SS'),
    10,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    136,
    TO_DATE('2019-12-11', 'YY-MM-DD'),
    TO_DATE('04:15:58', 'HH24:MI:SS'),
    9,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    53,
    TO_DATE('2019-10-11', 'YY-MM-DD'),
    TO_DATE('21:39:58', 'HH24:MI:SS'),
    9,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    148,
    TO_DATE('2019-09-08', 'YY-MM-DD'),
    TO_DATE('08:16:00', 'HH24:MI:SS'),
    11,
    12
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    147,
    TO_DATE('2019-09-25', 'YY-MM-DD'),
    TO_DATE('11:52:19', 'HH24:MI:SS'),
    10,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    15,
    TO_DATE('2020-02-06', 'YY-MM-DD'),
    TO_DATE('07:53:41', 'HH24:MI:SS'),
    9,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    6,
    TO_DATE('2020-01-28', 'YY-MM-DD'),
    TO_DATE('18:03:27', 'HH24:MI:SS'),
    15,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    75,
    TO_DATE('2020-03-10', 'YY-MM-DD'),
    TO_DATE('18:11:16', 'HH24:MI:SS'),
    3,
    7
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    69,
    TO_DATE('2020-04-24', 'YY-MM-DD'),
    TO_DATE('06:53:29', 'HH24:MI:SS'),
    11,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    1,
    TO_DATE('2019-07-28', 'YY-MM-DD'),
    TO_DATE('10:10:51', 'HH24:MI:SS'),
    6,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    138,
    TO_DATE('2020-02-08', 'YY-MM-DD'),
    TO_DATE('10:30:43', 'HH24:MI:SS'),
    15,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    85,
    TO_DATE('2020-03-06', 'YY-MM-DD'),
    TO_DATE('17:02:54', 'HH24:MI:SS'),
    10,
    9
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    97,
    TO_DATE('2019-08-04', 'YY-MM-DD'),
    TO_DATE('08:03:11', 'HH24:MI:SS'),
    4,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    131,
    TO_DATE('2019-09-11', 'YY-MM-DD'),
    TO_DATE('04:06:21', 'HH24:MI:SS'),
    11,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    89,
    TO_DATE('2020-03-07', 'YY-MM-DD'),
    TO_DATE('14:19:12', 'HH24:MI:SS'),
    8,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    34,
    TO_DATE('2019-06-12', 'YY-MM-DD'),
    TO_DATE('01:47:14', 'HH24:MI:SS'),
    1,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    148,
    TO_DATE('2020-02-06', 'YY-MM-DD'),
    TO_DATE('15:26:20', 'HH24:MI:SS'),
    14,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    131,
    TO_DATE('2019-11-16', 'YY-MM-DD'),
    TO_DATE('21:45:45', 'HH24:MI:SS'),
    7,
    3
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    66,
    TO_DATE('2019-06-10', 'YY-MM-DD'),
    TO_DATE('19:14:15', 'HH24:MI:SS'),
    11,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    35,
    TO_DATE('2019-10-06', 'YY-MM-DD'),
    TO_DATE('02:54:47', 'HH24:MI:SS'),
    15,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    128,
    TO_DATE('2019-06-08', 'YY-MM-DD'),
    TO_DATE('11:49:39', 'HH24:MI:SS'),
    9,
    4
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    81,
    TO_DATE('2019-10-28', 'YY-MM-DD'),
    TO_DATE('19:40:06', 'HH24:MI:SS'),
    6,
    11
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    41,
    TO_DATE('2020-02-01', 'YY-MM-DD'),
    TO_DATE('16:36:36', 'HH24:MI:SS'),
    1,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    15,
    TO_DATE('2019-07-01', 'YY-MM-DD'),
    TO_DATE('16:24:48', 'HH24:MI:SS'),
    7,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    21,
    TO_DATE('2019-08-14', 'YY-MM-DD'),
    TO_DATE('18:58:49', 'HH24:MI:SS'),
    7,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    37,
    TO_DATE('2020-01-22', 'YY-MM-DD'),
    TO_DATE('03:04:19', 'HH24:MI:SS'),
    15,
    1
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    74,
    TO_DATE('2019-08-29', 'YY-MM-DD'),
    TO_DATE('12:05:05', 'HH24:MI:SS'),
    15,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    107,
    TO_DATE('2019-05-20', 'YY-MM-DD'),
    TO_DATE('10:15:07', 'HH24:MI:SS'),
    7,
    8
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    28,
    TO_DATE('2019-09-12', 'YY-MM-DD'),
    TO_DATE('05:02:50', 'HH24:MI:SS'),
    4,
    14
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    86,
    TO_DATE('2019-11-01', 'YY-MM-DD'),
    TO_DATE('00:47:12', 'HH24:MI:SS'),
    3,
    10
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    30,
    TO_DATE('2019-12-01', 'YY-MM-DD'),
    TO_DATE('05:00:42', 'HH24:MI:SS'),
    12,
    5
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    94,
    TO_DATE('2019-08-12', 'YY-MM-DD'),
    TO_DATE('13:21:41', 'HH24:MI:SS'),
    1,
    13
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    73,
    TO_DATE('2020-01-26', 'YY-MM-DD'),
    TO_DATE('01:56:35', 'HH24:MI:SS'),
    12,
    2
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    43,
    TO_DATE('2020-04-07', 'YY-MM-DD'),
    TO_DATE('04:56:10', 'HH24:MI:SS'),
    1,
    15
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    75,
    TO_DATE('2020-04-03', 'YY-MM-DD'),
    TO_DATE('22:48:48', 'HH24:MI:SS'),
    10,
    6
  );
INSERT INTO visita_guidata_effettuata (codgr, data, ora, codtipovisita, codguida)
VALUES
  (
    20,
    TO_DATE('2019-07-21', 'YY-MM-DD'),
    TO_DATE('22:59:59', 'HH24:MI:SS'),
    1,
    3
  );