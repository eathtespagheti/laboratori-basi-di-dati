DROP TABLE IF EXISTS ragazzo CASCADE;
DROP TABLE IF EXISTS attività CASCADE;
DROP TABLE IF EXISTS campo_estivo CASCADE;
DROP TABLE IF EXISTS iscrizione_per_attività_in_campo_estivo CASCADE;
CREATE TABLE ragazzo (
  codfiscale CHAR(16) PRIMARY KEY,
  nome VARCHAR(200) NOT NULL,
  cognome VARCHAR(200) NOT NULL,
  datanascita DATE NOT NULL,
  cittàresidenza VARCHAR(200) NOT NULL
);
CREATE TABLE attività (
  codattività SMALLINT PRIMARY KEY,
  nome VARCHAR(200) NOT NULL,
  descrizione VARCHAR(500) NOT NULL,
  categoria VARCHAR(200) NOT NULL
);
CREATE TABLE campo_estivo (
  codcampo SMALLINT PRIMARY KEY,
  nomecampo VARCHAR(200) NOT NULL,
  città VARCHAR(200) NOT NULL
);
CREATE TABLE iscrizione_per_attività_in_campo_estivo (
  codfiscale CHAR(16) NOT NULL  REFERENCES ragazzo,
  codattività SMALLINT NOT NULL  REFERENCES attività,
  codcampo SMALLINT NOT NULL  REFERENCES campo_estivo,
  dataiscrizione DATE NOT NULL,
  PRIMARY KEY (
    codfiscale,
    codattività,
    codcampo,
    dataiscrizione
  )
);
/*
Data for ragazzo
*/
INSERT INTO ragazzo(codfiscale,nome,cognome,datanascita,cittàresidenza)
VALUES
  (
    'YHKWJJ38E54X808X',
    'Kimball',
    'Leebeter',
    '2004/01/02',
    'Ujung'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'NNZNUU28X12S243O',
    'Leif',
    'Algie',
    '2005/03/18',
    'Landskrona'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'LQENJZ10X37D528H',
    'Chick',
    'Blasli',
    '2005/05/05',
    'Nysa'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'CTLBRQ36G52P402I',
    'Gwenora',
    'Griffen',
    '2000/07/12',
    'Curahpacul Satu'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'SDKGZD33T27D595D',
    'Nicol',
    'MacEvilly',
    '2002/10/12',
    'Sīlat al Ḩārithīyah'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'ZINCHQ68G16Q241P',
    'Katrina',
    'Seid',
    '1999/11/17',
    'Pytalovo'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'VLSZIW89F67F410S',
    'Janaye',
    'Kondrachenko',
    '2005/03/07',
    'Bagarmossen'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'HHARED79H66J944S',
    'Aldrich',
    'Kincade',
    '1996/07/28',
    'Kayakent'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'IPMBZS65V32M583U',
    'Pancho',
    'O''Flaherty',
    '2001/04/01',
    'Bulqizë'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'TLOWRQ58C89E833N',
    'Hayden',
    'Francecione',
    '2004/04/15',
    'Madīnat ‘Īsá'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'OVBQSO71K30C901Q',
    'Celene',
    'Mylan',
    '2004/08/25',
    'Kihancha'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'LGUESE80L19B242N',
    'Carol',
    'Daldan',
    '1997/08/29',
    'Presidente Bernardes'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'ZSMVAB91P68Y600Y',
    'Rhianon',
    'Coad',
    '2005/05/18',
    'Al Muţayrifī'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'VKBHNW23H60S951F',
    'Joellyn',
    'Gething',
    '1996/04/03',
    'Dzikowiec'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'PQEKHU75L88Z322U',
    'Lorrin',
    'Barnsdall',
    '2001/02/28',
    'Sandefjord'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'CLVIKY77Q51B860C',
    'Karalynn',
    'Waight',
    '1997/06/19',
    'Vathýlakkos'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'GFMPKS27P22B473G',
    'Chico',
    'Jaggers',
    '2002/05/11',
    'Valky'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'RDGXSQ52T80U346W',
    'Hagan',
    'Breslin',
    '1996/07/10',
    'Az Ziyārah'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'OOOTSA88X39U356X',
    'Jimmy',
    'Yokelman',
    '2002/01/08',
    'Huanggu'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'VRTAEL19L64M897F',
    'Annabella',
    'Messruther',
    '2005/06/18',
    'Póvoa'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'FEMWPY42Y80Y214S',
    'Eudora',
    'Dibbert',
    '1996/11/18',
    'Dongshandi'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'OTGHQH42T64P140C',
    'Benedick',
    'Fairhurst',
    '2002/10/18',
    'Härnösand'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'NCQIEQ15K76F302F',
    'Gary',
    'Bruton',
    '2003/10/09',
    'Hidalgo'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'OGZBHQ45I70G670Q',
    'Ramona',
    'Stenyng',
    '2002/04/29',
    'Nantian'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'ZOLJYR23U75I232W',
    'Garrard',
    'Stonnell',
    '2001/06/01',
    'Guadalupe'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'SQIAKF72Y93A420M',
    'Caty',
    'Conley',
    '1996/11/15',
    'Yakymivka'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'YCGIXY95J48I551Y',
    'Stanfield',
    'Krollman',
    '2002/07/06',
    'Sidowayah Lor'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'LRFDDW52O75X548L',
    'Jemie',
    'Matussov',
    '1995/04/14',
    'Banjar Tibubiyu Kaja'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'TSEEGK60N55E000I',
    'Chickie',
    'Fransson',
    '1999/02/11',
    'Qiaochong'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'RJUFUQ56Z30K256V',
    'Curcio',
    'Tregensoe',
    '1998/06/24',
    'Tegalgede'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'XCJWAN70I55Y672Q',
    'Nichole',
    'Cabedo',
    '1999/03/26',
    'Kudanding'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'XIABWK07C68U956W',
    'Zabrina',
    'Beechcraft',
    '2003/09/10',
    'Bobigny'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'BQQJPG32W96H483Y',
    'Ciro',
    'Layland',
    '2000/02/23',
    'Pleszew'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'KEWGZP92F56O006B',
    'Marcie',
    'Portugal',
    '2001/01/31',
    'Xuhui'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'WBUOGV71C24I353R',
    'Juliana',
    'Cardow',
    '1997/12/01',
    'Maninihon'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'ZQGVCK14S29C052K',
    'Stephie',
    'Vasse',
    '2003/06/21',
    'Turan'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'USXKWK64P05T689I',
    'Florida',
    'Liversage',
    '1998/09/07',
    'Palaiochóri'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'TOLXJJ56Y06R612Q',
    'Yoko',
    'Berkeley',
    '1999/03/13',
    'Liushan'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'EOKMPI18M70X819C',
    'Terry',
    'Bealton',
    '2000/09/09',
    'Kadaka'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'XTKWFH67K23M759B',
    'Hilarius',
    'Alleyn',
    '1999/09/30',
    'Serednye Vodyane'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'AQDBNI50W95P264G',
    'Doroteya',
    'Dalli',
    '2002/09/10',
    'Xuanbao'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'MPYRTU30W74Z913N',
    'Lark',
    'Oldcote',
    '1998/09/07',
    'Thul'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'EUCZCN20C22W554T',
    'Merissa',
    'Wigglesworth',
    '1995/11/24',
    'Dois Portos'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'SEGDFN40X35P031S',
    'Marcellina',
    'Dozdill',
    '1995/07/31',
    'Vimieiro'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'MNJNAZ51K45B109D',
    'Celka',
    'Tyrone',
    '2005/02/17',
    'Columbeira'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'QVCZNY42E34D688X',
    'Hertha',
    'Roderham',
    '2000/01/02',
    'Taizi'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'PTOQPY06Q41K952K',
    'Lyndel',
    'De Giorgi',
    '1995/05/08',
    'Yelwa'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'YQTHJT13X11Z504U',
    'Molli',
    'Cookney',
    '2000/02/12',
    'Santo Niño'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'ZYSNFF09T01V419Z',
    'Nicol',
    'Stace',
    '2005/02/05',
    'Tongquan'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'GWWVAJ19G89C894B',
    'Brigid',
    'McCart',
    '2002/04/24',
    'Chiriguaná'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'XQZNZT88G40U962D',
    'Sunny',
    'Palister',
    '2005/10/28',
    'Songjianghe'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'LBLWJN49K33U650E',
    'Ferd',
    'Durden',
    '2001/08/05',
    'Sophia Antipolis'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'HCSQMO65I24Z817A',
    'Filia',
    'Crumpe',
    '2003/06/29',
    'Schiedam postbusnummers'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'JMRRTQ47I38T582Z',
    'Orv',
    'Cuttles',
    '1996/07/14',
    'Jolo'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'EZKUWY53I63J761E',
    'Ilka',
    'Bakewell',
    '1997/12/14',
    'Dumandesa'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'HJSUMR71Q98Q339J',
    'Inglis',
    'Alsford',
    '2004/01/11',
    'Bogotá'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'ENHCER63F82Y177V',
    'Perri',
    'O''Tierney',
    '2001/05/23',
    'Nepalgunj'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'ORSTSS31P31Y104Y',
    'Berkie',
    'Lyburn',
    '1997/10/01',
    'Palilula'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'OBTTVM87L51Z692Z',
    'Wendi',
    'Okenfold',
    '2003/06/11',
    'Nantes'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'FLMPZB77R57T465B',
    'Frankie',
    'Overington',
    '1995/05/22',
    'Sabbah'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'GGMDLR52U26C879L',
    'Crosby',
    'Lowerson',
    '2003/03/20',
    'Baie-D''Urfé'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'MBKSMC17M77C623E',
    'Werner',
    'Easbie',
    '1996/10/16',
    'Mīzan Teferī'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'VPFBBL26B64D093I',
    'Nikolos',
    'Tithecote',
    '1999/12/24',
    'Walakeri'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'SZCRKZ21A66Y000I',
    'Albertina',
    'Duce',
    '2005/02/24',
    'Terangun'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'PNHRZY51K57T045M',
    'Zola',
    'Brownlie',
    '1999/11/07',
    'Primorsko'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'EMSXGX19U73Y018Q',
    'Halsey',
    'Grayham',
    '1995/11/04',
    'Mohoro'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'RKHKIZ04F23T048L',
    'Arvie',
    'Bea',
    '1998/11/10',
    'Sasar'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'IIMGLY19Z81U011J',
    'Joseph',
    'Robez',
    '1995/12/18',
    'Cruz'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'IJOPKY15Y52Z111S',
    'Ulric',
    'Loney',
    '1997/03/10',
    'Węgliniec'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'IRMTXD97T02V003V',
    'Lew',
    'Giraudot',
    '1999/06/25',
    'El Copey'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'CPZKFW33W23B864Y',
    'Ragnar',
    'Endley',
    '2003/12/25',
    'Satita'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'BPDOYJ05F83A048V',
    'Dasie',
    'Duffin',
    '1995/04/29',
    'Olival Basto'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'YKNFUW90F91N043S',
    'Lyman',
    'Tohill',
    '2005/07/24',
    'Krasnohrad'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'KGJRDH88C95S309R',
    'Joannes',
    'Crosi',
    '1998/03/23',
    'El Bálsamo'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'UXRYEF75F99X304P',
    'Leela',
    'Berling',
    '1996/08/22',
    'Angered'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'RIAUKA68G97P974D',
    'Lief',
    'Empringham',
    '2000/06/20',
    'Asopía'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'YHEZDA01G99S628D',
    'Raphael',
    'Rosewarne',
    '2001/06/29',
    'El Paso'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'VLXTYJ37E95C042S',
    'Naoma',
    'Yeldon',
    '2003/01/28',
    'Yunmen'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'NDXNEL65D00V610K',
    'Bobina',
    'McIlharga',
    '2003/06/28',
    'Salto'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'IFNUON73U42X110K',
    'Sylvan',
    'Gladyer',
    '1996/01/30',
    'Grzęska'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'OEJIKD12Z59V728U',
    'Aubrie',
    'Amori',
    '1997/01/10',
    'Dongshan'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'QEFHMI58C36L308U',
    'Liesa',
    'Haysom',
    '2004/07/18',
    'Panxi'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'JJRVFG97K19I543P',
    'Tamara',
    'Feldberg',
    '1999/09/27',
    'Binzhou'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'VWTBVB67H26J932O',
    'Charmain',
    'Greenlees',
    '2000/08/25',
    'Düsseldorf'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'LTIYZN66B36I743M',
    'Boonie',
    'Dursley',
    '2000/09/26',
    'Bakovci'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'YLJKYG88W20C316Z',
    'Paule',
    'Anderton',
    '1997/12/16',
    'Espinheira'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'TCJWPJ41Q64B896K',
    'Jeddy',
    'Haryngton',
    '2001/06/14',
    'Issoire'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'FPJYGA51I12Z671B',
    'Alden',
    'Isacke',
    '2002/12/17',
    'Barili'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'YVXTTE91H89W246D',
    'Bond',
    'Rodmell',
    '1996/02/10',
    'Betong'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'CZKEQU58R55A910I',
    'Lennard',
    'Garaghan',
    '1999/06/03',
    'Mafeteng'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'IFDNWI07R60M980D',
    'Twila',
    'Orht',
    '2002/08/24',
    'Herīs'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'DPUZSV45E98L322Z',
    'Ania',
    'Fley',
    '1999/05/02',
    'Makiwalo'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'TTEBSW38G74U733H',
    'Neile',
    'Saundercock',
    '1997/02/19',
    'Maoshan'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'QFYJGL54G37N125Y',
    'Eddie',
    'Friberg',
    '1996/08/24',
    'Chiang Yuen'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'HIHDLS06K42B080V',
    'King',
    'Timmis',
    '2002/02/13',
    'Fukuroi'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'VZZCJN28A11Q979W',
    'Moll',
    'Dallmann',
    '2001/03/05',
    'Talavera'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'VTWDOM11F00K287F',
    'Skipton',
    'Marrow',
    '2000/11/26',
    'Spirovo'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'MDEVHV23U54F992P',
    'Jud',
    'Gude',
    '1998/06/19',
    'Kremenki'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'ORIORF06F56T308P',
    'Hoyt',
    'Smeath',
    '1999/04/25',
    'Tarnogskiy Gorodok'
  );
  INSERT INTO ragazzo (codfiscale,nome,cognome,datanascita,cittàresidenza )
VALUES
  (
    'PWMZOT50Z96W342N',
    'Ninette',
    'Cattach',
    '1997/01/06',
    'Corral de Bustos'
  );
  /*
    Data for attività
    */
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    1,
    'CgFX',
    'MRI of Bi Renal Art using Oth Contrast, Unenh, Enhance',
    'Mauv'
  );
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    2,
    'MDB',
    'Removal of Nonaut Sub FROM R Humeral Head, Open Approach',
    'Crimson'
  );
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    3,
    'BSI Tax Factory',
    'Inspection of Neck, Open Approach',
    'Crimson'
  );
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    4,
    'Sleep Apnea',
    'Fusion T-lum Jt w Autol Sub, Ant Appr A Col, Perc Endo',
    'Maroon'
  );
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    5,
    'WMI',
    'Revision of INT Fix IN R Femur Shaft, Open Approach',
    'Blue'
  );
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    6,
    'Behavior Management',
    'Phys Rehab & Diag Audiology, Rehab, Motor Trmt',
    'Violet'
  );
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    7,
    'Ion Chromatography',
    'Introduce Nonaut Fertilized Ovum IN Fem Reprod, Perc',
    'Mauv'
  );
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    8,
    'CVS',
    'Beam Radiation of Brain using Photons >10 MeV',
    'Blue'
  );
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    9,
    'Frame Relay',
    'Plain Radiography of Right Ankle using Low Osmolar Contrast',
    'Orange'
  );
INSERT INTO attività (codattività, nome, descrizione, categoria)
VALUES
  (
    10,
    'Kinesiology',
    'Repair Common Bile Duct, Open Approach',
    'Crimson'
  );
  /*
        Data fot campo_estivo
        */
INSERT INTO campo_estivo (codcampo, nomecampo, città)
VALUES
  (1, 'November', 'Kibara');
INSERT INTO campo_estivo (codcampo, nomecampo, città)
VALUES
  (2, 'Alfa', 'Kibara');
INSERT INTO campo_estivo (codcampo, nomecampo, città)
VALUES
  (3, 'Charlie', 'Kibara');
INSERT INTO campo_estivo (codcampo, nomecampo, città)
VALUES
  (4, 'India', 'Manonjaya');
INSERT INTO campo_estivo (codcampo, nomecampo, città)
VALUES
  (5, 'Echo', 'Manonjaya');
INSERT INTO campo_estivo (codcampo, nomecampo, città)
VALUES
  (6, 'Victor', 'Verkhnyaya Tura');
INSERT INTO campo_estivo (codcampo, nomecampo, città)
VALUES
  (7, 'Echo', 'Verkhnyaya Tura');
  /*
    Data for iscrizione_per_attività_in_campo_estivo
    */

  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('TCJWPJ41Q64B896K', 8, 4, '2012/02/13');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('EUCZCN20C22W554T', 8, 1, '2012/09/18');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('EZKUWY53I63J761E', 5, 1, '2012/01/17');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('BQQJPG32W96H483Y', 6, 4, '2012/11/26');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XIABWK07C68U956W', 7, 4, '2012/10/19');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('LGUESE80L19B242N', 9, 6, '2012/10/29');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('EOKMPI18M70X819C', 2, 6, '2012/07/29');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OBTTVM87L51Z692Z', 2, 5, '2012/12/07');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('EZKUWY53I63J761E', 2, 5, '2012/10/01');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('IPMBZS65V32M583U', 8, 7, '2012/09/13');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('GWWVAJ19G89C894B', 8, 4, '2012/02/06');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('SQIAKF72Y93A420M', 3, 6, '2012/05/09');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('CPZKFW33W23B864Y', 4, 5, '2012/06/03');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('GGMDLR52U26C879L', 9, 6, '2012/11/01');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OOOTSA88X39U356X', 9, 7, '2012/07/30');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('RDGXSQ52T80U346W', 4, 1, '2012/09/11');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('SZCRKZ21A66Y000I', 8, 5, '2012/03/19');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('FLMPZB77R57T465B', 7, 4, '2012/01/10');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('HCSQMO65I24Z817A', 3, 3, '2012/02/25');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('LQENJZ10X37D528H', 4, 5, '2012/08/06');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('IFNUON73U42X110K', 10, 7, '2012/07/30');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('TOLXJJ56Y06R612Q', 1, 7, '2012/11/01');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YCGIXY95J48I551Y', 9, 3, '2012/07/17');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('TLOWRQ58C89E833N', 6, 2, '2012/09/09');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('QEFHMI58C36L308U', 2, 6, '2012/05/26');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OOOTSA88X39U356X', 9, 5, '2012/07/20');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('CLVIKY77Q51B860C', 5, 4, '2012/07/11');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('EMSXGX19U73Y018Q', 4, 6, '2012/09/01');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VKBHNW23H60S951F', 3, 1, '2012/11/12');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ZINCHQ68G16Q241P', 2, 1, '2012/06/24');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('CLVIKY77Q51B860C', 1, 4, '2012/03/24');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('LQENJZ10X37D528H', 7, 3, '2012/08/10');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('LBLWJN49K33U650E', 10, 5, '2012/06/17');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('RJUFUQ56Z30K256V', 3, 6, '2012/11/01');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('RJUFUQ56Z30K256V', 10, 7, '2012/09/24');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ZQGVCK14S29C052K', 7, 7, '2012/04/16');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ORIORF06F56T308P', 5, 5, '2012/06/24');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('LBLWJN49K33U650E', 10, 2, '2012/02/10');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YHEZDA01G99S628D', 7, 6, '2012/08/01');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('HIHDLS06K42B080V', 6, 3, '2012/10/05');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OBTTVM87L51Z692Z', 5, 1, '2012/07/14');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XCJWAN70I55Y672Q', 8, 2, '2012/04/30');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('USXKWK64P05T689I', 6, 6, '2012/01/19');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('IJOPKY15Y52Z111S', 7, 4, '2012/06/17');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YKNFUW90F91N043S', 10, 6, '2012/03/21');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('TTEBSW38G74U733H', 8, 1, '2012/09/28');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('JJRVFG97K19I543P', 2, 7, '2012/12/11');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('TCJWPJ41Q64B896K', 10, 7, '2012/09/28');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('CPZKFW33W23B864Y', 5, 1, '2012/03/28');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('LGUESE80L19B242N', 7, 2, '2012/05/01');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OOOTSA88X39U356X', 6, 2, '2012/01/06');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('TCJWPJ41Q64B896K', 10, 6, '2012/06/21');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XIABWK07C68U956W', 9, 2, '2012/09/28');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('IFNUON73U42X110K', 5, 4, '2012/08/25');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('CLVIKY77Q51B860C', 6, 4, '2012/06/29');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VKBHNW23H60S951F', 6, 6, '2012/06/20');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('LQENJZ10X37D528H', 3, 4, '2012/10/24');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('PQEKHU75L88Z322U', 7, 3, '2012/07/06');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('AQDBNI50W95P264G', 1, 7, '2012/12/03');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XQZNZT88G40U962D', 10, 7, '2012/07/23');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VKBHNW23H60S951F', 2, 5, '2012/08/02');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('USXKWK64P05T689I', 3, 1, '2012/06/12');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('GFMPKS27P22B473G', 3, 2, '2012/04/16');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OGZBHQ45I70G670Q', 8, 4, '2012/08/14');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OGZBHQ45I70G670Q', 8, 6, '2012/11/22');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('HIHDLS06K42B080V', 10, 5, '2012/03/27');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('EUCZCN20C22W554T', 2, 2, '2012/10/17');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('JJRVFG97K19I543P', 2, 2, '2012/10/11');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ORSTSS31P31Y104Y', 1, 6, '2012/07/22');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('IPMBZS65V32M583U', 8, 2, '2012/02/14');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('BQQJPG32W96H483Y', 10, 6, '2012/04/29');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ZOLJYR23U75I232W', 7, 6, '2012/04/22');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('BPDOYJ05F83A048V', 4, 6, '2012/02/17');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('USXKWK64P05T689I', 8, 1, '2012/10/26');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ENHCER63F82Y177V', 2, 5, '2012/02/12');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('AQDBNI50W95P264G', 4, 4, '2012/12/16');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('IRMTXD97T02V003V', 8, 1, '2012/03/14');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('EUCZCN20C22W554T', 2, 6, '2012/07/29');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('IFNUON73U42X110K', 9, 4, '2012/06/23');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VTWDOM11F00K287F', 2, 2, '2012/12/02');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VKBHNW23H60S951F', 5, 4, '2012/10/31');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('FEMWPY42Y80Y214S', 1, 2, '2012/06/19');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VPFBBL26B64D093I', 5, 6, '2012/04/12');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('JJRVFG97K19I543P', 8, 6, '2012/09/19');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('JMRRTQ47I38T582Z', 1, 6, '2012/02/08');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OBTTVM87L51Z692Z', 8, 4, '2012/11/18');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('KEWGZP92F56O006B', 6, 1, '2012/01/12');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XQZNZT88G40U962D', 1, 3, '2012/05/15');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('RDGXSQ52T80U346W', 1, 3, '2012/08/03');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YCGIXY95J48I551Y', 3, 4, '2012/08/29');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('GWWVAJ19G89C894B', 8, 6, '2012/12/06');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('EOKMPI18M70X819C', 2, 6, '2012/10/12');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('IPMBZS65V32M583U', 5, 6, '2012/06/15');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('NCQIEQ15K76F302F', 5, 6, '2012/11/17');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('LGUESE80L19B242N', 10, 2, '2012/04/22');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XIABWK07C68U956W', 1, 4, '2012/05/21');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XQZNZT88G40U962D', 7, 2, '2012/05/19');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('SZCRKZ21A66Y000I', 5, 4, '2012/08/09');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VZZCJN28A11Q979W', 1, 5, '2012/05/24');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ZINCHQ68G16Q241P', 9, 1, '2012/11/26');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('LTIYZN66B36I743M', 2, 7, '2012/08/22');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('PQEKHU75L88Z322U', 1, 4, '2012/11/12');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YHKWJJ38E54X808X', 3, 1, '2012/02/05');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('UXRYEF75F99X304P', 1, 7, '2012/05/15');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('CLVIKY77Q51B860C', 3, 2, '2012/12/17');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('LBLWJN49K33U650E', 7, 1, '2012/11/20');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('EZKUWY53I63J761E', 3, 1, '2012/06/24');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('EOKMPI18M70X819C', 3, 6, '2012/11/06');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('FEMWPY42Y80Y214S', 4, 2, '2012/10/07');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VZZCJN28A11Q979W', 1, 4, '2012/01/24');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OTGHQH42T64P140C', 7, 1, '2012/10/09');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('NDXNEL65D00V610K', 2, 6, '2012/07/19');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('HJSUMR71Q98Q339J', 2, 5, '2012/09/18');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('SQIAKF72Y93A420M', 2, 4, '2012/02/08');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('JMRRTQ47I38T582Z', 2, 7, '2012/12/24');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('PNHRZY51K57T045M', 10, 5, '2012/09/07');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('GGMDLR52U26C879L', 2, 6, '2012/08/07');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('RJUFUQ56Z30K256V', 6, 6, '2012/11/09');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OBTTVM87L51Z692Z', 10, 1, '2012/10/19');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YHEZDA01G99S628D', 8, 6, '2012/05/06');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('TLOWRQ58C89E833N', 7, 2, '2012/08/28');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('FEMWPY42Y80Y214S', 3, 3, '2012/01/12');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('SZCRKZ21A66Y000I', 10, 2, '2012/12/02');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('IFDNWI07R60M980D', 2, 3, '2012/09/05');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ZOLJYR23U75I232W', 4, 3, '2012/09/04');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('HCSQMO65I24Z817A', 5, 6, '2012/11/17');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('GFMPKS27P22B473G', 7, 4, '2012/12/28');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('USXKWK64P05T689I', 3, 4, '2012/12/07');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ZQGVCK14S29C052K', 3, 5, '2012/02/21');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XCJWAN70I55Y672Q', 10, 7, '2012/12/11');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('CZKEQU58R55A910I', 7, 3, '2012/04/14');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('USXKWK64P05T689I', 2, 7, '2012/02/19');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('BPDOYJ05F83A048V', 10, 7, '2012/05/29');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('RDGXSQ52T80U346W', 10, 4, '2012/12/19');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VLSZIW89F67F410S', 5, 4, '2012/08/12');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OEJIKD12Z59V728U', 6, 3, '2012/10/16');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ZYSNFF09T01V419Z', 4, 5, '2012/01/17');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VKBHNW23H60S951F', 4, 4, '2012/03/02');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YVXTTE91H89W246D', 7, 7, '2012/09/21');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('CPZKFW33W23B864Y', 9, 4, '2012/07/09');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('MBKSMC17M77C623E', 9, 5, '2012/01/21');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('KEWGZP92F56O006B', 2, 6, '2012/12/14');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('QEFHMI58C36L308U', 2, 7, '2012/05/09');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YLJKYG88W20C316Z', 2, 4, '2012/02/29');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('RIAUKA68G97P974D', 10, 7, '2012/03/20');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('KEWGZP92F56O006B', 5, 3, '2012/11/19');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('MBKSMC17M77C623E', 7, 5, '2012/12/01');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VRTAEL19L64M897F', 4, 3, '2012/10/28');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('TLOWRQ58C89E833N', 8, 4, '2012/05/30');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('PWMZOT50Z96W342N', 6, 7, '2012/09/06');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('HHARED79H66J944S', 3, 3, '2012/10/09');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('LQENJZ10X37D528H', 5, 4, '2012/10/07');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OEJIKD12Z59V728U', 10, 2, '2012/06/07');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XCJWAN70I55Y672Q', 10, 3, '2012/12/02');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('IJOPKY15Y52Z111S', 7, 6, '2012/08/08');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XCJWAN70I55Y672Q', 6, 6, '2012/04/15');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('QFYJGL54G37N125Y', 4, 2, '2012/01/24');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('KGJRDH88C95S309R', 5, 5, '2012/03/07');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('TCJWPJ41Q64B896K', 8, 1, '2012/09/07');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OGZBHQ45I70G670Q', 10, 7, '2012/04/15');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OTGHQH42T64P140C', 5, 5, '2012/11/01');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('CTLBRQ36G52P402I', 9, 7, '2012/06/15');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('FEMWPY42Y80Y214S', 1, 2, '2012/07/11');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('PTOQPY06Q41K952K', 1, 2, '2012/05/23');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('QVCZNY42E34D688X', 9, 4, '2012/10/15');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('LTIYZN66B36I743M', 4, 1, '2012/04/03');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('MBKSMC17M77C623E', 9, 7, '2012/10/15');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XQZNZT88G40U962D', 4, 4, '2012/07/29');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YLJKYG88W20C316Z', 1, 6, '2012/01/29');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ZINCHQ68G16Q241P', 7, 6, '2012/12/09');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YLJKYG88W20C316Z', 10, 5, '2012/10/16');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('NDXNEL65D00V610K', 1, 3, '2012/08/16');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('SZCRKZ21A66Y000I', 3, 1, '2012/10/05');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('FEMWPY42Y80Y214S', 7, 2, '2012/08/11');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('MNJNAZ51K45B109D', 8, 4, '2012/06/29');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ORSTSS31P31Y104Y', 3, 5, '2012/06/12');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('DPUZSV45E98L322Z', 8, 6, '2012/09/29');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('QEFHMI58C36L308U', 1, 4, '2012/08/21');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YVXTTE91H89W246D', 6, 6, '2012/11/29');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VZZCJN28A11Q979W', 2, 7, '2012/05/24');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('GFMPKS27P22B473G', 4, 3, '2012/11/20');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('HJSUMR71Q98Q339J', 4, 4, '2012/05/18');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VLXTYJ37E95C042S', 4, 7, '2012/05/11');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('DPUZSV45E98L322Z', 5, 6, '2012/11/21');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YKNFUW90F91N043S', 3, 3, '2012/02/28');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OBTTVM87L51Z692Z', 7, 2, '2012/06/13');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('EZKUWY53I63J761E', 1, 2, '2012/02/05');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('IFDNWI07R60M980D', 2, 2, '2012/03/24');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('CPZKFW33W23B864Y', 4, 1, '2012/03/23');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XIABWK07C68U956W', 1, 5, '2012/11/01');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ZOLJYR23U75I232W', 1, 5, '2012/09/02');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('MPYRTU30W74Z913N', 6, 5, '2012/03/26');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XTKWFH67K23M759B', 10, 4, '2012/12/18');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('SQIAKF72Y93A420M', 3, 4, '2012/10/23');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OTGHQH42T64P140C', 10, 6, '2012/06/10');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('IPMBZS65V32M583U', 1, 7, '2012/08/11');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VWTBVB67H26J932O', 9, 2, '2012/04/07');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YQTHJT13X11Z504U', 4, 3, '2012/07/26');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('TOLXJJ56Y06R612Q', 7, 3, '2012/11/14');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('NCQIEQ15K76F302F', 3, 3, '2012/01/27');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OOOTSA88X39U356X', 7, 2, '2012/02/27');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('GFMPKS27P22B473G', 8, 2, '2012/12/23');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('EUCZCN20C22W554T', 4, 3, '2012/08/26');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('SZCRKZ21A66Y000I', 10, 1, '2012/10/10');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('SQIAKF72Y93A420M', 9, 5, '2012/04/20');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YLJKYG88W20C316Z', 1, 5, '2012/02/20');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('BQQJPG32W96H483Y', 3, 5, '2012/05/09');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('SEGDFN40X35P031S', 2, 3, '2012/07/18');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('AQDBNI50W95P264G', 1, 1, '2012/10/26');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OGZBHQ45I70G670Q', 4, 2, '2012/09/08');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('CLVIKY77Q51B860C', 3, 5, '2012/03/25');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('EMSXGX19U73Y018Q', 10, 2, '2012/01/06');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('TTEBSW38G74U733H', 2, 4, '2012/08/06');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VLSZIW89F67F410S', 3, 3, '2012/02/09');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('QFYJGL54G37N125Y', 8, 6, '2012/01/17');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YLJKYG88W20C316Z', 7, 3, '2012/09/23');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OVBQSO71K30C901Q', 1, 4, '2012/03/13');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('TOLXJJ56Y06R612Q', 7, 2, '2012/05/24');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ZQGVCK14S29C052K', 1, 6, '2012/08/20');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YLJKYG88W20C316Z', 6, 5, '2012/06/09');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VWTBVB67H26J932O', 1, 7, '2012/03/16');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('JMRRTQ47I38T582Z', 9, 7, '2012/06/18');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('EZKUWY53I63J761E', 4, 3, '2012/11/19');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('LGUESE80L19B242N', 7, 7, '2012/09/27');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('SZCRKZ21A66Y000I', 2, 1, '2012/06/11');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XIABWK07C68U956W', 2, 6, '2012/01/31');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('EMSXGX19U73Y018Q', 2, 1, '2012/04/29');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OBTTVM87L51Z692Z', 7, 2, '2012/05/06');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OBTTVM87L51Z692Z', 3, 2, '2012/11/21');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VKBHNW23H60S951F', 3, 6, '2012/04/19');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('EOKMPI18M70X819C', 3, 4, '2012/06/14');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YHKWJJ38E54X808X', 10, 7, '2012/12/23');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('PTOQPY06Q41K952K', 3, 3, '2012/08/03');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('PWMZOT50Z96W342N', 7, 7, '2012/09/06');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ZINCHQ68G16Q241P', 8, 2, '2012/07/26');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ORSTSS31P31Y104Y', 4, 5, '2012/08/14');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XQZNZT88G40U962D', 5, 7, '2012/04/07');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XCJWAN70I55Y672Q', 8, 3, '2012/08/10');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('MDEVHV23U54F992P', 10, 4, '2012/03/20');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XCJWAN70I55Y672Q', 6, 1, '2012/07/26');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('EZKUWY53I63J761E', 10, 4, '2012/12/20');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('IPMBZS65V32M583U', 5, 2, '2012/10/31');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YLJKYG88W20C316Z', 3, 6, '2012/10/14');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ZINCHQ68G16Q241P', 10, 4, '2012/04/30');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VLXTYJ37E95C042S', 1, 6, '2012/08/18');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('LBLWJN49K33U650E', 5, 2, '2012/01/11');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YLJKYG88W20C316Z', 4, 7, '2012/11/02');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('TCJWPJ41Q64B896K', 10, 2, '2012/08/29');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XCJWAN70I55Y672Q', 2, 5, '2012/11/06');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VRTAEL19L64M897F', 1, 3, '2012/10/25');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YKNFUW90F91N043S', 4, 3, '2012/08/23');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OTGHQH42T64P140C', 2, 6, '2012/11/27');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('TTEBSW38G74U733H', 7, 6, '2012/11/14');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OVBQSO71K30C901Q', 9, 5, '2012/12/06');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('RIAUKA68G97P974D', 9, 2, '2012/10/14');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('HCSQMO65I24Z817A', 2, 7, '2012/04/20');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('WBUOGV71C24I353R', 7, 4, '2012/05/23');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('CTLBRQ36G52P402I', 10, 1, '2012/05/11');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('CTLBRQ36G52P402I', 1, 6, '2012/05/31');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ORIORF06F56T308P', 6, 5, '2012/04/21');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('UXRYEF75F99X304P', 6, 1, '2012/07/31');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YVXTTE91H89W246D', 7, 4, '2012/04/08');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ZQGVCK14S29C052K', 2, 5, '2012/05/16');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('KGJRDH88C95S309R', 7, 4, '2012/07/13');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YHEZDA01G99S628D', 7, 7, '2012/10/06');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('DPUZSV45E98L322Z', 9, 3, '2012/01/12');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VPFBBL26B64D093I', 9, 5, '2012/03/02');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('OTGHQH42T64P140C', 1, 1, '2012/01/12');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XQZNZT88G40U962D', 4, 1, '2012/04/09');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('IJOPKY15Y52Z111S', 6, 2, '2012/04/17');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('CZKEQU58R55A910I', 4, 3, '2012/11/20');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('IIMGLY19Z81U011J', 5, 5, '2012/05/04');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XCJWAN70I55Y672Q', 4, 5, '2012/07/05');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('FEMWPY42Y80Y214S', 1, 7, '2012/07/06');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('FPJYGA51I12Z671B', 5, 2, '2012/12/17');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('USXKWK64P05T689I', 1, 6, '2012/06/22');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('HCSQMO65I24Z817A', 5, 3, '2012/03/18');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('HCSQMO65I24Z817A', 2, 6, '2012/10/12');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('MNJNAZ51K45B109D', 3, 1, '2012/11/11');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XTKWFH67K23M759B', 4, 3, '2012/07/13');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('EMSXGX19U73Y018Q', 4, 6, '2012/04/12');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('MPYRTU30W74Z913N', 7, 2, '2012/10/30');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YHKWJJ38E54X808X', 5, 6, '2012/12/29');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ZOLJYR23U75I232W', 4, 6, '2012/09/24');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('QEFHMI58C36L308U', 4, 2, '2012/07/07');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('FLMPZB77R57T465B', 5, 7, '2012/09/01');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('TOLXJJ56Y06R612Q', 3, 4, '2012/08/16');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('VZZCJN28A11Q979W', 10, 6, '2012/10/02');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('YVXTTE91H89W246D', 5, 6, '2012/06/18');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('LQENJZ10X37D528H', 3, 6, '2012/12/16');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('LRFDDW52O75X548L', 3, 1, '2012/06/20');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ENHCER63F82Y177V', 2, 7, '2012/01/07');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ZYSNFF09T01V419Z', 1, 6, '2012/05/06');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('PNHRZY51K57T045M', 9, 7, '2012/04/23');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('FPJYGA51I12Z671B', 6, 7, '2012/07/31');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('ZSMVAB91P68Y600Y', 2, 2, '2012/11/08');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('IFNUON73U42X110K', 4, 1, '2012/02/21');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('NCQIEQ15K76F302F', 7, 3, '2012/12/15');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('XIABWK07C68U956W', 9, 1, '2012/07/18');
  INSERT INTO iscrizione_per_attività_in_campo_estivo(codfiscale,codattività,codcampo,dataiscrizione)
VALUES
  ('GFMPKS27P22B473G', 4, 7, '2012/10/15');