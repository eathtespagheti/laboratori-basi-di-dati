DROP TABLE IF EXISTS studente CASCADE;
DROP TABLE IF EXISTS laboratorio CASCADE;
DROP TABLE IF EXISTS dispositivo CASCADE;
DROP TABLE IF EXISTS esperimento CASCADE;
CREATE TABLE studente (
  matricola SMALLINT PRIMARY KEY,
  nome VARCHAR(200) NOT NULL,
  cognome VARCHAR(200) NOT NULL,
  corsodilaurea VARCHAR(200)
);
CREATE TABLE laboratorio (
  codlab SMALLINT PRIMARY KEY,
  nomelab VARCHAR(200) NOT NULL,
  capienza SMALLINT NOT NULL
);
CREATE TABLE dispositivo (
  codicedisp SMALLINT PRIMARY KEY,
  nomedisp VARCHAR(200) NOT NULL,
  tipo VARCHAR(200) NOT NULL,
  codlab SMALLINT NOT NULL REFERENCES laboratorio
);
CREATE TABLE esperimento (
  codicedisp SMALLINT NOT NULL REFERENCES dispositivo,
  matricola SMALLINT NOT NULL  REFERENCES studente,
  data DATE,
  descrizione VARCHAR(500),
  categoria VARCHAR(200),


  PRIMARY KEY (codicedisp, matricola, data)
);
/*
Data for studente
*/
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (1, 'Jena', 'De Paepe', 'Ingegneria Informatica');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (2, 'Ethyl', 'Bertelmot', 'Ingegneria Informatica');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (3, 'Kimball', 'Hugonet', 'Ingegneria Informatica');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (4, 'Tamas', 'Gounod', 'Ingegneria Informatica');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (5, 'Shelby', 'Meers', 'Ingegneria Informatica');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (6, 'Laurens', 'Northleigh', 'Ingegneria Informatica');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (7, 'Ethelred', 'Maybery', 'Ingegneria Informatica');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (8, 'Agatha', 'Scanterbury', 'Ingegneria Informatica');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (
    9,
    'Hugues',
    'Ivey',
    'Online Reputation Management'
  );
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (10, 'Andee', 'Mungane', 'Ingegneria Informatica');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (11, 'Boniface', 'Volant', 'Ingegneria Informatica');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (12, 'Mallorie', 'Kitson', 'Ingegneria Informatica');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (13, 'Sibilla', 'Wynch', 'Ingegneria Informatica');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (14, 'Julianne', 'Corwin', 'Ingegneria Informatica');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (15, 'Gunner', 'Ellen', 'Ingegneria Informatica');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (16, 'Dukey', 'Fludder', 'Ingegneria Informatica');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (17, 'Bentley', 'Farnaby', 'NGS');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (18, 'Ortensia', 'Bolver', 'Molecular Dynamics');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (19, 'Aleda', 'Matantsev', 'NUnit');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (20, 'Sela', 'Kiffe', 'IES Virtual Environment');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (
    21,
    'Guillaume',
    'Jerdon',
    'Quality Patient Care'
  );
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (22, 'Adelice', 'Vivers', 'Ventilation');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (23, 'Ambrosio', 'Brauns', 'Mobile Marketing');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (
    24,
    'Marlyn',
    'Ludwikiewicz',
    'Pharmacy Benefit Management'
  );
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (25, 'Thane', 'Ferrara', 'Kaizen Leadership');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (26, 'Dasya', 'Eliesco', 'VSE');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (27, 'Dyanna', 'Bartleman', 'Ebay Sales');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (28, 'Charo', 'Kehoe', 'Lytec');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (29, 'Faina', 'Ronchka', 'CMake');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (
    30,
    'Brandais',
    'Tomasoni',
    'Variable Frequency Drives'
  );
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (31, 'Jerald', 'Grene', 'DPF');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (32, 'Stefania', 'Barbey', 'Start-up Ventures');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (33, 'Boyce', 'Bolan', 'Drip Irrigation');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (34, 'Lexine', 'Ginity', 'Negotiation');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (35, 'Marti', 'Brickhill', 'TCD');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (36, 'Amelita', 'Jakubowski', 'JDBC');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (
    37,
    'Gerladina',
    'Ivannikov',
    'Artistic Abilities'
  );
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (38, 'Mindy', 'Oswell', 'SEO');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (39, 'Eada', 'Wilkie', 'Avid DS Nitris');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (40, 'Homer', 'Whal', 'Cost Efficiency');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (41, 'Ronnica', 'Harpham', 'Lotus Domino');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (42, 'Teddie', 'Deeson', 'German');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (43, 'Dorothea', 'Baldacchi', 'iSCSI');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (44, 'Moishe', 'Melsom', 'Biopharmaceuticals');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (45, 'Tallulah', 'Feron', 'Electrical Controls');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (46, 'Thorin', 'Lacey', 'Izotope RX');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (47, 'Constantino', 'Quittonden', 'SAP PP');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (48, 'Sonia', 'Gofforth', 'TPP');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (49, 'Dasha', 'Baines', 'Private Events');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (50, 'George', 'Mansel', 'Type Approval');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (51, 'Lynne', 'Vinden', 'RSA Ace Server');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (52, 'Duff', 'Piatti', 'Water Quality');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (53, 'Quillan', 'Partleton', 'Health Psychology');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (54, 'Sherlocke', 'Spinas', 'Pay TV');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (55, 'Gabey', 'Blackstock', 'Solution Selling');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (56, 'Rozalin', 'Quinney', 'Urban');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (
    57,
    'Lynnelle',
    'Martinovsky',
    'Career Counseling'
  );
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (58, 'Romonda', 'Gosnold', 'Helicopters');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (59, 'Pamelina', 'Dur', 'Fraud');
INSERT INTO studente (matricola, nome, cognome, corsodilaurea)
VALUES
  (60, 'Clem', 'Langton', 'Xara');
  /*
  data for laboratorio
  */
INSERT INTO laboratorio (codlab, nomelab, capienza)
VALUES
  (1, 'Kilo', 34);
INSERT INTO laboratorio (codlab, nomelab, capienza)
VALUES
  (2, 'Tango Juliett', 35);
INSERT INTO laboratorio (codlab, nomelab, capienza)
VALUES
  (3, 'Victor Papa Tango', 31);
INSERT INTO laboratorio (codlab, nomelab, capienza)
VALUES
  (4, 'Echo', 34);
INSERT INTO laboratorio (codlab, nomelab, capienza)
VALUES
  (5, 'Papa', 14);
INSERT INTO laboratorio (codlab, nomelab, capienza)
VALUES
  (6, 'Bravo', 30);
INSERT INTO laboratorio (codlab, nomelab, capienza)
VALUES
  (7, 'Delta Alfa Papa', 18);
INSERT INTO laboratorio (codlab, nomelab, capienza)
VALUES
  (8, 'Victor India', 23);
INSERT INTO laboratorio (codlab, nomelab, capienza)
VALUES
  (9, 'India Alfa Oscar', 9);
INSERT INTO laboratorio (codlab, nomelab, capienza)
VALUES
  (10, 'Oscar', 26);
  /*
  data for dispositivo
  */
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (1, 'Tres-Zap', 'visa-electron', 7);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (2, 'Bytecard', 'bankcard', 5);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (3, 'Home Ing', 'diners-club-us-ca', 2);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (4, 'Alpha', 'jcb', 8);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (5, 'Tempsoft', 'jcb', 7);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (6, 'It', 'diners-club-carte-blanche', 9);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (7, 'Fixflex', 'jcb', 10);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (8, 'Bigtax', 'jcb', 8);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (9, 'Hatity', 'jcb', 5);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (10, 'Otcom', 'maestro', 7);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (11, 'Andalax', 'jcb', 2);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (12, 'Zamit', 'jcb', 9);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (13, 'Cardify', 'maestro', 3);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (14, 'Solarbreeze', 'visa', 10);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (15, 'Bytecard', 'jcb', 8);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (16, 'Pannier', 'jcb', 2);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (17, 'Domainer', 'bankcard', 1);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (18, 'Zoolab', 'diners-club-enroute', 10);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (19, 'Duobam', 'mastercard', 9);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (20, 'Home Ing', 'jcb', 10);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (21, 'Sonair', 'jcb', 1);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (22, 'Flexidy', 'diners-club-international', 4);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (23, 'Home Ing', 'jcb', 3);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (24, 'Keylex', 'diners-club-enroute', 2);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (25, 'Toughjoyfax', 'jcb', 1);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (26, 'Fix San', 'jcb', 4);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (27, 'Regrant', 'jcb', 3);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (28, 'Konklux', 'jcb', 3);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (29, 'Domainer', 'jcb', 2);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (30, 'Fixflex', 'jcb', 9);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (31, 'Zaam-Dox', 'jcb', 2);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (32, 'Pannier', 'jcb', 7);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (33, 'Zoolab', 'americanexpress', 9);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (34, 'Konklab', 'diners-club-carte-blanche', 1);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (35, 'Tin', 'mastercard', 7);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (36, 'Andalax', 'jcb', 8);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (37, 'Konklab', 'bankcard', 7);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (38, 'Viva', 'diners-club-us-ca', 7);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (39, 'Zathin', 'diners-club-carte-blanche', 6);
INSERT INTO dispositivo (codicedisp, nomedisp, tipo, codlab)
VALUES
  (40, 'Voyatouch', 'americanexpress', 2);
  /*
  esperimento data
  */
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    7,
    '2019/07/07',
    'Radiation Therapy, Lymph & Hemat Sys, Stereo Radiosurg',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    8,
    '2020/01/28',
    'Repair Right Hip Bursa and Ligament, Percutaneous Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    18,
    20,
    '2020/03/20',
    'Extirpation of Matter FROM Uvula, External Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    1,
    12,
    '2019/06/28',
    'Supplement Right Temporal Bone with Synth Sub, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    39,
    11,
    '2019/11/14',
    'Excision of Bilateral Lungs, Via Opening',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    38,
    36,
    '2019/12/29',
    'Drainage of L Thyroid Art with Drain Dev, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    35,
    13,
    '2020/01/06',
    'Dilation of Stomach, Pylorus, Percutaneous Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    45,
    '2019/10/26',
    'Excision of Upper Artery, Open Approach, Diagnostic',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    8,
    55,
    '2020/01/28',
    'CT Scan of Abd & Pelvis using H Osm Contrast',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    36,
    33,
    '2019/10/26',
    'Dilation of L Renal Art with 2 Drug-elut, Perc Endo Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    28,
    55,
    '2019/05/22',
    'Drainage of Right Popliteal Artery, Perc Endo Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    12,
    11,
    '2019/04/26',
    'Monitoring of Venous Flow, Portal, Open Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    21,
    30,
    '2019/08/11',
    'Revise Nonaut Sub IN Prostate/Seminal Ves, Perc Endo',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    1,
    '2020/04/01',
    'Excision of Right Foot Muscle, Percutaneous Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    23,
    39,
    '2019/12/30',
    'Drainage of Lumbosacral Disc, Perc Endo Approach, Diagn',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    45,
    '2019/08/22',
    'Bypass Right Axillary Artery to L Up Arm Art, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    29,
    34,
    '2020/04/06',
    'LDR Brachytherapy of Uterus using Palladium 103',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    45,
    '2020/03/14',
    'Alteration of Male Perineum with Synth Sub, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    53,
    '2019/04/29',
    'Supplement Anus with Synth Sub, Perc Endo Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    21,
    '2020/03/17',
    'Destruction of Left Upper Arm Muscle, Perc Endo Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    11,
    23,
    '2019/12/21',
    'Drainage of L Finger Phalanx Jt, Open Approach, Diagn',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    15,
    26,
    '2020/01/19',
    'Bypass Abd Aorta to B Ext Ilia w Synth Sub, Perc Endo',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    12,
    5,
    '2019/10/04',
    'Measurement of Lymphatic Flow, Open Approach',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    25,
    14,
    '2020/02/03',
    'Fusion Sacrococcygeal Jt w Nonaut Sub, Perc Endo',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    29,
    24,
    '2020/04/04',
    'Alteration of Left Upper Arm with Nonaut Sub, Open Approach',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    18,
    44,
    '2019/09/19',
    'Inspection of Left Hip Joint, Perc Endo Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    40,
    30,
    '2019/08/03',
    'Revise Synth Sub IN L Knee Jt, Patella, Perc Endo',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    33,
    22,
    '2019/06/12',
    'Fusion of Right Metatarsal-Tarsal Joint, Perc Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    18,
    27,
    '2020/02/01',
    'Supplement Sigmoid Colon with Synth Sub, Via Opening',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    13,
    37,
    '2019/06/07',
    'Fusion Lumsac Jt w Intbd Fus Dev, Ant Appr A Col, Perc Endo',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    58,
    '2019/05/30',
    'Removal of Nonaut Sub FROM Penis, Perc Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    22,
    17,
    '2019/09/24',
    'Drainage of Right Ureter, Percutaneous Approach, Diagnostic',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    11,
    14,
    '2019/11/18',
    'Dilation of Pulm Valve with Intralum Dev, Perc Endo Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    24,
    26,
    '2019/07/21',
    'Restriction of L Pulm Vein with Intralum Dev, Open Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    46,
    '2019/07/23',
    'Occlusion of Transverse Colon, Percutaneous Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    40,
    26,
    '2019/05/30',
    'Drainage of Intracranial Vein with Drain Dev, Perc Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    23,
    33,
    '2020/01/17',
    'Repair Aortic Lymphatic, Percutaneous Endoscopic Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    22,
    15,
    '2020/01/05',
    'Excision of Bi Seminal Vesicle, Perc Endo Approach, Diagn',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    36,
    9,
    '2019/08/16',
    'Excision of Left Ovary, Percutaneous Approach, Diagnostic',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    24,
    28,
    '2019/07/06',
    'Dilate of Up Art, Bifurc, with 2 Intralum Dev, Perc Approach',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    21,
    '2019/12/30',
    'Revision of Synth Sub IN Fem Perineum, Perc Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    40,
    4,
    '2019/08/14',
    'Bypass Bi Vas Deferens to R Epidid w Nonaut Sub, Open',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    35,
    3,
    '2019/08/24',
    'Supplement Portal Vein with Nonaut Sub, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    35,
    46,
    '2019/07/02',
    'Removal of Synthetic Substitute FROM Ureter, Open Approach',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    29,
    50,
    '2019/07/15',
    'Drainage of Lumbar Sympathetic Nerve, Open Approach, Diagn',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    28,
    '2019/05/30',
    'Dilation of Celiac Artery with 3 Drug-elut, Open Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    40,
    38,
    '2020/03/14',
    'Drainage of Right Patella with Drain Dev, Open Approach',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    19,
    38,
    '2020/04/16',
    'Revision of Autol Sub IN L Patella, Perc Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    32,
    36,
    '2019/10/18',
    'Removal of Drain Dev FROM R Pleural Cav, Extern Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    40,
    17,
    '2019/11/23',
    'Revision of INT Fix IN L Fibula, Extern Approach',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    22,
    4,
    '2019/10/30',
    'Anatomical Regions, General, Supplement',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    19,
    1,
    '2020/03/23',
    'Replacement of R Sphenoid Bone with Synth Sub, Perc Approach',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    38,
    58,
    '2019/09/13',
    'Insertion of Monitor Dev INTO R Ventricle, Perc Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    30,
    32,
    '2019/12/23',
    'Release Nasal Bone, Percutaneous Endoscopic Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    6,
    39,
    '2020/02/13',
    'Release Head and Neck Sympathetic Nerve, Perc Endo Approach',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    15,
    14,
    '2019/08/04',
    'Repair Aortic Lymphatic, Percutaneous Endoscopic Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    32,
    5,
    '2019/11/22',
    'Osteopathic Treatment Cervcal Region w Fascial Release',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    40,
    38,
    '2020/02/01',
    'CT Scan of R Eye using Oth Contrast, Unenh, Enhance',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    36,
    22,
    '2019/09/22',
    'Extraction of Right Mastoid Sinus, Perc Endo Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    7,
    29,
    '2020/03/17',
    'Excision of Female Perineum, Open Approach',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    54,
    '2019/12/09',
    'Bypass Thor Aorta DESC to Carotid, Perc Endo Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    35,
    12,
    '2019/05/22',
    'Replacement of Gastric Artery with Nonaut Sub, Open Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    39,
    3,
    '2019/06/29',
    'Alteration of Left Upper Extremity, Open Approach',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    12,
    1,
    '2019/10/31',
    'Introduce Oth Diagn Subst IN Cran Cav/Brain, Perc',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    44,
    '2019/09/13',
    'Supplement R INT Jugular Vein w Autol Sub, Perc Endo',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    26,
    52,
    '2019/08/29',
    'Revision of INT Fix IN Sacrum, Perc Approach',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    13,
    18,
    '2020/04/20',
    'Beam Radiation of Hypopharynx using Photons <1 MeV',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    29,
    59,
    '2019/12/25',
    'Nonimag Nucl Med Prob of Head & Neck using Technetium 99m',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    23,
    15,
    '2019/12/25',
    'Occlusion R Low Lobe Bronc w Intralum Dev, Open',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    28,
    57,
    '2020/03/04',
    'Resection of Trachea, Percutaneous Endoscopic Approach',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    29,
    5,
    '2020/02/23',
    'Dilation of Jejunum, Via Natural OR Artificial Opening',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    56,
    '2019/05/13',
    'Restrict R Less Saphenous w Extralum Dev, Open',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    37,
    24,
    '2019/06/08',
    'Bypass Left Brachial Vein to Upper Vein, Open Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    16,
    46,
    '2019/11/21',
    'Excision of Right Pleura, Percutaneous Approach, Diagnostic',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    14,
    '2019/11/10',
    'Supplement Ventricular Sept w Autol Sub, Perc Endo',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    28,
    28,
    '2019/06/11',
    'Supplement L Low Extrem with Autol Sub, Perc Endo Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    23,
    27,
    '2019/06/01',
    'Transfer Head Muscle, Percutaneous Endoscopic Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    35,
    '2019/08/22',
    'Drainage of Inferior Vena Cava, Perc Endo Approach, Diagn',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    37,
    60,
    '2019/12/24',
    'REPLACE of Head & Neck Tendon with Synth Sub, Open Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    26,
    7,
    '2020/01/24',
    'Supplement Bi Fallopian Tube w Synth Sub, Perc Endo',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    26,
    2,
    '2019/09/16',
    'Extirpation of Matter FROM Right Fallopian Tube, Via Opening',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    39,
    48,
    '2019/12/06',
    'Removal of Intraluminal Device FROM Trachea, Via Opening',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    32,
    39,
    '2019/06/23',
    'Destruction of Head Lymphatic, Percutaneous Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    35,
    46,
    '2019/07/03',
    'Drainage of Bone Marrow, Percutaneous Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    11,
    42,
    '2019/08/19',
    'Revise Drain Dev IN Low Extrem Subcu/Fascia, Extern',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    13,
    18,
    '2019/08/18',
    'Inspection of Tracheobronchial Tree, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    8,
    58,
    '2020/03/10',
    'Dilate L Thyroid Art, Bifurc, w Intralum Dev, Perc',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    26,
    46,
    '2019/12/27',
    'Dilation of R Lacrml Duct with Intralum Dev, Perc Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    36,
    54,
    '2020/03/19',
    'Bypass Jejunum to Sigm Colon with Autol Sub, Open Approach',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    22,
    33,
    '2019/12/04',
    'Supplement Right Hand Artery with Autol Sub, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    22,
    18,
    '2019/09/26',
    'Supplement Lg Intest with Autol Sub, Perc Endo Approach',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    22,
    3,
    '2019/11/07',
    'Reposition Left Elbow Joint with Ext Fix, Perc Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    16,
    18,
    '2019/09/22',
    'Destruction of Right Fallopian Tube, Endo',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    6,
    12,
    '2020/01/01',
    'Reposition R Metacarpocarp Jt with Ext Fix, Perc Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    60,
    '2019/12/05',
    'Reposition Left Tarsal Joint with Ext Fix, Perc Approach',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    9,
    17,
    '2019/08/13',
    'INSERT Infusion Dev IN R Toe Phalanx Jt, Perc Endo',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    18,
    38,
    '2020/03/31',
    'Release Thoracolumbar Vertebral Joint, Percutaneous Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    60,
    '2019/05/26',
    'Replacement of Com Bile Duct with Autol Sub, Open Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    8,
    1,
    '2020/04/06',
    'Excision of Bilateral Testes, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    16,
    19,
    '2019/05/17',
    'Reattachment of R Low Extrem Bursa/Lig, Perc Endo Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    3,
    50,
    '2019/10/06',
    'Revision of Infusion Device IN R Carpal Jt, Extern Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    32,
    '2020/03/19',
    'Removal of Other Device ON Right Inguinal Region',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    36,
    53,
    '2020/01/27',
    'Removal of Autol Sub FROM L Metatarsal, Perc Endo Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    9,
    3,
    '2020/01/11',
    'Extraction of Lumbar Sympathetic Nerve, Open Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    7,
    2,
    '2019/09/26',
    'Supplement R Up Extrem Bursa/Lig w Synth Sub, Perc Endo',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    32,
    '2019/10/25',
    'REPLACE Chordae Tendineae w Synth Sub, Perc Endo',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    45,
    '2020/02/19',
    'Revision of Nonaut Sub IN Thor Disc, Perc Endo Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    46,
    '2019/10/02',
    'Occlusion R Less Saphenous w Extralum Dev, Perc',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    25,
    32,
    '2020/04/09',
    'Planar Nucl Med Imag of Liver using Oth Radionuclide',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    12,
    '2020/01/26',
    'Extirpation of Matter FROM Left Main Bronchus, Perc Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    7,
    60,
    '2019/08/06',
    'Alteration of Chest Wall with Synth Sub, Perc Endo Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    29,
    30,
    '2020/04/15',
    'Release Left External Ear, Percutaneous Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    2,
    56,
    '2019/12/26',
    'Dilate R Verteb Art, Bifurc, w Drug-elut Intra, Open',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    7,
    '2020/03/09',
    'Release Perineum Muscle, Percutaneous Endoscopic Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    6,
    '2019/08/02',
    'Introduction of Analg/Hypnot/Sedat INTO Joint, Perc Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    54,
    '2019/09/05',
    'Beam Radiation of Adrenal Glands using Photons <1 MeV',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    25,
    9,
    '2019/05/18',
    'Remove Autol Sub FROM R Metacarpophal Jt, Perc Endo',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    7,
    1,
    '2019/12/25',
    'Replacement of L Lacrml Duct with Autol Sub, Via Opening',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    37,
    '2019/07/23',
    'Fusion of Right Finger Phalangeal Joint, Perc Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    3,
    20,
    '2019/08/20',
    'REPLACE L INT Iliac Art w Autol Sub, Perc Endo',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    36,
    15,
    '2020/01/25',
    'Excision of Right Thorax Tendon, Perc Endo Approach, Diagn',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    39,
    33,
    '2019/05/01',
    'Reposition Left Tibia, External Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    42,
    '2019/06/17',
    'Revision of Autol Sub IN L Tarsal Jt, Perc Endo Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    33,
    50,
    '2020/01/09',
    'Bypass Abd Aorta to L Renal A w Autol Art, Perc Endo',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    4,
    '2020/03/03',
    'Revision of Autol Sub IN R Glenoid Cav, Open Approach',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    36,
    53,
    '2019/06/29',
    'Drainage of Spleen, Percutaneous Approach, Diagnostic',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    32,
    '2019/08/16',
    'Drainage of Right Submaxillary Gland, Perc Approach, Diagn',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    24,
    53,
    '2019/08/04',
    'Dilation of L INT Iliac Art with 2 Drug-elut, Perc Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    5,
    34,
    '2020/01/27',
    'MRI of Bi Up Extrem Vein using Oth Contrast',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    17,
    '2019/07/03',
    'Destruction of Cervical Spinal Cord, Open Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    14,
    52,
    '2019/12/17',
    'Removal of Infusion Device FROM R Up Extrem, Perc Approach',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    12,
    29,
    '2019/08/27',
    'Occlusion of Bladder with Intraluminal Device, Endo',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    11,
    41,
    '2019/12/05',
    'Remove Radioact Elem FROM R Low Extrem, Perc Endo',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    33,
    58,
    '2019/07/12',
    'REPLACE of L Abd Tendon with Nonaut Sub, Perc Endo Approach',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    3,
    54,
    '2019/11/05',
    'Bypass R Com Iliac Art to L Renal A w Autol Art, Open',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    35,
    4,
    '2019/06/09',
    'Removal of Autol Sub FROM L Breast, Extern Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    8,
    26,
    '2019/05/10',
    'Release Right External Jugular Vein, Perc Endo Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    4,
    44,
    '2019/11/19',
    'Excision of Left Large Intestine, Via Opening',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    23,
    7,
    '2019/08/08',
    'Repair Large Intestine, Percutaneous Approach',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    19,
    26,
    '2019/07/31',
    'Bypass L Com Iliac Art to L Ext Ilia w Synth Sub, Perc Endo',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    13,
    '2019/11/28',
    'Fragmentation IN Transverse Colon, Percutaneous Approach',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    14,
    31,
    '2019/11/18',
    'Inspection of Left Extraocular Muscle, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    31,
    23,
    '2019/08/11',
    'Restriction of Right Thyroid Artery, Percutaneous Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    37,
    50,
    '2019/06/22',
    'Resection of Left Abdomen Tendon, Perc Endo Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    19,
    43,
    '2019/07/23',
    'Bypass R Ext Iliac Art to L INT Ilia, Open Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    13,
    53,
    '2020/02/23',
    'Division of Right Upper Arm Tendon, Perc Endo Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    23,
    43,
    '2020/04/08',
    'Insertion of Other Device INTO GU Tract, Perc Endo Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    18,
    38,
    '2019/08/02',
    'Dilation of L Ext Carotid with 4 Drug-elut, Perc Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    13,
    53,
    '2019/10/08',
    'Supplement Larynx with Nonaut Sub, Open Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    18,
    14,
    '2019/05/24',
    'Resection of Nasal Septum, Percutaneous Endoscopic Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    16,
    17,
    '2019/08/06',
    'LDR Brachytherapy of Peripheral Nerve using Palladium 103',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    12,
    23,
    '2019/08/22',
    'Excision of Bladder, Endo',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    4,
    31,
    '2019/05/28',
    'Division of Right Ear Skin, External Approach',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    30,
    50,
    '2019/06/08',
    'Remove of Drain Dev FROM Endocrine Gland, Perc Endo Approach',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    18,
    15,
    '2019/12/10',
    'Laser Interstitial Thermal Therapy of Mediastinum',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    6,
    54,
    '2019/07/09',
    'Bypass L INNER Ear to Endolymphatic w Nonaut Sub, Open',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    4,
    58,
    '2019/08/22',
    'Transfuse Autol Plasma Cryoprecip IN Periph Art, Open',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    25,
    12,
    '2020/02/04',
    'Drainage of Left Upper Leg Muscle, Percutaneous Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    11,
    15,
    '2019/08/30',
    'Supplement Pancreatic Duct with Nonaut Sub, Open Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    14,
    '2019/07/14',
    'Revision of Monitoring Device IN Heart, Perc Approach',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    45,
    '2020/01/22',
    'Supplement Left Humeral Shaft with Synth Sub, Perc Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    24,
    17,
    '2020/01/01',
    'Reattachment of Clitoris, External Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    25,
    26,
    '2019/05/28',
    'Occlusion R Hypogast Vein w Extralum Dev, Perc',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    14,
    '2019/10/30',
    'Plaque Radiation of Humerus',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    4,
    60,
    '2019/07/07',
    'Drainage of R Up Leg Tendon, Perc Endo Approach, Diagn',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    13,
    31,
    '2019/06/01',
    'Drainage of Pudendal Nerve with Drain Dev, Perc Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    40,
    41,
    '2019/07/03',
    'Removal of Synthetic Substitute FROM Bladder, Perc Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    6,
    13,
    '2020/01/18',
    'Supplement Inf Mesent Art with Synth Sub, Perc Approach',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    21,
    3,
    '2020/01/29',
    'Drainage of Left Anterior Chamber, Perc Approach, Diagn',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    38,
    43,
    '2019/05/16',
    'Revision of Synth Sub IN L Knee Jt, Tibial, Perc Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    22,
    39,
    '2019/07/18',
    'Insertion of Other Device INTO Left Lower Leg, Open Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    5,
    48,
    '2020/04/19',
    'Drainage of Right Ureter, Endo, Diagn',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    1,
    40,
    '2020/04/24',
    'Supplement L Low Lobe Bronc w Synth Sub, Perc Endo',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    19,
    59,
    '2019/12/03',
    'HDR Brachytherapy of Head & Neck using Cesium 137',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    6,
    56,
    '2019/08/01',
    'Supplement Left Foot Vein with Autol Sub, Perc Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    40,
    5,
    '2020/03/30',
    'Radiography of Gallbladder & Bile Duct using L Osm Contrast',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    31,
    39,
    '2019/05/26',
    'Excision of Transverse Colon, Via Opening',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    21,
    56,
    '2019/07/17',
    'Excision of Left Sclera, External Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    30,
    '2019/10/14',
    'Extirpation of Matter FROM R Sperm Cord, Perc Endo Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    36,
    55,
    '2020/01/19',
    'Excision of L Ankle Bursa/Lig, Perc Endo Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    37,
    59,
    '2019/10/11',
    'Dilation of R Subclav Vein with Intralum Dev, Perc Approach',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    9,
    12,
    '2019/05/15',
    'Excision of Left Glenoid Cavity, Percutaneous Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    18,
    22,
    '2019/07/18',
    'Extirpation of Matter FROM R Lacrimal Bone, Open Approach',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    47,
    '2019/08/15',
    'Insertion of Infusion Dev INTO R Com Carotid, Perc Approach',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    21,
    4,
    '2019/06/18',
    'Transfer Right Trunk Muscle with Subcu, Perc Endo Approach',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    37,
    '2019/06/28',
    'Revision of Ext Fix IN R Humeral Shaft, Perc Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    22,
    '2019/09/17',
    'Repair Thyroid Gland, Open Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    33,
    45,
    '2019/11/06',
    'Extraction of Oculomotor Nerve, Percutaneous Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    19,
    20,
    '2019/09/27',
    'CT Scan of Bi Up Extrem using H Osm Contrast',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    33,
    8,
    '2019/06/05',
    'Removal of Drainage Device FROM Lum Jt, Perc Endo Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    18,
    44,
    '2019/11/06',
    'Dilate R Foot Art, Bifurc, w 2 Intralum Dev, Open',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    14,
    58,
    '2020/01/07',
    'Drainage of Great Omentum with Drain Dev, Perc Endo Approach',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    16,
    26,
    '2019/07/25',
    'Excision of Right Ureter, Via Natural OR Artificial Opening',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    15,
    3,
    '2019/06/22',
    'Release Left Peroneal Artery, Open Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    25,
    10,
    '2019/07/22',
    'Destruction of Thoracic Vertebral Joint, Perc Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    38,
    48,
    '2020/03/03',
    'Repair Facial Nerve, Percutaneous Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    38,
    8,
    '2019/11/01',
    'Excision of Left Lower Eyelid, Percutaneous Approach',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    36,
    16,
    '2019/10/09',
    'Occlusion of R Foot Vein with Intralum Dev, Open Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    18,
    '2019/09/30',
    'Removal of Drainage Device FROM T-lum Disc, Extern Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    32,
    11,
    '2019/06/24',
    'Irrigation of Male Reproductive using Irrigat, Perc Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    1,
    56,
    '2019/08/03',
    'INSERT Infusion Dev IN L INT Jugular Vein, Perc Endo',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    24,
    '2020/03/23',
    'Destruction of Head Neck Symp Nrv, Perc Approach',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    8,
    11,
    '2019/12/06',
    'Revision of Hearing Device IN Left INNER Ear, Open Approach',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    24,
    48,
    '2019/11/24',
    'Dilate L Peroneal Art w 2 Drug-elut, Perc Endo',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    32,
    58,
    '2020/02/09',
    'Excision of Scrotum, Open Approach, Diagnostic',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    47,
    '2020/02/08',
    'Alteration of Nose with Synthetic Substitute, Perc Approach',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    24,
    14,
    '2019/08/26',
    'Drainage of Left Lower Eyelid, Open Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    40,
    45,
    '2020/02/27',
    'Removal of Synth Sub FROM L Wrist Jt, Perc Approach',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    53,
    '2019/09/02',
    'Supplement Thor Aorta ASC with Autol Sub, Perc Endo Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    6,
    45,
    '2020/03/22',
    'Supplement Ileum with Synthetic Substitute, Via Opening',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    39,
    30,
    '2019/12/17',
    'Revision of Autol Sub IN Lumsac Jt, Perc Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    13,
    20,
    '2019/09/11',
    'Excision of Hymen, Percutaneous Endoscopic Approach, Diagn',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    21,
    42,
    '2019/08/25',
    'Stereotactic Gamma Beam Radiosurgery of Left Breast',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    38,
    '2019/12/20',
    'Hyperthermia of Abdomen',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    28,
    19,
    '2019/07/07',
    'Repair Left 1st Toe, External Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    30,
    51,
    '2019/06/29',
    'Revision of Synth Sub IN R Up Femur, Extern Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    2,
    17,
    '2019/04/26',
    'Excision of Right Hand, Percutaneous Endoscopic Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    32,
    26,
    '2020/04/11',
    'Dilate L Popl Art w Drug-elut Intra, Drug Blln, Perc Endo',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    12,
    '2019/12/04',
    'Alteration of Bilateral External Ear, Perc Endo Approach',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    14,
    56,
    '2020/04/16',
    'Revision of Synth Sub IN R Up Extrem, Extern Approach',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    23,
    2,
    '2019/08/26',
    'Supplement L Cephalic Vein w Nonaut Sub, Perc Endo',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    2,
    42,
    '2019/12/28',
    'Revision of Synth Sub IN L Up Extrem, Perc Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    28,
    14,
    '2020/02/10',
    'Revision of Other Device IN Upper Jaw, Percutaneous Approach',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    36,
    13,
    '2019/10/16',
    'Drainage of Right Inferior Parathyroid Gland, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    3,
    10,
    '2019/11/18',
    'MRI of Bi INT Carotid using Oth Contrast, Unenh, Enhance',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    1,
    18,
    '2020/04/18',
    'Revision of Drainage Device IN L Up Extrem, Open Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    29,
    59,
    '2019/06/04',
    'Fluoroscopy of R INT Mamm Graft using H Osm Contrast',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    25,
    3,
    '2019/08/23',
    'Restrict L Up Lobe Bronc w Intralum Dev, Perc Endo',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    1,
    31,
    '2019/10/15',
    'Remove Nonaut Sub FROM Sacrococcygeal Jt, Perc Endo',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    38,
    43,
    '2020/03/16',
    'Removal of Ext Fix FROM Skull, Extern Approach',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    16,
    '2019/09/02',
    'Excision of Left Large Intestine, Via Opening, Diagn',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    35,
    6,
    '2019/08/21',
    'Removal of Stim Lead FROM Low Muscle, Perc Endo Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    11,
    57,
    '2019/11/16',
    'Restriction of L Femor Vein with Intralum Dev, Perc Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    26,
    50,
    '2019/09/26',
    'Supplement L Low Arm Subcu/Fascia w Nonaut Sub, Perc',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    12,
    37,
    '2020/04/07',
    'Dilation of Right Peroneal Artery, Percutaneous Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    1,
    '2020/02/21',
    'Insertion of Infusion Device INTO Left Lung, Endo',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    23,
    17,
    '2019/09/09',
    'Removal of Nonaut Sub FROM Lumsac Disc, Perc Approach',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    19,
    49,
    '2020/02/08',
    'Reposition Right Foot Artery, Perc Endo Approach',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    25,
    '2019/12/23',
    'Release Right Ankle Tendon, Open Approach',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    9,
    28,
    '2020/04/07',
    'Drainage of Uterine Supporting Structure, Open Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    18,
    60,
    '2020/04/14',
    'Transfuse Nonaut Platelets IN Central Vein, Perc',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    27,
    '2020/03/01',
    'Insertion of Infusion Device INTO L Elbow, Perc Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    39,
    8,
    '2020/04/24',
    'Excision of Left Upper Eyelid, Open Approach, Diagnostic',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    28,
    58,
    '2020/01/19',
    'Destruction of Lower Esophagus, Via Opening',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    13,
    6,
    '2020/01/11',
    'Fusion of Right Toe Phalangeal Joint, Perc Endo Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    52,
    '2019/04/29',
    'Drainage of Ascending Colon, Percutaneous Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    3,
    13,
    '2019/09/30',
    'Bypass R INT Iliac Art to B Femor A w Synth Sub, Open',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    43,
    '2019/10/21',
    'Excision of Oculomotor Nerve, Percutaneous Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    3,
    29,
    '2019/07/15',
    'Extraction of Olfactory Nerve, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    15,
    12,
    '2020/04/05',
    'Drainage of Right Upper Arm Tendon, Perc Approach, Diagn',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    22,
    30,
    '2019/10/09',
    'Traction of Right Lower Extremity',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    11,
    49,
    '2019/12/23',
    'Insertion of INT Fix INTO L Tarsal, Perc Endo Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    31,
    22,
    '2020/03/13',
    'Reposition Glossopharyngeal Nerve, Perc Endo Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    24,
    41,
    '2020/01/07',
    'Dilate L Popl Art w Intralum Dev, Drug Blln, Open',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    17,
    '2019/12/12',
    'Supplement Right Breast with Autol Sub, Open Approach',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    18,
    13,
    '2019/08/17',
    'Removal of Drainage Device FROM L Tarsal Jt, Perc Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    15,
    '2019/06/29',
    'Revision of Drainage Device IN Upper Intestinal Tract, Endo',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    16,
    20,
    '2020/03/09',
    'Bypass L Axilla Art to R Low Leg Art w Synth Sub, Open',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    32,
    28,
    '2019/05/25',
    'Revision of Extraluminal Device IN Up Intest Tract, Endo',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    29,
    54,
    '2019/06/25',
    'Restrict of L Up Lobe Bronc with Extralum Dev, Perc Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    28,
    16,
    '2020/03/15',
    'Removal of Synth Sub FROM L Sacroiliac Jt, Perc Approach',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    37,
    '2019/11/07',
    'Supplement R Femor Vein with Autol Sub, Perc Endo Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    15,
    16,
    '2020/04/15',
    'Inspection of Right Tarsal Joint, Perc Endo Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    4,
    26,
    '2020/03/21',
    'Removal of INT Fix FROM C-thor Jt, Perc Approach',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    6,
    '2019/06/28',
    'Reposition Right Lacrimal Bone, Percutaneous Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    13,
    18,
    '2020/02/07',
    'Restrict of Ductus Arterio with Extralum Dev, Perc Approach',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    6,
    23,
    '2020/03/12',
    'Supplement R Fem Art with Nonaut Sub, Perc Endo Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    7,
    38,
    '2019/10/08',
    'Insertion of Ext Fix INTO L Metatarsotars Jt, Open Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    37,
    26,
    '2019/07/05',
    'HDR Brachytherapy of Spinal Cord using Iodine 125',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    28,
    24,
    '2019/08/12',
    'Removal of Synth Sub FROM L Humeral Shaft, Perc Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    11,
    47,
    '2019/11/19',
    'Replacement of R Popl Art with Synth Sub, Perc Endo Approach',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    21,
    19,
    '2020/02/08',
    'Extirpate of Matter FROM L Submaxillary Gland, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    4,
    43,
    '2020/04/01',
    'Replacement of R Femor Vein with Synth Sub, Open Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    3,
    3,
    '2019/09/07',
    'Supplement T-lum Jt with Synth Sub, Open Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    4,
    31,
    '2019/09/15',
    'Revision of Autol Sub IN L Knee Jt, Open Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    50,
    '2020/01/03',
    'Extirpation of Matter FROM Lumbosacral Joint, Perc Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    11,
    57,
    '2019/08/12',
    'Bypass R Basilic Vein to Up Vein w Autol Art, Open',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    32,
    15,
    '2019/07/03',
    'Replacement of Left Lacrimal Duct with Nonaut Sub, Endo',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    8,
    '2020/03/18',
    'CREATE Vagina IN Male Perineum w Nonaut Sub, Open',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    13,
    28,
    '2019/11/17',
    'Drainage of L Up Arm Subcu/Fascia, Open Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    16,
    '2019/05/19',
    'Drainage of Ileum, Percutaneous Endoscopic Approach',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    38,
    58,
    '2019/07/20',
    'Removal of Autol Sub FROM Low Muscle, Perc Endo Approach',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    37,
    39,
    '2020/02/02',
    'Extirpate matter FROM Face Art, Bifurc, Perc Endo',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    22,
    '2020/02/06',
    'Release Right Clavicle, Open Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    16,
    30,
    '2019/05/14',
    'Excision of Left Radial Artery, Perc Endo Approach, Diagn',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    15,
    24,
    '2020/04/19',
    'Monitoring of Arterial Saturation, Peripheral, Open Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    21,
    9,
    '2020/04/04',
    'Change Traction Apparatus ON Left Upper Arm',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    35,
    37,
    '2019/12/06',
    'Restriction of Right Vertebral Artery, Perc Endo Approach',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    25,
    7,
    '2020/01/09',
    'Drainage of R Sternoclav Jt, Perc Approach, Diagn',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    24,
    40,
    '2019/12/08',
    'Hyperthermia of Inguinal Lymphatics',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    1,
    60,
    '2019/05/21',
    'Transfer Left Upper Arm Muscle with Skin, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    33,
    54,
    '2020/02/24',
    'Removal of INT Fix FROM L Scapula, Extern Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    16,
    36,
    '2020/02/29',
    'Drainage of Left Peroneal Artery, Perc Approach, Diagn',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    58,
    '2019/07/25',
    'Drainage of Right Innominate Vein, Perc Endo Approach, Diagn',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    16,
    54,
    '2019/07/16',
    'Removal of Autol Sub FROM Lumsac Disc, Open Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    29,
    60,
    '2019/11/28',
    'Extirpation of Matter FROM Cecum, Endo',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    25,
    21,
    '2019/08/31',
    'Occlusion Esophagast Junct w Extralum Dev, Perc',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    37,
    60,
    '2019/08/23',
    'Replacement of L Radius with Autol Sub, Perc Endo Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    39,
    40,
    '2020/02/02',
    'Repair Left Atrium, Open Approach',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    1,
    33,
    '2019/06/09',
    'Bypass L Axilla Vein to Up Vein w Nonaut Sub, Perc Endo',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    16,
    '2019/06/26',
    'Revision of Autol Sub IN R Tibia, Extern Approach',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    29,
    25,
    '2019/08/15',
    'Repair Right Femoral Vein, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    16,
    53,
    '2019/10/02',
    'Muscle Perform Assess Integu Head, Neck w Assist Equip',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    3,
    58,
    '2019/11/01',
    'Supplement Left Ureter with Autol Sub, Endo',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    1,
    55,
    '2019/07/08',
    'Dilation of R Com Iliac Art with 2 Drug-elut, Open Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    35,
    34,
    '2019/06/09',
    'Occlusion of Right Lesser Saphenous Vein, Perc Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    21,
    45,
    '2020/03/17',
    'MRI of Prostate using Oth Contrast, Unenh, Enhance',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    36,
    59,
    '2020/03/12',
    'Drainage of Right Upper Lobe Bronchus, Endo',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    26,
    10,
    '2019/11/27',
    'Excision of Bilateral Lungs, Perc Endo Approach, Diagn',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    15,
    37,
    '2019/10/11',
    'Excision of Penis, External Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    11,
    4,
    '2019/11/30',
    'Revision of Autol Sub IN L Fibula, Perc Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    30,
    5,
    '2019/06/04',
    'Removal of Drainage Device FROM L Hip Jt, Perc Endo Approach',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    38,
    6,
    '2019/08/21',
    'Release Trigeminal Nerve, Percutaneous Endoscopic Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    19,
    5,
    '2019/09/20',
    'Fluoroscopy of Left Lower Extremity Veins',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    43,
    '2019/07/25',
    'Introduction of Recomb Protein C INTO Periph Vein, Open',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    50,
    '2020/01/27',
    'Revision of Autol Sub IN Fallopian Tube, Perc Endo Approach',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    40,
    36,
    '2019/09/03',
    'Excision of Left Lower Leg, Open Approach, Diagnostic',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    19,
    59,
    '2019/12/18',
    'Inspection of Left Lower Extremity, Perc Endo Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    4,
    30,
    '2019/11/23',
    'Bypass L Com Iliac Art to R Renal A w Autol Vn, Open',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    13,
    13,
    '2019/11/03',
    'Supplement Com Bile Duct with Synth Sub, Perc Endo Approach',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    37,
    19,
    '2020/04/18',
    'INSERT of Intralum Dev INTO L Less Saphenous, Open Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    4,
    50,
    '2020/02/20',
    'Drainage of Hypoglossal Nerve with Drain Dev, Open Approach',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    38,
    23,
    '2019/12/23',
    'Alteration of R Low Extrem with Synth Sub, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    6,
    54,
    '2019/10/23',
    'Restriction of Right Internal Mammary Artery, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    36,
    48,
    '2019/04/30',
    'Supplement L Up Leg Muscle with Autol Sub, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    32,
    1,
    '2019/05/18',
    'Supplement Vagina with Autologous Tissue Substitute, Endo',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    30,
    '2019/12/29',
    'Inspection of Right Upper Leg, Open Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    38,
    16,
    '2020/02/14',
    'Repair Right Finger Phalanx, Perc Endo Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    38,
    56,
    '2020/04/05',
    'Destruction of Left Toe Phalangeal Joint, Perc Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    24,
    58,
    '2020/02/01',
    'INSERT of Infusion Dev INTO R Brach Art, Perc Endo Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    17,
    '2019/05/26',
    'Removal of Drainage Device FROM Ovary, Open Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    14,
    39,
    '2020/01/14',
    'Beam Radiation of Pituitary Gland using Neutrons',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    37,
    15,
    '2019/10/10',
    'Removal of Nonaut Sub FROM Urethra, Via Opening',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    4,
    24,
    '2019/09/25',
    'Restriction of R Neck Lymph with Extralum Dev, Perc Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    23,
    25,
    '2019/12/20',
    'Replacement of Left Hip Joint with Nonaut Sub, Open Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    18,
    7,
    '2019/04/28',
    'Bypass Cecum to Ascending Colon, Perc Endo Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    50,
    '2019/11/08',
    'Removal of Nonautologous Tissue Substitute FROM L Eye, Endo',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    12,
    30,
    '2020/03/02',
    'Reposition Left Lacrimal Bone with INT Fix, Perc Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    1,
    28,
    '2020/03/15',
    'Drainage of R Metacarpophal Jt with Drain Dev, Perc Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    30,
    '2019/08/15',
    'Revision of Nonaut Sub IN Tracheobronc Tree, Endo',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    8,
    21,
    '2019/05/23',
    'Revision of Autol Sub IN Thor Vertebra, Perc Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    9,
    30,
    '2020/03/07',
    'Inspection of Scrotum & Tunica, Perc Endo Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    28,
    24,
    '2019/09/27',
    'Resection of Common Bile Duct, Perc Endo Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    3,
    28,
    '2019/06/11',
    'Inspection of Left Knee Region, Open Approach',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    21,
    20,
    '2019/09/24',
    'Measurement of Gastrointestinal Motility, Via Opening',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    38,
    '2020/04/23',
    'Revision of Nonaut Sub IN R Elbow Jt, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    16,
    31,
    '2019/06/25',
    'Supplement Cystic Duct with Autol Sub, Open Approach',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    37,
    30,
    '2019/09/27',
    'Planar Nuclear Medicine Imaging of Other Anatomical Region',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    2,
    '2019/07/28',
    'Excision of Prostate, Endo',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    25,
    45,
    '2019/12/13',
    'REPLACE Genitalia Skin w Autol Sub, FULL Thick, Extern',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    4,
    35,
    '2020/02/04',
    'Occlusion of Intracranial Artery, Perc Endo Approach',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    26,
    57,
    '2020/02/01',
    'Destruction of Thoracic Spinal Cord, Perc Endo Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    5,
    '2019/09/05',
    'Drainage of Left Hip Muscle, Perc Endo Approach, Diagn',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    18,
    12,
    '2020/04/18',
    'Repair Left Testis, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    22,
    6,
    '2019/07/24',
    'REPLACE of Low Tooth, Mult, with Nonaut Sub, Extern Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    32,
    16,
    '2019/10/10',
    'Drainage of Accessory Pancreatic Duct, Open Approach, Diagn',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    38,
    35,
    '2019/06/17',
    'Drain of R Low Arm & Wrist Tendon, Perc Endo Approach, Diagn',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    38,
    47,
    '2020/01/12',
    'Extirpation of Matter FROM Right Kidney, Open Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    5,
    22,
    '2020/02/04',
    'Bypass R INT Iliac Art to R INT Ilia w Nonaut Sub, Open',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    34,
    '2019/08/09',
    'Compression of Left Hand using Pressure Dressing',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    26,
    44,
    '2019/09/13',
    'Insertion of Neuro Lead INTO Azygos Vein, Perc Endo Approach',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    9,
    7,
    '2019/07/22',
    'Bypass R Atrium to R Pulm Art w Synth Sub, Perc Endo',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    45,
    '2019/09/23',
    'Release Sciatic Nerve, Percutaneous Approach',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    9,
    41,
    '2020/03/23',
    'Occlusion of L INT Carotid with Intralum Dev, Perc Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    32,
    33,
    '2019/10/19',
    'Fragmentation IN Bilateral Fallopian Tubes, Via Opening',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    31,
    29,
    '2019/07/17',
    'Drainage of Greater Omentum, Open Approach',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    36,
    '2020/01/23',
    'Repair Left Eustachian Tube, Percutaneous Approach',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    23,
    '2020/02/17',
    'Plain Radiography of Pelvic Arteries using Other Contrast',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    5,
    46,
    '2020/02/16',
    'Revision of Autol Sub IN L Radius, Perc Endo Approach',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    26,
    7,
    '2019/05/10',
    'Drainage of Tibial Nerve, Open Approach, Diagnostic',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    32,
    '2019/05/26',
    'Excision of Innominate Artery, Perc Endo Approach',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    6,
    60,
    '2020/02/27',
    'REPLACE L Low Arm Skin w Synth Sub, FULL Thick, Extern',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    30,
    49,
    '2020/03/02',
    'Restriction of Inf Mesent Art, Perc Endo Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    11,
    34,
    '2019/10/01',
    'REPLACE R Low Arm Subcu/Fascia w Autol Sub, Perc',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    30,
    35,
    '2019/06/16',
    'Replacement of R Femur Shaft with Nonaut Sub, Open Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    23,
    10,
    '2019/05/30',
    'Bypass Cereb Vent to Nasophar with Autol Sub, Perc Approach',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    37,
    22,
    '2019/06/09',
    'Extirpation of Matter FROM Liver, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    26,
    56,
    '2019/04/29',
    'Supplement Uterine Support Struct w Autol Sub, Open',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    24,
    31,
    '2020/01/25',
    'HDR Brachytherapy of Thymus using Cesium 137',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    37,
    59,
    '2020/04/06',
    'Plain Radiography of Left Ureter using Low Osmolar Contrast',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    15,
    32,
    '2019/11/13',
    'Revise Autol Sub IN Epididymis/Sperm Cord, Perc Endo',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    9,
    56,
    '2019/05/10',
    'Removal of Intbd Fus Dev FROM Occip Jt, Perc Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    7,
    54,
    '2019/11/01',
    'Restriction of L Axilla Art with Extralum Dev, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    13,
    '2020/01/30',
    'Repair Left Vocal Cord, Percutaneous Endoscopic Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    33,
    34,
    '2019/11/04',
    'Supplement R Up Arm Muscle with Synth Sub, Open Approach',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    2,
    28,
    '2019/12/17',
    'Excision of Right Lower Leg Muscle, Percutaneous Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    28,
    55,
    '2019/11/08',
    'Drainage of Parathyroid Gland, Perc Endo Approach, Diagn',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    19,
    8,
    '2019/11/24',
    'Bypass Inf Vena Cava to Inf Mesent V w Nonaut Sub, Open',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    31,
    30,
    '2019/09/22',
    'Removal of Monitoring Device FROM Trachea, Via Opening',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    14,
    '2019/11/30',
    'Excision of Right Hand Muscle, Perc Endo Approach, Diagn',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    5,
    11,
    '2020/02/02',
    'Irrigation of Resp Tract using Irrigat, Perc Approach, Diagn',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    37,
    39,
    '2020/04/08',
    'Restrict Trans Colon w Intralum Dev, Perc Endo',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    33,
    4,
    '2019/09/02',
    'Destruction of Left Shoulder Joint, Open Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    19,
    45,
    '2019/09/28',
    'Dilate L Colic Art, Bifurc, w 4 Drug-elut, Open',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    9,
    10,
    '2019/05/06',
    'Dilate R INT Mamm Art, Bifurc, w 2 Intralum Dev, Perc',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    6,
    29,
    '2020/02/15',
    'Dilation of L Ulnar Art with 2 Drug-elut, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    23,
    '2019/08/07',
    'Drainage of Left Nipple, External Approach, Diagnostic',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    21,
    13,
    '2020/01/28',
    'Short Increment Sensitivity Index Assessment',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    34,
    '2019/12/08',
    'Release Hymen, Percutaneous Endoscopic Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    22,
    14,
    '2020/04/08',
    'Beam Radiation of Duodenum using Photons <1 MeV',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    9,
    34,
    '2019/09/20',
    'Release Right Neck Muscle, Percutaneous Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    33,
    16,
    '2019/11/27',
    'Removal of INT Fix FROM L Elbow Jt, Perc Endo Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    38,
    1,
    '2019/07/13',
    'Bypass L Ureter to Colocutan with Autol Sub, Open Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    18,
    23,
    '2020/02/13',
    'Reposition Right Mandible, Open Approach',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    32,
    15,
    '2019/05/06',
    'Computerized Tomography (CT Scan) of Chest and Abdomen',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    14,
    48,
    '2020/01/24',
    'Removal of Nonaut Sub FROM Kidney, Via Opening',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    19,
    38,
    '2020/03/04',
    'Removal of Pressure Dressing ON Right Upper Arm',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    8,
    14,
    '2019/12/05',
    'Drainage of Pericardial Cavity, Perc Endo Approach, Diagn',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    23,
    5,
    '2020/04/19',
    'Reposition R Tibia with Monopln Ext Fix, Perc Endo Approach',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    6,
    15,
    '2019/11/28',
    'Release Right Zygomatic Bone, Open Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    3,
    52,
    '2019/05/03',
    'Supplement Hard Palate with Autol Sub, Perc Approach',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    8,
    10,
    '2019/09/05',
    'Replacement of L Carpal Jt with Synth Sub, Open Approach',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    25,
    '2019/12/01',
    'Destruction of Accessory Sinus, Open Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    16,
    37,
    '2019/08/27',
    'Supplement R Abd Bursa/Lig w Autol Sub, Perc Endo',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    4,
    34,
    '2019/04/27',
    'Transfer Hypoglossal Nerve to Accessory Nerve, Open Approach',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    33,
    49,
    '2019/08/28',
    'Revision of Nonaut Sub IN T-lum Disc, Perc Endo Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    40,
    52,
    '2020/04/21',
    'Insertion of Infusion Device INTO R Up Leg, Open Approach',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    1,
    17,
    '2019/10/22',
    'Transfuse Nonaut Bone Marrow IN Periph Art, Open',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    23,
    18,
    '2019/08/28',
    'Drainage of Pudendal Nerve with Drain Dev, Open Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    39,
    23,
    '2019/12/24',
    'Insertion of Radioact Elem INTO Abd Wall, Perc Endo Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    26,
    24,
    '2019/07/25',
    'Control Bleeding IN Left Lower Extremity, Perc Endo Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    30,
    '2019/07/27',
    'Repair Left Submaxillary Gland, Open Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    35,
    16,
    '2019/04/26',
    'Contact Radiation of Adrenal Glands',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    3,
    57,
    '2019/08/03',
    'Repair Pancreatic Duct, Endo',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    21,
    43,
    '2020/01/08',
    'Drainage of Right Colic Artery, Percutaneous Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    3,
    6,
    '2019/11/05',
    'Male Reproductive System, Reposition',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    12,
    25,
    '2019/12/09',
    'Fluoroscopy Mult Cor Art w Oth Contrast, Laser Intraop',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    28,
    10,
    '2019/08/04',
    'Removal of Synthetic Substitute FROM R Fibula, Perc Approach',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    11,
    43,
    '2020/02/11',
    'Restrict of R INT Mamm Art with Intralum Dev, Perc Approach',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    31,
    9,
    '2019/09/18',
    'Extirpation of Matter FROM Left Ankle Joint, Perc Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    13,
    '2020/01/23',
    'Excision of Left Lesser Saphenous Vein, Open Approach, Diagn',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    6,
    13,
    '2019/09/06',
    'Replacement of L Hand Tendon with Autol Sub, Open Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    33,
    15,
    '2019/06/20',
    'Supplement Right Knee Tendon with Autol Sub, Open Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    39,
    '2019/11/16',
    'Removal of Synth Sub FROM Hepatobil Duct, Via Opening',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    7,
    51,
    '2019/10/02',
    'Removal of Autol Sub FROM Ureter, Via Opening',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    29,
    45,
    '2019/04/28',
    'Revision of Bone Stim IN Facial Bone, Perc Endo Approach',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    22,
    40,
    '2019/12/14',
    'Resection of Duodenum, Endo',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    40,
    14,
    '2019/12/24',
    'Alteration of Left Upper Leg with Autol Sub, Open Approach',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    31,
    54,
    '2019/09/21',
    'Dilation of Larynx, Open Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    5,
    32,
    '2020/01/20',
    'Bypass Abdominal Aorta to R Com Ilia, Perc Endo Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    15,
    35,
    '2019/07/13',
    'Dix-Hallpike Dynamic Assessment',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    24,
    49,
    '2019/11/14',
    'Introduction of Nutritional INTO Fem Reprod, Via Opening',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    11,
    8,
    '2019/08/07',
    'Reposition Nasal Bone, Percutaneous Endoscopic Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    12,
    '2020/04/16',
    'CT Scan of Bi Pelvic Vein using H Osm Contrast',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    38,
    31,
    '2019/07/12',
    'Dilate R Com Iliac Vein w Intralum Dev, Perc Endo',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    57,
    '2019/09/04',
    'Fluoroscopy of R Temporomand Jt using L Osm Contrast',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    17,
    '2019/10/14',
    'Plain Radiography of Bladder and Urethra',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    6,
    60,
    '2019/11/25',
    'Insertion of Ext Fix INTO L Metacarpal, Perc Endo Approach',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    9,
    '2020/02/20',
    'Supplement R Index Finger w Nonaut Sub, Perc Endo',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    21,
    11,
    '2019/07/24',
    'Revision of INT Fix IN Occip Jt, Perc Endo Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    6,
    13,
    '2019/12/07',
    'Excision of Left External Carotid Artery, Open Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    38,
    41,
    '2019/09/05',
    'Reposition R Sphenoid Bone with INT Fix, Perc Endo Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    21,
    36,
    '2020/01/30',
    'Removal of Synth Sub FROM C-thor Disc, Perc Endo Approach',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    21,
    33,
    '2019/10/26',
    'Excision of Pelvis Lymphatic, Open Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    9,
    47,
    '2020/02/21',
    'Bypass L INT Iliac Art to R Ext Ilia w Autol Vn, Open',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    39,
    21,
    '2019/10/03',
    'Insertion of Infusion Device INTO Genitourinary Tract, Endo',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    28,
    3,
    '2019/07/22',
    'Remove Nonaut Sub FROM Uterus & Cervix, Perc Endo',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    7,
    '2020/01/11',
    'Plain Radiography of Dialysis Shunt using Oth Contrast',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    29,
    24,
    '2019/05/23',
    'Peripheral Nervous System, Transfer',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    14,
    20,
    '2019/05/17',
    'Repair Left Upper Extremity, External Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    30,
    14,
    '2019/10/28',
    'GROUP Counseling for Substance Abuse Treatment, 12-Step',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    25,
    47,
    '2020/01/25',
    'Change Bandage ON Left Upper Leg',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    60,
    '2020/01/02',
    'Reposition Right Mandible with INT Fix, Perc Endo Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    30,
    17,
    '2019/09/19',
    'Resection of Left Sphenoid Sinus, Open Approach',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    29,
    26,
    '2019/08/16',
    'Drainage of Left Lower Leg Muscle, Percutaneous Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    32,
    33,
    '2019/08/04',
    'Release Left Internal Iliac Artery, Perc Endo Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    26,
    32,
    '2019/09/18',
    'Extirpate of Matter FROM R Axilla Art, Bifurc, Perc Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    26,
    50,
    '2019/10/12',
    'Restriction of Cecum, Percutaneous Endoscopic Approach',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    26,
    '2020/02/29',
    'Replacement of R Up Arm Skin with Synth Sub, Extern Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    30,
    9,
    '2019/07/04',
    'Dilate R Temporal Art, Bifurc, w 3 Drug-elut, Open',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    13,
    56,
    '2019/06/17',
    'Removal of Extraluminal Device FROM L Eye, Extern Approach',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    1,
    14,
    '2019/11/29',
    'Removal of Synthetic Substitute FROM L Hip Jt, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    12,
    47,
    '2019/10/23',
    'Reattachment of Abdomen Skin, External Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    5,
    22,
    '2019/07/02',
    'Drain of Inf Mesent Art with Drain Dev, Perc Endo Approach',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    26,
    3,
    '2019/11/30',
    'Excision of Right Upper Femur, Perc Endo Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    3,
    32,
    '2019/08/05',
    'Removal of Drainage Device FROM Left Breast, Via Opening',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    4,
    '2019/08/30',
    'Supplement L Main Bronc with Nonaut Sub, Perc Endo Approach',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    6,
    3,
    '2020/01/04',
    'INSERT of Artif Sphincter INTO Bladder, Perc Endo Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    15,
    40,
    '2019/06/02',
    'Low Dose Rate (LDR) Brachytherapy of Chest using Cesium 137',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    24,
    37,
    '2019/10/13',
    'Repair Right Scapula, Percutaneous Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    52,
    '2019/06/27',
    'Transfer Left Abdomen Muscle with Skin, Subcu, Open Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    7,
    18,
    '2019/08/12',
    'Insertion of Infusion Device INTO Left Foot, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    1,
    9,
    '2019/12/15',
    'Occlusion of Vagina, Endo',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    27,
    '2019/06/01',
    'Excision of Left Brachial Artery, Percutaneous Approach',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    16,
    28,
    '2020/03/12',
    'Revision of Infusion Device IN Penis, Percutaneous Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    8,
    14,
    '2020/02/15',
    'Control Bleeding IN R Inguinal Region, Perc Endo Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    16,
    13,
    '2019/10/22',
    'Fluoroscopy of Bile Ducts using High Osmolar Contrast',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    22,
    53,
    '2019/08/19',
    'MRI of Pelvic Art using Oth Contrast, Unenh, Enhance',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    31,
    60,
    '2019/05/14',
    'Repair Supernumerary Breast, Via Opening',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    49,
    '2019/09/20',
    'Beam Radiation of Duodenum using Electrons',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    13,
    '2019/10/31',
    'REPLACE of R Great Saphenous with Autol Sub, Open Approach',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    28,
    60,
    '2020/04/05',
    'Drainage of Ampulla of Vater with Drain Dev, Via Opening',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    37,
    44,
    '2019/07/24',
    'Supplement Left Thorax Muscle with Nonaut Sub, Open Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    22,
    '2019/04/26',
    'Bypass Superior Vena Cava to R Pulm Art, Perc Endo Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    9,
    47,
    '2019/12/06',
    'Excision of Femoral Nerve, Open Approach',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    3,
    25,
    '2019/10/19',
    'Dilate L Post Tib Art, Bifurc, w Intralum Dev, Perc',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    25,
    44,
    '2020/03/07',
    'Introduction of Local Anesth INTO Pleural Cav, Perc Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    28,
    38,
    '2019/08/10',
    'Excision of Right Ovary, Via Natural OR Artificial Opening',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    25,
    16,
    '2020/02/15',
    'Drainage of Hymen with Drainage Device, Perc Endo Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    3,
    50,
    '2019/08/24',
    'Removal of INT Fix FROM L Wrist Jt, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    49,
    '2019/07/05',
    'Supplement Left Retina with Autol Sub, Perc Approach',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    9,
    6,
    '2019/05/29',
    'ROM/Jt Integrity Assess Integu Low Back/LE w Oth Equip',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    21,
    52,
    '2019/10/11',
    'Repair Left Brachial Artery, Open Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    6,
    22,
    '2020/04/22',
    'REPLACE R Low Leg Subcu/Fascia w Nonaut Sub, Perc',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    1,
    31,
    '2019/11/14',
    'Revision of Drain Dev IN Retroperitoneum, Extern Approach',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    12,
    37,
    '2019/09/11',
    'Drain L Low Extrem Bursa/Lig w Drain Dev, Open',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    37,
    36,
    '2020/03/28',
    'Occlusion of Left Ulnar Artery, Perc Endo Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    55,
    '2020/01/31',
    'Excision of Lung Lingula, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    37,
    '2019/05/08',
    'Reposition Left Upper Lung Lobe, Open Approach',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    13,
    13,
    '2020/04/10',
    'Repair Left Eustachian Tube, External Approach',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    16,
    48,
    '2020/02/07',
    'Revision of Drainage Device IN Toe Nail, External Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    24,
    50,
    '2019/09/10',
    'Removal of Autol Sub FROM R Eye, Via Opening',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    19,
    44,
    '2019/08/16',
    'Removal of Synth Sub FROM Periton Cav, Perc Approach',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    33,
    43,
    '2020/01/05',
    'Release Right External Auditory Canal, Perc Endo Approach',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    36,
    5,
    '2019/05/21',
    'Resection of Right Neck Muscle, Open Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    26,
    56,
    '2019/06/21',
    'Inspection of Products of Conception, Percutaneous Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    25,
    10,
    '2019/09/27',
    'Control Bleeding IN Left Foot, Perc Endo Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    14,
    55,
    '2019/06/16',
    'Introduction of Destr Agent INTO Mouth/Phar, Perc Approach',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    18,
    51,
    '2019/11/11',
    'Dilation of Celiac Artery with 2 Drug-elut, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    33,
    33,
    '2019/07/20',
    'Dilate L Colic Art, Bifurc, w Intralum Dev, Perc Endo',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    24,
    32,
    '2020/03/03',
    'Measurement of Cardiac Defibrillator, External Approach',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    16,
    '2019/09/10',
    'Drainage of Left Elbow Joint, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    52,
    '2020/01/08',
    'Drainage of Ileocecal Valve, Percutaneous Approach, Diagn',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    32,
    58,
    '2020/03/28',
    'Restriction of R Pulm Art with Extralum Dev, Perc Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    28,
    13,
    '2019/07/20',
    'Fluoroscopy of Duodenum',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    14,
    20,
    '2020/02/29',
    'Drainage of Stomach with Drainage Device, Perc Endo Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    35,
    43,
    '2019/09/16',
    'Supplement ASC Colon with Autol Sub, Perc Endo Approach',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    16,
    43,
    '2019/09/09',
    'Release Left Lobe Liver, Open Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    23,
    59,
    '2019/08/06',
    'Release Left Hip Bursa and Ligament, Percutaneous Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    30,
    17,
    '2020/04/12',
    'Repair Right Pelvic Bone, Percutaneous Endoscopic Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    22,
    45,
    '2019/07/18',
    'Revision of Synth Sub IN Lum Vertebra, Open Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    40,
    '2019/12/18',
    'Insertion of Radioact Elem INTO R Knee, Perc Endo Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    28,
    60,
    '2019/05/21',
    'Release Bilateral Adrenal Glands, Open Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    34,
    12,
    '2019/06/13',
    'Bypass R INT Iliac Art to L INT Ilia w Autol Art, Perc Endo',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    22,
    22,
    '2019/12/15',
    'Excision of Perineum Bursa/Lig, Open Approach, Diagn',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    20,
    '2019/07/30',
    'REPLACE L Ear Skin w Nonaut Sub, FULL Thick, Extern',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    35,
    15,
    '2019/11/08',
    'Contact Radiation of Abdomen Skin',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    16,
    27,
    '2019/11/29',
    'Excision of Inferior Mesenteric Artery, Perc Approach',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    13,
    12,
    '2020/03/13',
    'Supplement R Com Iliac Vein with Autol Sub, Perc Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    36,
    13,
    '2019/12/21',
    'Introduction of Other Gas INTO Epidural Space, Via Opening',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    25,
    '2020/01/10',
    'Drainage of L Ext Carotid with Drain Dev, Perc Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    37,
    13,
    '2019/11/02',
    'Occlusion of L Sperm Cord with Intralum Dev, Perc Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    3,
    11,
    '2019/07/20',
    'Removal of Drainage Device FROM Diaphragm, Endo',
    'Puce'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    24,
    8,
    '2019/10/06',
    'Fluoroscopy of Bile Ducts using Other Contrast',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    21,
    47,
    '2020/02/06',
    'Revision of Infusion Dev IN Lumsac Disc, Perc Endo Approach',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    38,
    '2020/04/14',
    'Extirpate of Matter FROM R Radial Art, Bifurc, Open Approach',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    3,
    47,
    '2020/02/11',
    'ROM & Jt Mobility Treatment of Integu Body using Orthosis',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    33,
    45,
    '2019/07/06',
    'Repair Right Mastoid Sinus, Percutaneous Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    35,
    34,
    '2019/08/31',
    'Dilate R Popl Art, Bifurc, w 4 Drug-elut, Perc',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    19,
    11,
    '2019/12/23',
    'Excision of Right Lower Lung Lobe, Percutaneous Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    40,
    27,
    '2020/03/09',
    'Replacement of R Maxilla with Nonaut Sub, Perc Endo Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    23,
    14,
    '2019/12/27',
    'Integumentary Integrity Assessment of Integu Low Back/LE',
    'Violet'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    50,
    '2019/07/26',
    'Dilate L Axilla Art w 2 Intralum Dev, Perc Endo',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    7,
    25,
    '2019/10/21',
    'Insertion of Radioactive Element INTO Rectum, Via Opening',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    29,
    16,
    '2020/02/24',
    'Drainage of INT Mamm, L Lymph with Drain Dev, Open Approach',
    'Crimson'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    7,
    54,
    '2019/10/26',
    'Mental Health, Narcosynthesis',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    13,
    37,
    '2020/04/13',
    'Resection of Cecum, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    45,
    '2019/11/15',
    'Excision of Right Hand Vein, Open Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    11,
    57,
    '2019/07/24',
    'Destruction of Left Colic Artery, Perc Endo Approach',
    'Aquamarine'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    15,
    2,
    '2020/01/15',
    'Supplement R Knee Bursa/Lig w Nonaut Sub, Perc Endo',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    18,
    22,
    '2020/04/19',
    'Extraction of Median Nerve, Percutaneous Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    22,
    35,
    '2019/09/06',
    'Insertion of INT Fix INTO R Mandible, Open Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    11,
    21,
    '2019/05/19',
    'Revision of Autol Sub IN L Sacroiliac Jt, Perc Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    28,
    30,
    '2020/01/16',
    'Insertion of Hearing Device INTO L INNER Ear, Perc Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    5,
    58,
    '2019/12/07',
    'Drainage of Upper Back with Drain Dev, Perc Endo Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    2,
    24,
    '2019/10/04',
    'Destruction of Thyroid Gland, Perc Endo Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    37,
    59,
    '2019/06/22',
    'Speech/Lang Screen Assessment using Augment Comm Equipment',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    17,
    28,
    '2020/03/01',
    'Fluoroscopy of Left Heart using High Osmolar Contrast',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    26,
    10,
    '2019/10/11',
    'Inspection of Larynx, Percutaneous Approach',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    13,
    22,
    '2019/11/14',
    'Reposition Left Fibula with Monopln Ext Fix, Perc Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    24,
    48,
    '2020/03/05',
    'Drainage of Right Nipple, Endo',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    4,
    '2020/03/30',
    'Inspection of Right Lung, Endo',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    33,
    54,
    '2019/06/14',
    'Removal of Nonaut Sub FROM Occip Jt, Perc Approach',
    'Turquoise'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    32,
    30,
    '2020/04/22',
    'Measurement of Respiratory Total Activity, External Approach',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    24,
    13,
    '2019/12/01',
    'Restriction of Right Foot Artery, Percutaneous Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    38,
    43,
    '2019/07/01',
    'Introduction of Liquid Brachy INTO Resp Tract, Via Opening',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    25,
    48,
    '2019/12/17',
    'Repair Left Index Finger, Open Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    22,
    10,
    '2019/11/04',
    'Replacement of R Brach Art with Nonaut Sub, Open Approach',
    'Yellow'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    35,
    51,
    '2020/04/03',
    'Drainage of Ileocecal Valve, Perc Endo Approach, Diagn',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    14,
    58,
    '2019/08/21',
    'Replacement of Lumbar Vertebra with Synth Sub, Perc Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    29,
    47,
    '2020/03/19',
    'Repair Portal Vein, Percutaneous Endoscopic Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    4,
    26,
    '2019/12/21',
    'Bypass Gastric Vein to Low Vein w Synth Sub, Perc Endo',
    'Goldenrod'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    20,
    23,
    '2019/11/10',
    'Radiation Therapy, Lymph & Hemat Sys, Oth Radiation',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    29,
    32,
    '2019/08/02',
    'Revision of Drain Dev IN R Metatarsotars Jt, Extern Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    32,
    48,
    '2019/09/20',
    'Revise of Monitor Dev IN Endocrine Gland, Perc Endo Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    33,
    34,
    '2019/08/10',
    'Excision of Adenoids, Open Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    10,
    38,
    '2019/08/20',
    'REPLACE of R Metatarsophal Jt with Nonaut Sub, Open Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    27,
    7,
    '2020/04/02',
    'Replacement of Left Nipple with Autol Sub, Perc Approach',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    23,
    12,
    '2019/06/27',
    'Extraction of Products of Conception, Other, Via Opening',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    7,
    52,
    '2020/02/24',
    'Introduce of Oxazolidinones INTO Central Vein, Open Approach',
    'Green'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    32,
    31,
    '2019/05/25',
    'Bypass Right Kidney Pelvis to L Ureter, Perc Endo Approach',
    'Orange'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    22,
    54,
    '2019/08/18',
    'Destruction of Lower Esophagus, Endo',
    'Khaki'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    2,
    26,
    '2019/09/12',
    'Insertion of INT Fix INTO L Fibula, Open Approach',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    13,
    55,
    '2019/05/26',
    'Supplement Buccal Mucosa with Nonaut Sub, Extern Approach',
    'Blue'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    40,
    6,
    '2019/05/18',
    'Imaging, Female Reproductive System, Plain Radiography',
    'Elettronica'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    35,
    59,
    '2019/05/16',
    'Resection of Left Carpal Joint, Open Approach',
    'Maroon'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    15,
    28,
    '2019/05/20',
    'Reattachment of Left Main Bronchus, Open Approach',
    'Red'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    36,
    23,
    '2019/10/18',
    'Drainage of L Sacroiliac Jt with Drain Dev, Perc Approach',
    'Fuscia'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    3,
    45,
    '2019/11/30',
    'Removal of Nonaut Sub FROM L Metacarpocarp Jt, Open Approach',
    'Purple'
  );
INSERT INTO esperimento (
    codicedisp,
    matricola,
    data,
    descrizione,
    categoria
  )
VALUES
  (
    35,
    40,
    '2019/05/19',
    'Extirpation of Matter FROM Mesentery, Perc Endo Approach',
    'Blue'
  );