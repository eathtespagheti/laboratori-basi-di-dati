DELETE FROM corsi
WHERE
  codc IN (
    SELECT
      codc
    FROM programma
    GROUP BY
      (codc)
    HAVING
      COUNT(giorno) > 1
  );
DELETE FROM istruttore
WHERE
  codfisc LIKE 'SMTPLA80N31B791Z'