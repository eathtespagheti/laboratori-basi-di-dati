INSERT into ISTRUTTORE(
    CodFisc,
    Nome,
    Cognome,
    DataNascita,
    Email,
    Telefono
  )
values
  (
    'SMTPLA80N31B791Z',
    'Paul',
    'Smith',
    TO_DATE('1980/12/31', 'YYYY/MM/DD'),
    'p.smith@email.it',
    NULL
  ),
  (
    'KHNJHN81E30C455Y',
    'John',
    'Johnson',
    TO_DATE('1981/05/30', 'YYYY/MM/DD'),
    'j.johnson@email.it',
    '+2300110303444'
  ),
  (
    'AAAGGG83E30C445A',
    'Peter',
    'Johnson',
    TO_DATE('1980/12/31', 'YYYY/MM/DD'),
    'p.johnson@email.it',
    '+2300110303444'
  );
INSERT INTO CORSI(CodC, Nome, Tipo, Livello)
values
  ('CT100', 'Spinning principianti', 'Spinning', 1),
  (
    'CT101',
    'Ginnastica e musica',
    'Attività musicale',
    2
  ),
  (
    'CT104',
    'Spinning profesisonisti',
    'Spinning',
    4
  );
INSERT into PROGRAMMA (
    CodFisc,
    Giorno,
    OraInizio,
    Durata,
    CodC,
    Sala
  )
values
  (
    'SMTPLA80N31B791Z',
    'Lunedì',
    '10:00',
    '45',
    'CT100',
    'S1'
  ),
  (
    'SMTPLA80N31B791Z',
    'Martedì',
    '11:00',
    '45',
    'CT100',
    'S1'
  ),
  (
    'SMTPLA80N31B791Z',
    'Martedì',
    '15:00',
    '45',
    'CT100',
    'S2'
  ),
  (
    'KHNJHN81E30C455Y',
    'Lunedì',
    '10:00',
    '30',
    'CT101',
    'S2'
  ),
  (
    'KHNJHN81E30C455Y',
    'Lunedì',
    '11:30',
    '30',
    'CT104',
    'S2'
  ),
  (
    'KHNJHN81E30C455Y',
    'Mercoledì',
    '9:00',
    '60',
    'CT104',
    'S1'
  );