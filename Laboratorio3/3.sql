SELECT
  companyid
FROM companydel
WHERE
  numcollections >= 0
  OR numdeliveries >= 0
GROUP BY
  companyid
HAVING
  (
    COUNT(delivererid) * 100 / (
      SELECT
        COUNT(delivererid)
      FROM deliverers
    )
  ) > 30