SELECT
  delivererid,
  min(data) AS "Prima",
  MAX(data) AS "Ultima"
FROM penalties
GROUP BY
  delivererid
HAVING
  COUNT(paymentid) >= 2