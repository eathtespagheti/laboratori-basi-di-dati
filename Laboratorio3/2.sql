SELECT
  penalties.delivererid,
  amount,
  penalties.data
FROM penalties
JOIN (
    SELECT
      delivererid,
      MAX(data) AS data
    FROM penalties
    GROUP BY
      delivererid
  ) AS max_values ON penalties.delivererid = max_values.delivererid
  AND penalties.data = max_values.data