SELECT
  delivererid,
  SUM(amount) AS total
FROM penalties
WHERE
  delivererid IN (
    SELECT
      delivererid
    FROM deliverers
    WHERE
      town = 'Inglewood'
  )
GROUP BY
  delivererid
HAVING
  COUNT(delivererid) > 1