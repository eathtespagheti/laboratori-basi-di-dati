SELECT
  delivererid
FROM DELIVERERS
WHERE
  year_of_birth > 1962
  AND delivererid IN (
    SELECT
      companydel.delivererid
    FROM companydel
    JOIN companies ON companydel.companyid = companies.companyid
    WHERE
      companies.mandate = 'first'
  )
ORDER BY
  delivererid DESC