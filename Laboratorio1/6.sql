SELECT
  name,
  initials
FROM deliverers
WHERE
  delivererid IN (
    SELECT
      companies.delivererid
    FROM penalties
    JOIN companies ON penalties.delivererid = companies.delivererid
    WHERE
      penalties.data > CAST('1980-12-31' AS DATE)
  )
ORDER BY
  name