SELECT
  SUM(deliveries) AS "total deliveries",
  SUM(collections) AS "toal collections"
FROM deliverers
JOIN (
    SELECT
      delivererid,
      SUM(numdeliveries) AS deliveries,
      SUM(numcollections) AS collections
    FROM companydel
    GROUP BY
      delivererid
  ) AS sq ON deliverers.delivererid = sq.delivererid
WHERE
  town != 'Stratford'
  AND name LIKE 'B%'