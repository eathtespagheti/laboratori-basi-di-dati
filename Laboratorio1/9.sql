SELECT
  name
FROM deliverers
WHERE
  delivererid IN (
    SELECT
      delivererid
    FROM companydel
    GROUP BY
      delivererid
    HAVING
      COUNT(delivererid) > 1
  )
  AND (
    town = 'Inglewood'
    OR town = 'Stratford'
  )