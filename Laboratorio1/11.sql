SELECT
  name,
  min
FROM deliverers
JOIN (
    SELECT
      delivererid,
      MIN(amount) AS min
    FROM penalties
    GROUP BY
      delivererid
    HAVING
      COUNT(delivererid) >= 2
      AND COUNT(delivererid) <= 4
  ) AS sq ON deliverers.delivererid = sq.delivererid