SELECT
  subquery.companyid,
  deliverers.delivererid
FROM deliverers
JOIN (
    SELECT
      companyid,
      delivererid
    FROM companydel
    WHERE
      numcollections > 1
      AND numdeliveries > 0
  ) AS subquery ON deliverers.delivererid = subquery.delivererid
WHERE
  town = 'Stratford'