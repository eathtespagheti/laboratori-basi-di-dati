#!/usr/bin/env sh

keywords="AND ANY AS ASC AUTO_INCREMENT"
keywords="$keywords BACKUP BETWEEN BY"
keywords="$keywords CASCADE CASE CAST CHAR CHECK COUNT CREATE"
keywords="$keywords DATE DATETIME DELETE DESC DISTINCT DROP"
keywords="$keywords EXCEPT EXEC EXISTS"
keywords="$keywords FOREIGN FROM FULL"
keywords="$keywords GROUP"
keywords="$keywords HAVING"
keywords="$keywords IF IN INNER INSERT INT INTO INTERSECT INTERVAL IS"
keywords="$keywords JOIN"
keywords="$keywords KEY"
keywords="$keywords LEFT LIKE LIMIT"
keywords="$keywords MAX"
keywords="$keywords NOT NULL"
keywords="$keywords ON OR ORDER OUTER"
keywords="$keywords PRIMARY"
keywords="$keywords REFERENCES REPLACE RIGHT ROWNUM"
keywords="$keywords SELECT SET SMALLINT SUM"
keywords="$keywords TABLE TIME TIMESTAMP TRUNCATE"
keywords="$keywords UNION UPDATE"
keywords="$keywords VALUES VARCHAR"
keywords="$keywords WHERE"
sedCommand=""
changeIn="uppercase"
paths=""

# Parse input parameters
for parameter in "$@"; do
    case "$parameter" in
    --uppercase)
        changeIn="uppercase"
        ;;
    --lowercase)
        changeIn="lowercase"
        ;;
    *)
        paths="$paths$parameter "
        ;;
    esac
done

# Check if lowercase it's required
[ "$changeIn" = "lowercase" ] && {
    keywords="$(echo "$keywords" | tr ' ' '\n' | awk '{print tolower($1)}' | tr '\n' ' ')"
}

# Compose sed command
for word in $keywords; do
    sedCommand="${sedCommand}s|\<$word\>|$word|gi;"
done

# For every path suggested
for path in $paths; do
    # If it's a single file
    [ -f "$path" ] && {
        eval sed -i "'$sedCommand'" "$path"
    } || [ -d "$path" ] && { # If it's a folder
        filesList="$(find "$path" -iname "*.SQL" | sed 's|^\./||' | tr '\n' ' ')"
        [ -n "$filesList" ] && {
            for file in $filesList; do
                eval sed -i "'$sedCommand'" "$file"
            done
        }
    }
done
