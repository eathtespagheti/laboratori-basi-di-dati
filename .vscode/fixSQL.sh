#!/usr/bin/env sh

# convert syntax
extension="sql"
outFile="${1%.$extension}_out.$extension"

# Convert SQL
sqlines -s=oracle -t=postgresql -log=/dev/null -in="$1"
# fix drop table
sed -i 's/drop[[:space:]]table/DROP TABLE IF EXISTS/g' "$outFile"
# if the newfile parameter exist
[ "$2" != "--replace" ] && (mv "$outFile" "$3/${2%.$extension}.$extension") || (rm "$1" && mv "$outFile" "$1")
