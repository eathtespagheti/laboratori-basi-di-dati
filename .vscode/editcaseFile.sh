#!/usr/bin/env sh

changeIn="toupper"
# Parse input parameters
for parameter in "$@"; do
    case "$parameter" in
    --toupper)
        changeIn="toupper"
        ;;
    --tolower)
        changeIn="tolower"
        ;;
    *)
        paths="$paths$parameter "
        ;;
    esac
done

# For every path suggested
for path in $paths; do
    # If it's a single file
    [ -f "$path" ] && {
        awk "{print ${changeIn}(\$0)}" "$path" >"$path.tmp"
        mv "$path.tmp" "$path"
    } || [ -d "$path" ] && { # If it's a folder
        filesList="$(find "$path" -iname "*.txt" | sed 's|^\./||' | tr '\n' ' ')"
        [ -n "$filesList" ] && {
            for file in $filesList; do
                awk "{print ${changeIn}(\$0)}" "$file" >"$file.tmp"
                mv "$file.tmp" "$file"
            done
        }
    }
done
