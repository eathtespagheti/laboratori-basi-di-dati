-- 1.A
SELECT
  data,
  nome,
  cognome,
  COUNT(codguida) AS "Number of Guides in that day",
  SUM(durata)
FROM tipo_visita
JOIN (
    SELECT
      guide_valide.*,
      DATE(visita_guidata_effettuata.data) AS data,
      visita_guidata_effettuata.codtipovisita AS codtipovisita
    FROM visita_guidata_effettuata
    RIGHT OUTER JOIN (
        SELECT
          codguida,
          nome,
          cognome
        FROM guida
        WHERE
          codguida NOT IN (
            SELECT
              codguida
            FROM visita_guidata_effettuata
            JOIN gruppo ON visita_guidata_effettuata.codgr = gruppo.codgr
            WHERE
              lingua LIKE 'French%'
          )
      ) AS guide_valide ON visita_guidata_effettuata.codguida = guide_valide.codguida
  ) AS bigtable ON tipo_visita.codtipovisita = bigtable.codtipovisita
GROUP BY
  (data, codguida, nome, cognome)
ORDER BY
  (data);