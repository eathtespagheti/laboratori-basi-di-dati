-- 2.A
SELECT
  dispositivo.nomedisp,
  laboratorio.nomelab,
  studente.nome,
  studente.cognome,
  COUNT(esperimento.data) AS "Utilizzi"
FROM esperimento
JOIN studente ON esperimento.matricola = studente.matricola
JOIN dispositivo ON dispositivo.codicedisp = esperimento.codicedisp
JOIN laboratorio ON dispositivo.codlab = laboratorio.codlab
WHERE
  dispositivo.codicedisp IN (
    SELECT
      codicedisp
    FROM esperimento
    GROUP BY
      codicedisp
    HAVING
      COUNT(codicedisp) >= 10
  )
GROUP BY
  dispositivo.codicedisp,
  laboratorio.codlab,
  studente.matricola
ORDER BY
  COUNT(esperimento.data) DESC;