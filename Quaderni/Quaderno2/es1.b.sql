-- 1.B
SELECT
  tipo_visita.monumento,
  SUM(gruppo.numeropartecipanti)
FROM tipo_visita
JOIN visita_guidata_effettuata ON tipo_visita.codtipovisita = visita_guidata_effettuata.codtipovisita
JOIN gruppo ON gruppo.codgr = visita_guidata_effettuata.codgr
GROUP BY
  (monumento)
having
  (
    COUNT(data) >= 10
    AND SUM(gruppo.numeropartecipanti) = (
      SELECT
        MAX(visitatori)
      FROM (
          SELECT
            tipo_visita.monumento,
            SUM(gruppo.numeropartecipanti) AS visitatori
          FROM tipo_visita
          JOIN visita_guidata_effettuata ON tipo_visita.codtipovisita = visita_guidata_effettuata.codtipovisita
          JOIN gruppo ON gruppo.codgr = visita_guidata_effettuata.codgr
          GROUP BY
            (monumento)
          having
            COUNT(data) >= 10
        ) AS monumenti_visite
    )
  );