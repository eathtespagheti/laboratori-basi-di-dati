-- 3.A
SELECT
  iscrizione_per_attività_in_campo_estivo.codcampo,
  campivalidi.città,
  iscrizione_per_attività_in_campo_estivo.codattività,
  COUNT(DISTINCT codfiscale) AS "Iscritti"
FROM (
    SELECT
      -- Campi per cui è soddisfatto il numero di ragazzi iscritti ed il numero di attività
      campo_estivo.codcampo,
      città
    FROM (
        SELECT
          -- Esclusione iscrizioni con sola data differente
          codcampo,
          codattività,
          codfiscale
        FROM iscrizione_per_attività_in_campo_estivo
        GROUP BY
          codcampo,
          codattività,
          codfiscale
      ) AS dataesclusa
    JOIN campo_estivo ON dataesclusa.codcampo = campo_estivo.codcampo
    GROUP BY
      campo_estivo.codcampo
    HAVING
      COUNT(DISTINCT codattività) >= 3 -- Almeno 3 differenti attività
      AND COUNT(DISTINCT codfiscale) >= 15 -- Almeno 15 ragazzi iscritti
  ) AS campivalidi
JOIN iscrizione_per_attività_in_campo_estivo ON campivalidi.codcampo = iscrizione_per_attività_in_campo_estivo.codcampo
GROUP BY
  iscrizione_per_attività_in_campo_estivo.codcampo,
  campivalidi.città,
  iscrizione_per_attività_in_campo_estivo.codattività;