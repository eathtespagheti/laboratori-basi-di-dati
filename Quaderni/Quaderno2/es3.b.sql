-- 3.B
SELECT
  nome,
  cognome,
  nomecampo
FROM iscrizione_per_attività_in_campo_estivo
JOIN ragazzo ON iscrizione_per_attività_in_campo_estivo.codfiscale = ragazzo.codfiscale
JOIN campo_estivo ON iscrizione_per_attività_in_campo_estivo.codcampo = campo_estivo.codcampo,
  (
    SELECT
      codcampo,
      COUNT(*) AS "Numero attività"
    FROM (
        SELECT
          codcampo
        FROM iscrizione_per_attività_in_campo_estivo
        GROUP BY
          codcampo,
          codattività
        ORDER BY
          codcampo
      ) AS test
    GROUP BY
      codcampo
  ) AS num_attività
WHERE
  iscrizione_per_attività_in_campo_estivo.codfiscale IN (
    SELECT
      codfiscale
    FROM ragazzo
    WHERE
      datanascita <= '2005/01/01'
  )
  AND iscrizione_per_attività_in_campo_estivo.codcampo = num_attività.codcampo
GROUP BY
  ragazzo.codfiscale,
  campo_estivo.codcampo,
  num_attività."Numero attività"
HAVING
  COUNT (codattività) = num_attività."Numero attività";