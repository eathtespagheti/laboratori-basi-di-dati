-- 2.B
SELECT
  laboratorio.codlab,
  laboratorio.nomelab,
  esperimento.data
FROM laboratorio
JOIN dispositivo ON laboratorio.codlab = dispositivo.codlab
JOIN esperimento ON dispositivo.codicedisp = esperimento.codicedisp
WHERE
  dispositivo.codlab IN(
    SELECT
      dispositivo.codlab
    FROM esperimento
    JOIN studente ON esperimento.matricola = studente.matricola
    JOIN dispositivo ON esperimento.codicedisp = dispositivo.codicedisp
    JOIN (
        SELECT
          dispositivo.codlab,
          COUNT(*)
        FROM esperimento
        JOIN dispositivo ON esperimento.codicedisp = dispositivo.codicedisp
        GROUP BY
          dispositivo.codlab
      ) AS noninfo ON dispositivo.codlab = noninfo.codlab
    WHERE
      studente.corsodilaurea LIKE 'Ingegneria Informatica'
    GROUP BY
      dispositivo.codlab,
      noninfo.count
    HAVING
      COUNT(*) = noninfo.count
  )
  AND esperimento.categoria LIKE 'Elettronica'
  AND esperimento.data >= '2019/06/01'
  AND esperimento.data <= '2019/06/30';