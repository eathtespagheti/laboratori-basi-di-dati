# Progettazione Logica

## Entità

I campi in grassetto e corsivo indicano la chiave primaria

* __Edificio__ (*__Cod__*, Nome, Indirizzo, Volume)
* __Sala__ (*__Cod__*, *__CodEdificio__*, Nome, Posti, Posti In Piedi, Monitor)
* __Opera__ (*__Cod__*, Titolo, Autore, Materiale, Data*)
* __Operativo__ (*__CodFiscale__*, Nome, Cognome, Sesso, Anni di Servizio, Ruolo)
* __Restauratore__ (*__CodFiscale__*, Nome, Cognome, Sesso, Anni di Servizio)
* __Restauro__ (*__Cod__*, CodOpera, DataInizio, DataFine, Costo)
* __Mostra__ (*__Cod__*, Titolo, Descrizione, Temporanea, Periodo Artistico*, Esposta*, Responsabile*)
* __Periodo__ (*__CodEdificio__*, *__CodMostra__*, *__Inizio__*, *__Fine__*)
* __Specializzazione__ (*__Titolo__*)

### Relazioni trasformate in entità

* __Conserva__ (*__CodEdificio__*, *__CodOpera__*)
* __Specializzato__ (*__CodFiscale__*, *__Titolo__*)
* __Lavora__ (*__CodOpera__*, *__DataInizio__*, *__CodFiscale__*)
* __Espone__ (*__CodMostra__*, *__CodOpera__*)

## Vincoli di integrità

* Sala(CodEdificio) __REFERENCES__ Edificio(Cod)
* Restauro(CodOpera) __REFERENCES__ Opera(Cod)
* Mostra(Esposta) __REFERENCES__ Edificio(Cod)
* Mostra(Responsabile) __REFERENCES__ Operativo(CodFiscale)
* Periodo(CodEdificio) __REFERENCES__ Edificio(Cod)
* Periodo(CodMostra) __REFERENCES__ Mostra(Cod)
* Conserva(CodEdificio) __REFERENCES__ Edificio(Cod)
* Conserva(CodOpera) __REFERENCES__ Opera(Cod)
* Specializzato(CodFiscale) __REFERENCES__ Restauratore(CodFiscale)
* Specializzato(Titolo) __REFERENCES__ Specializzazione(Titolo)
* Lavora(CodOpera) __REFERENCES__ Opera(Cod)
* Lavora(CodFiscale) __REFERENCES__ Restauratore(CodFiscale)
* Espone(CodMostra) __REFERENCES__ Mostra(Cod)
* Espone(CodOpera) __REFERENCES__ Opera(Cod)
