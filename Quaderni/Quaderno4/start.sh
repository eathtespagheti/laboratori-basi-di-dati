#!/usr/bin/env sh

docker container ls -a | grep quaderno4 >/dev/null && {
    if [ "$1" = "-r" ]; then
        docker-compose -f quaderno4.yaml down --remove-orphans
    elif [ "$1" = "-stop" ]; then
        docker-compose -f quaderno4.yaml stop
        exit 0
    else
        docker-compose -f quaderno4.yaml start
        exit 0
    fi
}

docker-compose -f quaderno4.yaml up -d
