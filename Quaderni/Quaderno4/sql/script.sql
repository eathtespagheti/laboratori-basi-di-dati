DROP DATABASE IF EXISTS campionato_ciclistico;
CREATE DATABASE campionato_ciclistico;
USE campionato_ciclistico;
CREATE TABLE SQUADRA (
  CodS INT AUTO_INCREMENT PRIMARY KEY,
  NomeS VARCHAR(50) NOT NULL,
  AnnoFondazione INT NOT NULL CHECK(
    AnnoFondazione >= 1900
    AND AnnoFondazione <= 2000
  ),
  SedeLegale VARCHAR(50)
);
CREATE TABLE CICLISTA (
  CodC INT PRIMARY KEY,
  Nome VARCHAR(50) NOT NULL,
  Cognome VARCHAR(50) NOT NULL,
  Nazionalita VARCHAR(50) NOT NULL,
  CodS INT NOT NULL,
  AnnoNascita INT NOT NULL CHECK(
    AnnoNascita >= 1900
    AND AnnoNascita <= 2000
  ),
  FOREIGN KEY (CodS) REFERENCES SQUADRA(CodS) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE TAPPA (
  Edizione INT AUTO_INCREMENT,
  CodT INT CHECK(CodT >= 0),
  CittaPartenza VARCHAR(50) NOT NULL,
  CittaArrivo VARCHAR(50) NOT NULL,
  Lunghezza INT NOT NULL,
  Dislivello INT NOT NULL,
  GradoDifficolta SMALLINT NOT NULL CHECK(
    GradoDifficolta >= 0
    AND GradoDifficolta <= 10
  ),
  PRIMARY KEY (Edizione, CodT)
);
CREATE TABLE CLASSIFICA_INDIVIDUALE (
  CodC INT,
  CodT INT,
  Edizione INT,
  Posizione SMALLINT CHECK(Posizione >= 1),
  PRIMARY KEY(CodC, CodT, Edizione),
  FOREIGN KEY (CodC) REFERENCES CICLISTA(CodC) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (Edizione, CodT) REFERENCES TAPPA(Edizione, CodT) ON DELETE CASCADE ON UPDATE CASCADE
);
/*
Squadre
*/
INSERT INTO SQUADRA (CodS, NomeS, AnnoFondazione, SedeLegale)
VALUES
  (
    1,
    'San Diego Coastalcreeper',
    1916,
    '8 Buena Vista Trail'
  ),
  (2, 'Texan Horsenettle', 1941, '8 Eagan Plaza'),
  (3, 'Spleenwort', 2000, '3 Southridge Street'),
  (4, 'Yellowseed', 1951, '29 Maryland Place'),
  (
    5,
    'Tygh Valley Milkvetch',
    1965,
    '73015 Lakeland Crossing'
  ),
  (6, 'Waxflower', 1946, '19 Transport Place'),
  (
    7,
    'Parasite Cup Lichen',
    1933,
    '938 Carberry Circle'
  ),
  (8, 'Supple Fleabane', 1929, '009 Fairfield Way'),
  (
    9,
    'Broad Beechfern',
    1934,
    '21 Northview Circle'
  ),
  (10, 'White Phacelia', 1975, '19876 Onsgard Park');
/*
Ciclisti
*/
INSERT INTO CICLISTA (
    CodC,
    Nome,
    Cognome,
    Nazionalita,
    CodS,
    AnnoNascita
  )
VALUES
  (
    1,
    'Blakelee',
    'MacGibbon',
    'Bangladesh',
    8,
    1988
  ),
  (2, 'Flint', 'Human', 'Thailand', 5, 1982),
  (3, 'Tadeo', 'Cottey', 'Indonesia', 6, 1908),
  (4, 'Rey', 'Yurasov', 'Chile', 2, 1908),
  (5, 'Elora', 'Enrique', 'Indonesia', 7, 1901),
  (6, 'Pebrook', 'Widdowes', 'China', 3, 1987),
  (7, 'Marris', 'Corragan', 'Philippines', 8, 1915),
  (8, 'Livvyy', 'Millsom', 'China', 6, 1961),
  (9, 'Kennedy', 'Tiller', 'France', 4, 1980),
  (10, 'Fionna', 'Scarsbrooke', 'Portugal', 8, 1907),
  (11, 'Frederich', 'Waddilove', 'France', 5, 1935),
  (12, 'Angy', 'Vella', 'Croatia', 8, 1919),
  (13, 'Heywood', 'Raunds', 'Sweden', 6, 1916),
  (14, 'Mommy', 'Habershon', 'Indonesia', 9, 1920),
  (15, 'Leena', 'Erswell', 'Argentina', 1, 1930),
  (16, 'Sharon', 'Reedie', 'Poland', 9, 1981),
  (17, 'Conchita', 'Vanns', 'China', 1, 1907),
  (18, 'Prince', 'Pepis', 'Greece', 10, 1973),
  (19, 'Benedetta', 'Coffey', 'Kyrgyzstan', 5, 1993),
  (20, 'Pascal', 'Banane', 'Indonesia', 6, 1913),
  (21, 'Merrily', 'Allain', 'Slovenia', 10, 1911),
  (22, 'Filia', 'Keymer', 'Sweden', 3, 1909),
  (23, 'Harmonia', 'Kingsman', 'Portugal', 1, 1950),
  (24, 'Gussie', 'Purvis', 'Portugal', 1, 1950),
  (25, 'Heath', 'O''Dennehy', 'China', 10, 1972),
  (26, 'Sergeant', 'Craigg', 'France', 8, 1901),
  (27, 'Gaye', 'Englishby', 'China', 4, 1901),
  (28, 'Modesty', 'Awcoate', 'Netherlands', 4, 1908),
  (29, 'Hazel', 'Kane', 'Mexico', 2, 1910),
  (30, 'Torrin', 'Bartczak', 'Peru', 10, 1919),
  (31, 'Aidan', 'Piborn', 'Brazil', 3, 1939),
  (32, 'Liva', 'Walworth', 'Latvia', 5, 1901),
  (33, 'Catrina', 'Hurche', 'Poland', 1, 1935),
  (34, 'Kent', 'Lohrensen', 'Azerbaijan', 4, 1969),
  (35, 'Wittie', 'Reyna', 'United States', 1, 1957),
  (36, 'Ethelda', 'Eddoes', 'Canada', 9, 1926),
  (37, 'Valeda', 'O''Currigan', 'Poland', 6, 1986),
  (38, 'Alana', 'Flobert', 'China', 3, 1942),
  (39, 'Hailey', 'Hilbourne', 'Indonesia', 4, 1953),
  (40, 'Pandora', 'Fritter', 'Sweden', 1, 1904),
  (41, 'Alvy', 'Jurs', 'Azerbaijan', 3, 1904),
  (42, 'Modesta', 'Duham', 'Philippines', 2, 1911),
  (43, 'Beverly', 'Tubby', 'Portugal', 5, 1987),
  (44, 'Bealle', 'Leon', 'China', 5, 1989),
  (45, 'Michell', 'Espadero', 'Nigeria', 6, 1958),
  (46, 'Kessia', 'Morman', 'Luxembourg', 2, 1964),
  (47, 'Glenine', 'Jopling', 'China', 9, 1968),
  (
    48,
    'Marcos',
    'Shannahan',
    'Philippines',
    3,
    1932
  ),
  (49, 'Kristina', 'Persehouse', 'China', 1, 1971),
  (50, 'Neysa', 'Dicke', 'South Korea', 4, 1963),
  (51, 'Clair', 'Cavozzi', 'Russia', 8, 1987),
  (52, 'Patience', 'Gregolotti', 'Poland', 10, 1936),
  (53, 'Dawna', 'Nelthrop', 'China', 4, 1911),
  (54, 'Jelene', 'Deackes', 'Ethiopia', 5, 1981),
  (55, 'Krystal', 'Keilloh', 'Russia', 5, 1972),
  (56, 'Carlene', 'Satterlee', 'Serbia', 4, 1924),
  (57, 'Gav', 'Pattinson', 'China', 6, 1903),
  (58, 'Stefano', 'Robertet', 'Brazil', 9, 1984),
  (59, 'Berne', 'Penk', 'Mexico', 2, 1955),
  (60, 'Kristien', 'McIndoe', 'China', 10, 1984),
  (61, 'Hermie', 'Earles', 'Azerbaijan', 7, 1929),
  (62, 'Ezechiel', 'Croci', 'Thailand', 2, 1938),
  (63, 'Ree', 'Kelway', 'Brazil', 2, 1996),
  (
    64,
    'Berkeley',
    'Mc Kellen',
    'United States',
    5,
    1990
  ),
  (65, 'Gretal', 'Wrist', 'Brazil', 5, 1947),
  (66, 'Rosina', 'Enrique', 'China', 4, 1999),
  (
    67,
    'Sigismond',
    'Schwandermann',
    'China',
    9,
    1938
  ),
  (68, 'Chrotoem', 'McBride', 'China', 3, 1980),
  (69, 'Trumann', 'Simononsky', 'Russia', 2, 1973),
  (
    70,
    'Aloisia',
    'Degli Abbati',
    'Costa Rica',
    3,
    1958
  ),
  (71, 'Malina', 'Maurice', 'Norway', 3, 1925),
  (72, 'Petey', 'Zeal', 'Afghanistan', 7, 1993),
  (73, 'Brittan', 'Couzens', 'Kazakhstan', 3, 1965),
  (74, 'Giffer', 'Clemente', 'Macedonia', 5, 1959),
  (75, 'Sigrid', 'O''Shirine', 'Colombia', 2, 1939),
  (76, 'Todd', 'Spatari', 'France', 9, 1945),
  (77, 'Roley', 'Elies', 'Russia', 9, 1963),
  (78, 'Berri', 'Ivashov', 'China', 5, 1986),
  (79, 'Gualterio', 'Gallear', 'Mongolia', 7, 1994),
  (
    80,
    'Talbert',
    'Kellaway',
    'Netherlands',
    2,
    1975
  ),
  (81, 'Langston', 'Withrington', 'France', 1, 1927),
  (82, 'Hamnet', 'Driver', 'Mexico', 10, 1929),
  (83, 'Lyndsie', 'Jepensen', 'China', 8, 1958),
  (84, 'Valdemar', 'Hacksby', 'China', 4, 1942),
  (85, 'Fredric', 'Paish', 'China', 10, 1921),
  (86, 'Katy', 'Havvock', 'Ukraine', 5, 1912),
  (87, 'Germana', 'Derrington', 'China', 9, 1920),
  (88, 'Angelika', 'Dronsfield', 'Russia', 8, 1953),
  (89, 'Dante', 'Tregenna', 'Nigeria', 4, 1984),
  (90, 'Amble', 'Sylvester', 'France', 2, 1987),
  (91, 'Miriam', 'Borrow', 'Cuba', 7, 1991),
  (92, 'Misty', 'Senogles', 'China', 10, 1989),
  (93, 'Mickie', 'Elkins', 'Indonesia', 6, 1943),
  (94, 'Joseph', 'Drinan', 'United States', 5, 1989),
  (95, 'Ulrikaumeko', 'Egiloff', 'Nigeria', 7, 1956),
  (96, 'Martino', 'Venner', 'China', 10, 1980),
  (97, 'Dana', 'Masic', 'Czech Republic', 4, 1998),
  (98, 'Beret', 'Knowlton', 'China', 2, 1911),
  (99, 'Norina', 'Genge', 'Indonesia', 5, 1983),
  (100, 'Merry', 'Terram', 'China', 9, 1953);
/*
Tappa
*/
INSERT INTO TAPPA (
    Edizione,
    CodT,
    CittaPartenza,
    CittaArrivo,
    Lunghezza,
    Dislivello,
    GradoDifficolta
  )
VALUES
  (2, 1, 'Teresópolis', 'Kuandian', 3697, 343, 3),
  (2, 2, 'Lianrao', 'Jiacun', 24282, 82, 3),
  (2, 3, 'Ngantru', 'Rączna', 39932, -204, 1),
  (2, 4, 'Jingyang', 'Shuangjing', 6146, 246, 6),
  (2, 5, 'Daojiang', 'Maha Sarakham', 35433, 919, 7),
  (2, 6, 'Passo', 'Mulanay', 8588, -537, 8),
  (2, 7, 'Wangkung', 'Sandakan', 15928, -937, 2),
  (
    2,
    8,
    'Żywiec',
    'Villa Constitución',
    6357,
    -104,
    6
  ),
  (1, 1, 'Ovalle', 'Munde', 14091, -164, 6),
  (1, 2, 'Dzuunmod', 'Przystajń', 6902, -705, 3),
  (1, 3, 'Jamalteca', 'Nangan', 14889, -412, 5),
  (1, 4, 'Cineumbeuy', 'Watrous', 10926, -926, 0),
  (1, 5, 'Laguna', 'Xinghua', 11338, -986, 8),
  (
    1,
    6,
    'Ouanaminthe',
    'Umm Şalāl ‘Alī',
    24081,
    -894,
    2
  ),
  (1, 7, 'Lahoysk', 'Sumeng', 2174, -504, 10),
  (1, 8, 'Cidade Velha', 'Rzozów', 2873, 383, 10);
/*  
Classifiche
*/
INSERT INTO CLASSIFICA_INDIVIDUALE (CodC, CodT, Edizione, Posizione)
VALUES
  (1, 6, 2, 1),
  (88, 6, 2, 2),
  (69, 6, 2, 3),
  (21, 6, 2, 4),
  (46, 6, 2, 5),
  (54, 6, 2, 6),
  (13, 6, 2, 7),
  (68, 6, 2, 8),
  (30, 6, 2, 9),
  (67, 6, 2, 10),
  (27, 5, 2, 1),
  (15, 5, 2, 2),
  (11, 5, 2, 3),
  (34, 5, 2, 4),
  (64, 5, 2, 5),
  (62, 5, 2, 6),
  (68, 5, 2, 7),
  (25, 5, 2, 8),
  (96, 5, 2, 9),
  (14, 5, 2, 10),
  (41, 4, 2, 1),
  (7, 4, 2, 2),
  (70, 4, 2, 3),
  (74, 4, 2, 4),
  (15, 4, 2, 5),
  (11, 4, 2, 6),
  (63, 4, 2, 7),
  (23, 4, 2, 8),
  (58, 4, 2, 9),
  (64, 4, 2, 10),
  (80, 3, 2, 1),
  (53, 3, 2, 2),
  (46, 3, 2, 3),
  (50, 3, 2, 4),
  (17, 3, 2, 5),
  (91, 3, 2, 6),
  (38, 3, 2, 7),
  (7, 3, 2, 8),
  (43, 3, 2, 9),
  (86, 3, 2, 10),
  (17, 2, 2, 1),
  (15, 2, 2, 2),
  (10, 2, 2, 3),
  (67, 2, 2, 4),
  (61, 2, 2, 5),
  (20, 2, 2, 6),
  (68, 2, 2, 7),
  (23, 2, 2, 8),
  (46, 2, 2, 9),
  (45, 2, 2, 10),
  (62, 1, 2, 1),
  (27, 1, 2, 2),
  (44, 1, 2, 3),
  (68, 1, 2, 4),
  (21, 1, 2, 5),
  (10, 1, 2, 6),
  (78, 1, 2, 7),
  (58, 1, 2, 8),
  (34, 1, 2, 9),
  (1, 1, 2, 10),
  (63, 2, 1, 1),
  (94, 2, 1, 2),
  (88, 2, 1, 3),
  (36, 2, 1, 4),
  (51, 2, 1, 5),
  (11, 2, 1, 6),
  (70, 2, 1, 7),
  (12, 2, 1, 8),
  (6, 2, 1, 9),
  (48, 2, 1, 10),
  (45, 3, 1, 1),
  (49, 3, 1, 2),
  (89, 3, 1, 3),
  (98, 3, 1, 4),
  (87, 3, 1, 5),
  (67, 3, 1, 6),
  (24, 3, 1, 7),
  (2, 3, 1, 8),
  (58, 3, 1, 9),
  (77, 3, 1, 10),
  (96, 4, 1, 1),
  (84, 4, 1, 2),
  (24, 4, 1, 3),
  (16, 4, 1, 4),
  (48, 4, 1, 5),
  (58, 4, 1, 6),
  (28, 4, 1, 7),
  (74, 4, 1, 8),
  (12, 4, 1, 9),
  (44, 4, 1, 10),
  (18, 5, 1, 1),
  (82, 5, 1, 2),
  (81, 5, 1, 3),
  (91, 5, 1, 4),
  (3, 5, 1, 5),
  (99, 5, 1, 6),
  (22, 5, 1, 7),
  (21, 5, 1, 8),
  (40, 5, 1, 9),
  (17, 5, 1, 10),
  (75, 6, 1, 1),
  (37, 6, 1, 2),
  (30, 6, 1, 3),
  (24, 6, 1, 4),
  (74, 6, 1, 5),
  (21, 6, 1, 6),
  (73, 6, 1, 7),
  (89, 6, 1, 8),
  (8, 6, 1, 9),
  (16, 6, 1, 10),
  (29, 7, 1, 1),
  (93, 7, 1, 2),
  (51, 7, 1, 3),
  (71, 7, 1, 4),
  (46, 7, 1, 5),
  (72, 7, 1, 6),
  (95, 7, 1, 7),
  (49, 7, 1, 8),
  (50, 7, 1, 9),
  (73, 7, 1, 10),
  (12, 8, 1, 1),
  (9, 8, 1, 2),
  (13, 8, 1, 3),
  (33, 8, 1, 4),
  (3, 8, 1, 5),
  (45, 8, 1, 6),
  (90, 8, 1, 7),
  (2, 8, 1, 8),
  (19, 8, 1, 9),
  (66, 8, 1, 10),
  (14, 8, 2, 1),
  (50, 8, 2, 2),
  (71, 8, 2, 3),
  (89, 8, 2, 4),
  (63, 8, 2, 5),
  (48, 8, 2, 6),
  (57, 8, 2, 7),
  (67, 8, 2, 8),
  (84, 8, 2, 9),
  (41, 8, 2, 10),
  (74, 7, 2, 1),
  (81, 7, 2, 2),
  (78, 7, 2, 3),
  (91, 7, 2, 4),
  (72, 7, 2, 5),
  (11, 7, 2, 6),
  (70, 7, 2, 7),
  (100, 7, 2, 8),
  (56, 7, 2, 9),
  (29, 7, 2, 10),
  (3, 1, 1, 1),
  (42, 1, 1, 2),
  (39, 1, 1, 3),
  (9, 1, 1, 4),
  (55, 1, 1, 5),
  (80, 1, 1, 6),
  (97, 1, 1, 7),
  (60, 1, 1, 8),
  (7, 1, 1, 9),
  (17, 1, 1, 10);