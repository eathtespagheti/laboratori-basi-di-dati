SELECT
  Nome,
  Cognome,
  NomeS,
  CittaPartenza,
  CittaArrivo,
  Dislivello,
  GradoDifficolta,
  Lunghezza,
  TAPPA.Edizione,
  Posizione
FROM CICLISTA
JOIN (
    SELECT
      *
    FROM CLASSIFICA_INDIVIDUALE
    WHERE
      CodC = 1
      AND CodT = 1
  ) AS classifica ON CICLISTA.CodC = classifica.CodC
JOIN SQUADRA ON CICLISTA.CodS = SQUADRA.CodS
JOIN TAPPA ON classifica.CodT = TAPPA.CodT
  AND classifica.Edizione = TAPPA.Edizione
ORDER BY
  TAPPA.Edizione