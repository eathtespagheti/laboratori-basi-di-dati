<?php
function connectToDB()
{
    return mysqli_connect("db", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"], $_ENV["MYSQL_DATABASE"]);
}

function openDB()
{
    global $db;
    if (!mysqli_connect_errno($db)) {
        $db = connectToDB();
    }
}

function checkDB()
{
    global $db;
    if (!isset($db)) {
        return false;
    }
    return mysqli_connect_errno($db);
}

function getDB()
{
    global $db;
    if (checkDB()) {
        return $db;
    } else {
        openDB();
    }
    return $db;
}

function stringCloseTag($tag)
{
    return "</" . $tag . ">";
}

function stringCloseLastTag()
{
    global $_OPEN_TAGS;
    if (!isset($_OPEN_TAGS)) {
        return false;
    }
    $tag = array_pop($_OPEN_TAGS);
    if (is_null($tag)) {
        unset($_OPEN_TAGS);
        return false;
    }
    return stringCloseTag($tag);
}

function stringTagger($tag, $parameters, $addToOpen)
{
    if (is_array($parameters)) {
        $arrayString = "";
        foreach ($parameters as $key => $value) {
            if (is_int($key)) {
                $arrayString = $arrayString . $value . " ";
            } else {
                $arrayString = $arrayString . $key . "=\"" . $value . "\" ";
            }
        }
        return stringTagger($tag, $arrayString, $addToOpen);
    }
    global $_OPEN_TAGS;
    if ($addToOpen) {
        if (!isset($_OPEN_TAGS)) {
            $_OPEN_TAGS = array();
        }
        array_push($_OPEN_TAGS, $tag);
    }
    return "<" . $tag . " " . $parameters . ">";
}

function stringTagOpen($tag, $parameters)
{
    return stringTagger($tag, $parameters, true);
}

function stringTagUnclosed($tag, $parameters)
{
    return stringTagger($tag, $parameters, false);
}

function stringTag($tag, $content, $parameters)
{
    return stringTagOpen($tag, $parameters) . $content . stringCloseLastTag();
}

function stringShortTag($tag, $content)
{
    return stringTag($tag, $content, "");
}

function stringShortTagOpen($tag)
{
    return stringTagOpen($tag, "", "");
}

function stringShortTagUnclosed($tag)
{
    return stringTagUnclosed($tag, "", "");
}

function printCloseTag($tag)
{
    echo stringCloseTag($tag);
}

function closeLastTag()
{
    echo stringCloseLastTag();
}

function printTag($tag, $content, $parameters)
{
    echo stringTag($tag, $content, $parameters);
}

function printShortTag($tag, $content)
{
    echo stringShortTag($tag, $content);
}

function printShortTagOpen($tag)
{
    echo stringShortTagOpen($tag);
}

function printShortTagUnclosed($tag)
{
    echo stringShortTagUnclosed($tag);
}

function printTagOpen($tag, $parameters)
{
    echo stringTagOpen($tag, $parameters);
}

function printTagUnclosed($tag, $parameters)
{
    echo stringTagUnclosed($tag, $parameters);
}

function stringCloseAllTags()
{
    $closed = "";
    while ($close = stringCloseLastTag()) {
        $closed = $closed . $close;
    }
    return $closed;
}

function closeAllTags()
{
    $close = stringCloseAllTags();
    if ($close != "") {
        echo $close;
    }
}

function printFormInput($type, $label, $name)
{
    printTag("label", $label, array("for" => $name));

    printTagUnclosed("input", array(
        "type" => $type,
        "name" => $name,
        "id" => $name
    ));
}

function printFormInputWithPlaceholder($type, $label, $name, $placeholder)
{
    printTag("label", $label, array("for" => $name));

    printTagUnclosed("input", array(
        "type" => $type,
        "placeholder" => $placeholder,
        "name" => $name,
        "id" => $name
    ));
}

function printSubmit($value)
{
    printTagUnclosed("input", array(
        "type" => "submit",
        "value" => $value
    ));
}
