<?php
require_once __DIR__ . "/functions.php";
$links = array(
    "Homepage" => "/index.php",
    "Ricerca Posizione in Classifica" => "/posizione.php",
    "Inserimento Ciclista" => "/inserisci-ciclista.php",
    "Inserimento in Classifica" => "/inserisci-classifica.php"
);

printTagOpen("div", array("class" => "links"));
printShortTag("h3", "Collegamenti");
printShortTagOpen("ul");

foreach ($links as $title => $link) {
    printShortTag(
        "li",
        stringTag("a", $title, array("href" => $link))
    );
}

closeLastTag(); # Close ul
closeLastTag(); # Close div
