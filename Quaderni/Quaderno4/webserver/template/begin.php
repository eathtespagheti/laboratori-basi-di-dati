<!DOCTYPE html>
<?php
require_once __DIR__ . "/functions.php";
printTagOpen(
    "html",
    array("lang" => "it")
);
printShortTagOpen("head");
printTagUnclosed("meta", array(
    "charset" => "UTF-8",
    "name" => "viewport",
    "content" => "width=device-width, initial-scale=1.0"
));
printTagUnclosed("link", array(
    "rel" => "stylesheet",
    "href" => "/style.css"
));

# Title
printShortTagOpen("title");
if (isset($_PAGE_TITLE)) {
    echo $_PAGE_TITLE;
} else {
    echo "Title";
}

closeLastTag();
closeLastTag();
printShortTagOpen("body");
?>