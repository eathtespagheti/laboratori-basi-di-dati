<?php
$_PAGE_TITLE = "Inserimento nuova posizione in classifica";
require __DIR__ . "/../template/begin.php";

$query = "INSERT INTO CLASSIFICA_INDIVIDUALE VALUES (?, ?, ?, ?);";

if (
    isset($_POST["CodC"]) &&
    isset($_POST["CodT"]) &&
    isset($_POST["Edizione"]) &&
    isset($_POST["Posizione"])
) {
    $parametrizedQuery = getDB()->prepare($query);
    $parametrizedQuery->bind_param(
        'iiii',
        $_POST["CodC"],
        $_POST["CodT"],
        $_POST["Edizione"],
        $_POST["Posizione"]
    );
    $parametrizedQuery->execute();

    if ($parametrizedQuery->affected_rows == 1) {
        printTag("h3", "Inserimento completato!", array("class" => "good"));
    } else {
        printTag("h3", "Errore inserimento dati!", array("class" => "error"));
    }
    $parametrizedQuery->close();
} else {
    printTag("h3", "Missing Parameters!", array("class" => "error"));
}
require __DIR__ . "/../template/end.php";
