<?php
$_PAGE_TITLE = "Ricerca Posizione - Risultati";
require __DIR__ . "/../template/begin.php";

$query = "SELECT
  DISTINCT
  Cognome,
  Nome,
  TAPPA.CodT,
  CLASSIFICA_INDIVIDUALE.Edizione,
  Posizione
FROM CLASSIFICA_INDIVIDUALE
JOIN TAPPA ON CLASSIFICA_INDIVIDUALE.CodT = TAPPA.CodT
JOIN CICLISTA ON CLASSIFICA_INDIVIDUALE.CodC = CICLISTA.CodC
WHERE
  CLASSIFICA_INDIVIDUALE.CodC = ?
  AND TAPPA.CodT = ?";

if (isset($_POST["CodC"]) && isset($_POST["CodT"])) {
  printShortTagOpen("table");
  printShortTagOpen("thead");
  printShortTagOpen("tr");
  printShortTag("th", "Cognome");
  printShortTag("th", "Nome");
  printShortTag("th", "CodT");
  printShortTag("th", "Edizione");
  printShortTag("th", "Posizione");
  closeLastTag(); # Close tr
  closeLastTag(); # Close thead
  printShortTagOpen("tbody");

  $parametrizedQuery = getDB()->prepare($query);
  $parametrizedQuery->bind_param('ii', $_POST["CodC"], $_POST["CodT"]);
  $parametrizedQuery->execute();
  $parametrizedQuery->bind_result($Cognome, $Nome, $CodT, $Edizione, $Posizione);

  while ($parametrizedQuery->fetch()) {
    printShortTagOpen("tr");

    foreach (array($Cognome, $Nome, $CodT, $Edizione, $Posizione) as $key => $value) {
      printShortTag("td", $value);
    }

    closeLastTag(); # Close tr
  }
  $parametrizedQuery->close();

  closeLastTag(); # Close tbody
} else {
  printTag("h3", "Missing Parameters!", array("class" => "error"));
}
closeLastTag(); # Close table

require __DIR__ . "/../template/end.php";
