<?php
$_PAGE_TITLE = "Inserimento nuovo ciclista";
require __DIR__ . "/../template/begin.php";

$query = "INSERT INTO CICLISTA VALUES (?, ?, ?, ?, ?, ?);";

if (
    isset($_POST["CodC"]) &&
    isset($_POST["Nome"]) &&
    isset($_POST["Cognome"]) &&
    isset($_POST["Nazionalita"]) &&
    isset($_POST["CodS"]) &&
    isset($_POST["AnnoNascita"])
) {
    $parametrizedQuery = getDB()->prepare($query);
    $parametrizedQuery->bind_param(
        'isssii',
        $_POST["CodC"],
        $_POST["Nome"],
        $_POST["Cognome"],
        $_POST["Nazionalita"],
        $_POST["CodS"],
        $_POST["AnnoNascita"]
    );
    $parametrizedQuery->execute();
    
    if ($parametrizedQuery->affected_rows == 1) {
        printTag("h3", "Inserimento completato!", array("class" => "good"));
    } else {
        printTag("h3", "Errore inserimento dati!", array("class" => "error"));
    }
    $parametrizedQuery->close();
} else {
    printTag("h3", "Missing Parameters!", array("class" => "error"));
}
require __DIR__ . "/../template/end.php";
