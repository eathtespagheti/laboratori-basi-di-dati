<?php
$_PAGE_TITLE = "Inserimento Ciclista";
require __DIR__ . "/template/begin.php";

printTagOpen("form", array(
    "action" => "results/inserimento-ciclista.php",
    "method" => "post"
));
printShortTag("h3", $_PAGE_TITLE);

printFormInputWithPlaceholder("text", "Codice Ciclista:", "CodC", "e.g. 5");


printFormInputWithPlaceholder("text", "Nome Ciclista:", "Nome", "e.g. Mario");


printFormInputWithPlaceholder("text", "Cognome Ciclista:", "Cognome", "e.g. Rossi");


printFormInputWithPlaceholder("text", "Nazionalità:", "Nazionalita", "e.g. Italiana");


$squadre_query = mysqli_query(getDB(), "select CodS, NomeS from SQUADRA");
printTag("label", "Codice Squadra", array("for" => "CodS"));

printTagOpen("select", array(
    "name" => "CodS",
    "id" => "CodS"
));
printTag(
    "option",
    "Seleziona una squadra",
    array(
        "disabled",
        "selected"
    )
);
while ($row = mysqli_fetch_row($squadre_query)) {
    printTag(
        "option",
        $row[0] . " - " . $row[1],
        array("value" => $row[0])
    );
}
closeLastTag();



printFormInputWithPlaceholder("text", "Anno di Nascita Ciclista:", "AnnoNascita", "e.g. 1980");

printSubmit("Inserisci");

closeLastTag(); # Close form

require __DIR__ . "/template/end.php";
