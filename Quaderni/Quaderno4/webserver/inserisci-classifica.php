<?php
$_PAGE_TITLE = "Inserimento in Classifica";
require __DIR__ . "/template/begin.php";

printTagOpen("form", array(
    "action" => "results/inserimento-classifica.php",
    "method" => "post"
));

printShortTag("h3", $_PAGE_TITLE);

$ciclisti_query = mysqli_query(getDB(), "select CodC, Cognome, Nome from CICLISTA");
printTag("label", "Codice Ciclista:", array("for" => "CodC"));

printTagOpen("select", array(
    "name" => "CodC",
    "id" => "CodC"
));
printTag(
    "option",
    "Seleziona un ciclista",
    array(
        "disabled",
        "selected"
    )
);
while ($row = mysqli_fetch_row($ciclisti_query)) {
    printTag(
        "option",
        $row[0] . " - " . $row[1] . " " . $row[2],
        array("value" => $row[0])
    );
}
closeLastTag(); # Close select



$tappe_query = mysqli_query(getDB(), "select distinct CodT from TAPPA");
printTag("label", "Codice Tappa:", array("for" => "CodT"));

printTagOpen("select", array(
    "name" => "CodT",
    "id" => "CodT"
));
printTag(
    "option",
    "Seleziona una tappa",
    array(
        "disabled",
        "selected"
    )
);
while ($row = mysqli_fetch_row($tappe_query)) {
    printTag(
        "option",
        $row[0],
        array("value" => $row[0])
    );
}
closeLastTag(); # Close select

$edizione_query = mysqli_query(getDB(), "select distinct Edizione from TAPPA");
printTag("label", "Edizione", array("for" => "Edizione"));

printTagOpen("select", array(
    "name" => "Edizione",
    "id" => "Edizione"
));
printTag(
    "option",
    "Scegli Edizione",
    array(
        "disabled",
        "selected"
    )
);
while ($row = mysqli_fetch_row($edizione_query)) {
    printTag(
        "option",
        $row[0],
        array("value" => $row[0])
    );
}
closeLastTag(); # Close select

printFormInputWithPlaceholder("text", "Posizione Ciclista:", "Posizione", "e.g. 2");

printSubmit("Inserisci");

closeLastTag(); # Close form

require __DIR__ . "/template/end.php";
