<?php
$_PAGE_TITLE = "Ricerca posizione";
require __DIR__ . "/template/begin.php";

printTagOpen("form", array(
    "action" => "results/posizione.php",
    "method" => "post"
));
printShortTag("h3", $_PAGE_TITLE);

$atleti_query = mysqli_query(getDB(), "select CodC, Cognome, Nome from CICLISTA");
printTag("label", "Codice Ciclista", array("for" => "CodC"));

printTagOpen("select", array(
    "name" => "CodC",
    "id" => "CodC"
));
while ($row = mysqli_fetch_row($atleti_query)) {
    printTag(
        "option",
        $row[0] . " - " . $row[1] . " " . $row[2],
        array("value" => $row[0])
    );
}
closeLastTag(); # Close select



printFormInputWithPlaceholder("text", "Codice Tappa", "CodT", "e.g. 1");
printSubmit("Cerca");
closeLastTag(); # Close form

require __DIR__ . "/template/end.php";
